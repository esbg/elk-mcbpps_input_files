[0_(1)=APH2(9){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************
$0=164(164):
>APH2_disp consensus seq
{()INTIEISGEGNDCIAYEINXXXXRXDFIFKFPKHSRGXXXSTNLFNEVNILKRIHPXLIPEVVFTGMPSEYQFAGYTKIKGVPLTLLLNXXXNLPKQSQNQAAKDLARFLSELHSICLIHNDFSSDHILFDTETICGIIDFGDAAIXSDPDNDFISLMEEYGEF()}*

$1=223(164):
>gi|379318673|pdb|4DTB|A
{()INTIEISGEGNDCIAYEIN----R-DFIFKFPKHSRG---STNLFNEVNILKRIHnklP-LpIPEVVFTGMPSEtYQmsFAGYTKIKGVPLTpLLLN---NLPKQSQNQAAKDLARFLSELHSInisgfksnlvldfrekinednkkikkllsrelkgpqmkkvddfyrdileneiyfkyypCLIHNDFSSDHILFDTEknTICGIIDFGDAAI-SDPDNDFISLMeddEEYGmEF()}*

$2=221(164):
>gi|254839395|pdb|3HAM|A
{()INELRYLSSGDDSDTFLCN----E-QYVVKVPKRDSV---RISQKREFELYRFLEncklS-YqIPAVVY---QSDrFNim----KYIKGERITyEQYH---KLSEKEKDALAYDEATFLKELHSIeidcsvslfsdalvnkkdkflqdkkllisilekeqlltdemlehietiyenilnnavlfkytpCLVHNDFSANNMIF--RnnRLFGVIDFGDFNV-GDPDNDFLCLLdcstDDFGkEF()}*

$3=223(164):
>gi|379318673|pdb|4DTB|A
{()INTIEISGEGNDCIAYEIN----R-DFIFKFPKHSRG---STNLFNEVNILKRIHnklP-LpIPEVVFTGMPSEtYQmsFAGYTKIKGVPLTpLLLN---NLPKQSQNQAAKDLARFLSELHSInisgfksnlvldfrekinednkkikkllsrelkgpqmkkvddfyrdileneiyfkyypCLIHNDFSSDHILFDTEknTICGIIDFGDAAI-SDPDNDFISLMeddEEYGmEF()}*

$4=220(164):
>gi|345022069|ref|ZP_08785682.1|
{()VNHIIKLGEGWMSRAYLIN----H-EVVFRFPKEKAG---AIDTEIEIKALPSLKkhiS-LnIPEFIFLGKQDNgFPFVGYRLLPGEPMNeQLFC---SLPPETKNKIAHQIAEFMDQISSFsvahaknldipvknfyqsyletfyevkekafpevnqdlqtyisyrfemylenknnfkyspKLLHADLSMDHLLFDQKrqELTGVIDFGDIKI-GDPDYEYLYLLEECGeAF()}*

$5=220(164):
>gi|403234394|ref|ZP_10912980.1|
{()IKHIMKLGEGWRSKAYLIN----H-TFVFRFPKAEAG---AIDTEKEIKVLPYLKnqiT-LqIPEFIYCGRQDNgFPFVGYKRLQGDPLTeQQLL---SLPVEVRNRICDKIAEFINQISSFnvekarelhvqetnfyheylqtfkevqekvypiinkelqthitsrftsyldnkeyfiytpMLLHSDLSIDHLLFDKQrqELTGIIDFGDMRI-GDPDYEYIYLFEECGeEF()}*

$6=223(164):
>gi|218289249|ref|ZP_03493484.1|
{()VDRLEIHDEGWDHWVVVVN----R-AWVFRFARRPGV---AKRLCIEQRVLNHIGdrgqkaG-VrVPSPSPVKDARGrVIGSRYPFISGEPLRaAVLE---GLDASTTDRIAEHLGSFLTVLHDAdvsafqtqgvpalqtesfwqqdwremqrrvrllldldeqrmlvgrfenflnqalsgtlprALLHGDLTHEHILYDPRvtEAIGIIDFGDVQI-GDPAYDFAGLYWDYGpAF()}*

$7=223(164):
>gi|152974906|ref|YP_001374423.1| unnamed protein product 
{()IDSFQQNESGWDHIAIIIN----N-ALLFRFPREATI---AKRIPLEKELCSLLAksLHdIeIPNYHLFYtNESDtIPtcsyYTLIYGEPLTAQmI---A---NLTIQEREQVITQIAQFLATLHTIslehatqlgfkiektitywkqlqmklnrylshtfikcekeafdklfhqfftlfhrydlqyTMIHADFTHHHILFDEKrkKVSGIIDFGDTQI-RDPAFDFAGLYHDFGnVF()}*

$8=223(164):
>gi|228996591|ref|ZP_04156230.1| unnamed protein product 
{()IHSYEQNDNGWDNVAIIIN----N-EWLFRFPRKSQY---AKRIPREKELCEILSqsSQeIkVPKYHILYkNELDlIPfcsyYKLIHGEPLTVQtI---E---RLTVKERETVISQLAIFLASLHTIpiehavkwgfdtekpisywrqiqtklhvhlactltllekkafdclfenffehmyasdfqkTMIHADFTHHHILFDGEkrSIAGIIDFGDAQI-GDPAFDFAGLYNNFGsEF()}*

_0].
[0_(1)=APH3-ACAD(7){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=214(164):
>ACD10_HUMAN  {|292(553)|}Acyl-CoA dehydrogenase family member 10 OS=Homo sapiens GN=ACAD10 PE=2 SV=1 
{()HGQSNPTYYI--------RL-A--NrDLVLRKKPPG-T---LLpSahaIEREFRIMKALanAGVPVPNVLDLCEDSSvigTPFYVMEYCPGLIYkdpslpG---LEP-SHRRAIYTAMNTV-LC---KIHSVDLQavgledygkqgdyiprqvrtwvkqyrasetstipamerliewlplhlprqqrttvvHGDFRLDNLVF-HPEepEVlAVLDWELSTL-GDPLADVAYSCLAHYLP()}*

$2=224(164):
>tr|I0Z2L9|ACAD10_9CHLO  {|18(506)|}Acyl-CoA dehydrogenase NM domain-like protein OS=Coccomyxa subellipsoidea C-169 GN=COCSUDRAFT_36129 PE=3 SV=1 
{()HGQSNPTYLL--------QV-S--GvvqcaKYVLRKKPPG-K---VLsSahaVEREFQVMAALggTNVPVPRALCLCTDAAiigTPFYVMQHVQGRIFtdpscpE---LAP-EERAATYAAMAAT-LA---SLHSVQPTavglsrfgrssgyckrqvwrwaqqyrsqltgpplpqmaaliswleahippedgdpsatrisHGDFRLDNLVV-DPRaagRVlAVLDWELCTL-GHPFADLAYSAMPYHLP()}*

$3=214(164):
>tr|E6ZIY6|ACAD10_DICLA  {|42(512)|}Acyl-CoA dehydrogenase family member 11 OS=Dicentrarchus labrax GN=ACAD11 PE=3 SV=1 
{()-GQSNPTFLI--------QT-P--SnSFVLRKKPPG-E---LLpGahkVDREYRVQKALfpTGFPVPQPFLHCTDSEvigTEFYLMEHVKGRIFsdqrlpG---VSP-AERTALYVAAVEV-LA---KLHSLDLAslslegygkgsgyckrqvstwtkqynaaahrdipamnelsdwlmknlpasdnevtlvHGDFRFDNLIF-HPTeaRViAVLDWELSTT-GQPLADLAYFLMHHFWP()}*

$4=219(164):
>tr|I7MHM9|ACAD10_TETTS  {|43(515)|}Acyl-CoA dehydrogenase family protein OS=Tetrahymena thermophila (strain SB210) GN=TTHERM_00540330 PE=3 SV=1 
{()HGQSNPTFYI--------NY-R--NeEYVLRKKPPG-K---LLkGahlVDREFKIISALhkANFPVAKPILYCSDPQiigTEFYVMSYVRGRIFradnidlkG---LIP-DERREIYRELLDV-LV---RLHKLDPYqiglgdlsanpeayydkqietwsrnyklaetdpikdmddviewlpnnkpvktadnskisivHGDYRLDNVIF-HPTepRIlAVLDWELTAL-GNPIADAAYTCMAYY--()}*

$5=211(164):
>tr|M7YFC4|ACAD10_TRIUA  {|29(500)|}Acyl-CoA dehydrogenase family member 10 OS=Triticum urartu GN=TRIUR3_02387 PE=3 SV=1 
{()------------------------GkYRLLRNSEGS-H---SNsTsslIDEIVHVLKALgayTDVPVPKVFCLCTDASvigTPFYIMEYLEGALYlnnkltG---ITP-DKRRNIYLAAAKT-LA---AIHKIDATavglqkygkrdnyckrqvdrwgkqylhstgegkparyqkmldlidwlkenipeedsstglgtglvHGDYRVDNLVF-HPTedRViGVLDWELSTL-GNQMCDVAYSCMPYIID()}*

$6=221(164):
>tr|M2Y866|ACAD10_GALSU  {|44(511)|}Acyl-CoA dehydrogenase OS=Galdieria sulphuraria GN=Gasu_06720 PE=3 SV=1 
{()HGQSNPTFLV--------QVPA--GtPFVIRKKPQG-T---LLpSahaVEREYEVVRCLgpTKVPVPKVYGFCSDSSvigTPFYLMEYVKGRIFtdlslpT---VAP-EERKQIYQEMAKV-LA---EIHNIDYKkiglesfgipgnyvmrqirrwtkqyeascegvgrdpcmdnlvkwlpanipysavapreestivHGDYRLDNLVF-AEGspKIlAVLDWELSTI-GNPLADVAYNCMMYRL-()}*

$7=220(164):
>tr|A8J3M3|ACAD10_CHLRE  {|51(514)|}Acyl-CoA dehydrogenase (Fragment) OS=Chlamydomonas reinhardtii GN=CHLREDRAFT_118751 PE=3 SV=1 
{()HGQSNPTFLV--------TI-G--QtKLVLRKKPPG-K---LLaSahaVEREHAVLAALspSGFPVPRAVHLCAGNEplgTPFYLMECAEGRVFldpnlpE---LAP-GQRSAVLV--CTV-LA---RLHSLDPSglglapgygnpdhycarqlrrwseqyrasvpqpmpevlrlmdwlgahvppgdarptrpavvHGDYRLDNIVF-RPDpsgsfTPlAVLDWELSTL-GNPWADVAYNCLPYHLP()}*

_0].
[0_(1)=APH3-Bac(7){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************
$1=233(164):
>gi|392311982|pdb|4FEV|A
{()ETSCSRPRLNSnLDADLyGyrwardNvG-Q--SGATIYRlygkPNAPE-L---FLkhgKgsvandVTDEMVRLNWLtaF-MPLPTIKHFIRTPDDAWLLTTAIPGKTAfQVLEEYP-DSGENIVDALAVF-LR---RLHSIPVCncpfnsdrvfrlaqaqsrmnnglvdasdfdderngwpveqvwkemhkllpfspdsvvtHGDFSLDNLIF-DEGKLiGCIDVGRVGI-ADRYQDLAILWNCLGEF()}*

$2=210(164):
>gi|15825812|pdb|1J7I|A
{()-----DTEGMSPA----KvyklvG-E--NENLYLKMTDSrykgtT-Y---D--VEREKDMMLWLeGKLPVPKVLHFERHDGWSNLLMSEADGVLCseE---YED-EqSPEKIIELYAEC-IR---LFHSIDISdcpytnsldsrlaeldyllnndladvdcenweedtpfkdprelydflktekpeeelvfsHGDLGDSNIFV-KDGKVsGFIDLGRSGR-ADKWYDIAFCVRSIRED()}*

$3=210(164):
>gi|50880304|emb|CAD36021.1|
{()-----DTEGMSPA----KvyklvG-E--NENLYLKMTDSrykgtT-Y---D--VEREKDMMLWLeGKLPVPKVLHFERHDGWSNLLMSEADGVLCseE---YED-EqSPEKIIELYAEC-IR---LFHSIDISdcpytnslnsrlaeldyllnndladvdcenweedtpfkdprelydflktekpeeelvfsHGDLGDSNIFV-KDGKVsGFIDLGRSGR-ADKWYDIAFCVRSIRED()}*

$4=213(164):
>gi|75766358|pdb|2BKK|A
{()--SVKDTEGMSPA----KvyklvG-E--NENLYLKMTDSrykgtT-Y---D--VEREKDMMLWLeGKLPVPKVLHFERHDGWSNLLMSEADGVLCseE---YED-EqSPEKIIELYAEC-IR---LFHSIDISdcpytnsldsrlaeldyllnndladvdsenweedtpfkdprelydflktekpeeelvfsHGDLGDSNIFV-KDGKVsGFIDLGRSGR-ADKWYDIAFCVRSIRED()}*

$5=210(164):
>gi|21730689|pdb|1L8T|A
{()-----DTEGMSPA----KvyklvG-E--NENLYLKMTDSrykgtT-Y---D--VEREKDMMLWLeGKLPVPKVLHFERHDGWSNLLMSEADGVLCseE---YED-EqSPEKIIELYAEC-IR---LFHSIDISdcpytnsldsrlaeldyllnndladvdcenweedtpfkdprelydflktekpeeelvfsHGDLGDSNIFV-KDGKVsGFIDLGRSGR-ADKWYDIAFCVRSIRED()}*

$6=209(164):
>gi|61252179|sp|P0A3Y6.1|KKA3_STAAU
{()-----DTEGMSPA----KvyklG-E--NENLYLKMTDSrykgtT-Y---D--VEREKDMMLWLeGKLPVPKVVHFERHDGWSNLLMSEADGVLCseE---YED-EqSPEKIIELYAEC-IR---LFHSIDISdcpytnsldsrlaeldyllnndladvdcenweedtpfkdprelydflktekpeeelvfsHGDLGDSKIFV-KDGKVsGFIDLGRSGR-ADKWYDIPFCVRSIRED()}*

$7=205(164):
>gi|125468|sp|P14508.1|KKA7_CAMJE
{()-----CSEGMSPA----EvykcqL-K--NTVCYLKKIDDifsktT-Y---S--VKREAEMMMWLsDKLKVPDVIEYGVREHSEYLIMSELRGK-HidC---FID--HPIKYIECLVNA-LH---QLQAIDIRncpfsskidvrlkelkylldnriadidvsnwedttefddpmtlyqwlcenqpqeelclsHGDMS-ANFFV-SHDGIYFYDLARCGV-ADKWLDIAFCVREIREY()}*

_0].
[0_(1)=APH3-Fungi(11){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=233(164):
>tr|G2RGS8|G2RGS8_THITE  {|20(30)|}Putative uncharacterized protein OS=Thielavia terrestris (strain ATCC 38088 / NRRL 8126) GN=THITE_2124798 PE=4 SV=1 
{()HMSYLNAGEGKEIHNFfGnrvlehTtS-T--GAVLAIKVKPPE-G---L--DRSEGDMMHYAatHGVLAPKV-QGVYDIItkkllARAIVSELVRGVPLvdvwmE---LSD-AQKSSIKDQLRTQ-LS---KMRSCTQPfigrvgrqhtrniydrlqqtycgpfddekqfdewclarltggplvrwkwkrvlererrassgrfvltHGDLTPRNILV-QDGVItGIVDWEYSGF-YPEYAEFAFAMVLcHSHE()}*

$2=226(164):
>tr|I8TM62|I8TM62_ASPO3  {|12(33)|}Uncharacterized protein OS=Aspergillus oryzae (strain 3.042) GN=Ao3042_08896 PE=4 SV=1 
{()-FSYTDAIQGKELYNYfGnrvlesRaS-G--GRVVAVKQKSTR-G---F--ARSEADMMHYAsqQpGILAPQV-LGCYDVDheITVTVSDLVEGDSLdnvwhT---MTK-EERKSIKQQLKEQ-LR---LFRQCTQPyigrinrqetknfydrleihfmgpfeseeefdswclervkspiakkiwarllpgmrgtgeqkfvltHGDLAARNIMV-KDGKItGIVDWEYSGF-FPEYMEYALATVIHD--()}*

$3=227(164):
>tr|C5JJA3|C5JJA3_AJEDS  {|22(34)|}Putative uncharacterized protein OS=Ajellomyces dermatitidis (strain SLH14081) GN=BDBG_02647 PE=4 SV=1 
{()ERRYDGSFDGETIYDYfGnlvikshNiR-D--GHPVAIKFKTVG-F---F--WESEADMMAYAhsQNLLAPAV-WECRQLDshr-IAMIADFVPGNPLdevwpT---LDE-QQRMSITEQLADH-LK---LFRSCTQPyigrvnrqptynpyegierklmgpfnseadfdqwclsriknpsekskwrrklaelrrnsqskfvltHGDLFPRNILV-KEGKItGIVDWERSGF-YPEYVEYALAACLYD--()}*

$4=228(164):
>tr|Q2UHT1|Q2UHT1_ASPOR  {|12(31)|}Predicted protein OS=Aspergillus oryzae (strain ATCC 42149 / RIB 40) GN=AO090023000323 PE=4 SV=1 
{()-FSYTDAIQGKELYSYfGnrvlesRaS-G--GRVVAVKQKSTR-G---F--ARSEADMMHYAsqQpGILAPQV-LGCYDVDpeITVTVSDLVEGDSLdnvwhT---MTK-EERKSIKQQLKEQ-LR---LFRQCTQPyigrinrqetknfydrleihfmgpfeseeefdswclervksptakkiwarllpgmrgsgeqkfvltHGDLAARNIMV-KGGKItGIVDWEYSGF-FPEYMEYALATVIHDGH()}*

$5=219(164):
>tr|C0NDF6|C0NDF6_AJECG  {|22(52)|}Putative uncharacterized protein OS=Ajellomyces capsulatus (strain G186AR / H82 / ATCC MYA-2454 / RMSCC 2432) GN=HCBG_01152 PE=4 SV=1 
{()-CRYTSHVKLFYGADTrGVwS-L--GSDVVLKERP-N-I---G--SQDEMANIQFVkenTQIPIPSVIkeWTDDNDR-HFVLMERIKGQTLedawpT---LSS-SDKERIADQVAKC-LV---QLRSLqsPQMqslnhrplysgwlflrepltphgpftsetefwdylslkltklpkealarlrtrlppsapytftHGDLTFCNIIV-KDGNLaGILDWEDAGY-FPVWWEYVAATIGLGE-()}*

$6=222(164):
>tr|C6HKG8|C6HKG8_AJECH  {|18(49)|}Putative uncharacterized protein OS=Ajellomyces capsulatus (strain H143) GN=HCDG_06699 PE=4 SV=1 
{()HCNYKSHVKLFYEAGDrGVwS-L--GSNLILKDRGSS-L---P--TI-EVPNIQFVqekTSIPVPTVVesWNEGE-H-TLILMKRIPGEPLsnvwpR---LST-DEKEMIARQTAEY-LL---ELRKLqsDNIqaldgrpvysnflfrdkdselphgplasddelwadmerglketipeaarirlrhcmpsatpytftHGDLTNVNIMV-ENGTVtGIIDWEMSGY-FPVWWEYVCTSVPDSME()}*

$7=221(164):
>tr|C6HM45|C6HM45_AJECH  {|20(50)|}Putative uncharacterized protein OS=Ajellomyces capsulatus (strain H143) GN=HCDG_07161 PE=4 SV=1 
{()RCHYTSHLKLFYGASTrGVwS-I--GSDVILKDRPDE-G---L--KAKvEVNTLNYLathTDIPVPKVLrdWVDRDGR-YFVLNERVGGQTLeeawtS---LSE-PQKLDVADQVVKV-RK---KLRSVtsTSIqcvdqspcypgllffdlepcgpfhsdqelwdalalnlrhlpqqvlenlrkslpkcepyvltHCDLNLGNIMV-QDGKVvGILDWEYAAF-FPIWYEYVSASFGFTDT()}*

$8=219(164):
>tr|E9E2S7|E9E2S7_METAQ  {|20(51)|}Putative uncharacterized protein OS=Metarhizium acridum (strain CQMa 102) GN=MAC_04175 PE=4 SV=1 
{()-CGYQSNVKIFYAVGDrATwE-V--GSQRIVKDRSSH-P---P--N-SEAENLRFVrvnTTIPVPVVVddWNDR-GR-HFLITERIPGTPLsevwlN---LSE-SDKDGYAKQTVEY-LH---QLRTLcsTRIqsineqpvysaflfrdgygiphgpltsdehlwyemakglknapqdlqdllrrrmpsaqpytftHGDLTLKNIIV-QNGSVtGIIDWESSGY-FPAWWEFVSTTIVDSED()}*

$9=221(164):
>tr|D4B124|D4B124_ARTBC  {|29(50)|}Putative uncharacterized protein OS=Arthroderma benhamiae (strain ATCC MYA-4681 / CBS 112371) GN=ARB_02153 PE=4 SV=1 
{()RCSHISQVKLVNAISNrAWwH-I--GSDMVLKEDPYD-K---W--KTSEVANLKFIqekTTIPVPTIVkdWVQSDNR-HFLLMERMPGETLdtlyqK---LST-AELEAIADQVAEL-IQ---QLRPLqsPQIggiggtplhngwifvnnmepagpfssddelwdciknglakvpakavenlrkrmpackpytwtHDDLSSSNIVV-KDGKVtGILDWEFCGY-YPVWWQYVSAGSGDPEE()}*

$10=224(164):
>tr|B6H9K4|B6H9K4_PENCW  {|19(53)|}Pc16g11130 protein OS=Penicillium chrysogenum (strain ATCC 28089 / DSM 1075 / Wisconsin 54-1255) GN=Pc16g11130 PE=4 SV=1 
{()RCIYQSSVRLFYGAHDrGYwS-L--GSKFLLKERGRD-P---P--S-HEVINTHFLkenTTIPIPTTVqeWTEGE-K-YFQIVERVPGVPLeeiwpS---IPQ-PDRERLARQTAEY-LG---QLRSFhsAKMeslhgqplscpllfndgnlgvthgplstadelwdvlakslsakvpevvklrlrdrmpsphpwtftHGDLTNCNFIV-DpvKFELtGLIDWEGSGY-YPVWWKFMGASFGFDED()}*

$11=222(164):
>tr|B6H770|B6H770_PENCW  {|19(54)|}Pc16g02490 protein OS=Penicillium chrysogenum (strain ATCC 28089 / DSM 1075 / Wisconsin 54-1255) GN=Pc16g02490 PE=4 SV=1 
{()-CRYQSHVNLFYGVSNrGVwS-L--GSNLILKERSTE-P---P--NF-EVPNIRFLaskTSIPIPRIVeeWQEDDGA-CFLLTRRIRGQPLselwpT---MTS-ADKERVAKQTAEY-LMI--QLHELrsNRMesiggqpiysaflfphtygighgpfssddelwaemsltlndqvpeqvrsklrqrmptaapytftHGDLTNVNIMV-ENGNLaGILDWESSAF-FPVWWEFTCAGIGLGQD()}*

_0].
[0_(1)=ChoK(12){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=244(164):
>sp|P35790|CHKA_HUMAN  {|109(104)|}Choline kinase alpha OS=Homo sapiens GN=CHKA PE=1 SV=3 
{()EFHISVIRGGLSNMLFQCSLPdttatlGdePR-KVLLRLYgA---I--LqmrscnkegseqaqkenefqgAEAMVLESVMFAILaERSLGPKLYGI-F-P-QG-RLEQFIPSRRLD--T----EELSL--PDISAEIAEKMATFHGMKmpfnkepkwlfgtmekylkevlrikfteesrikklhkllsynlplelenlrsllestpspvvFCHNDCQEGNILLlegrenseKQ--KLMLIDFEYSSY-NYRGFDIGNHFCEWMYD()}*

$2=244(164):
>sp|O54804|CHKA_MOUSE  {|105(104)|}Choline kinase alpha OS=Mus musculus GN=Chka PE=1 SV=3 
{()QFHISVIRGGLSNMLFQCSLPdsiasvGdePR-KVLLRLYgA---I--LkmrscnkegseqaqnenefqgAEAMVLESVMFAILaERSLGPKLFGI-F-P-QG-RLEQFIPSRRLD--T----EELRL--PDISAEIAEKMATFHGMKmpfnkepkwlfgtmekylnqvlrlkfsrearvqqlhkilsynlplelenlrsllqytrspvvFCHNDCQEGNILLlegqenseRR--KLMLIDFEYSSY-NYRGFDIGNHFCEWMYD()}*

$3=220(164):
>sp|Q9Y259|CHKB_HUMAN  {|67(108)|}Choline/ethanolamine kinase OS=Homo sapiens GN=CHKB PE=1 SV=3 
{()ELRVYPVSGGLSNLLFRCSLPdhlpsvGeePR-EVLLRLYgA---I--LqgVDSLVLESVMFAILaERSLGPQLYGV-F-P-EG-RLEQYIPSRPLK--T----QELRE--PVLSAAIATKMAQFHGMEmpftkephwlfgtmerylkqiqdlpptglpemnllemyslkdemgnlrkllestpspvvFCHNDIQEGNILLlsepenadS----LMLVDFEYSSY-NYRGFDIGNHFCEWVYD()}*

$4=220(164):
>sp|O55229|CHKB_MOUSE  {|67(107)|}Choline/ethanolamine kinase OS=Mus musculus GN=Chkb PE=1 SV=3 
{()ELSVCPVSGGLSNLLFRCSLPnhvpsvGgePR-EVLLRLYgA---I--LqgVDSLVLESVMFAILaERSLGPQLYGV-F-P-EG-RLEQYLPSRPLK--T----QELRD--PVLSGAIATRMARFHGMEmpftkeprwlfgtmerylkqiqdlpstslpqmnlvemyslkdemnslrkllddtpspvvFCHNDIQEGNILLlsepdsddN----LMLVDFEYSSY-NYRGFDIGNHFCEWVYD()}*

$5=271(164):
>sp|P20485|KICH_YEAST  {|146(165)|}Choline kinase OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) GN=CKI1 PE=1 SV=1 
{()DMNLVKISGAMTNAIFKVEYPKLP-SLLLRIYgP---N--IdnIIDREYELQILARLsLKNIGPSLYGC-F-V-NG-RFEQFLeNSKTLT--K----DDIRN--WKNSQRIARRMKELHvGVPllsserkngsacwqkinqwlrtiekvdqwvgdpkniensllcenwskfmdivdryhkwlisqeqgieqvnknliFCHNDAQYGNLLFtapvmntpslytapsstsltsqssslfpsssnvivddiinppkqeqsqDS--KLVVIDFEYAGA-NPAAYDLANHLSEWMYD()}*

$6=209(164):
>sp|Q9NVF9|EKI2_HUMAN  {|81(96)|}Ethanolamine kinase 2 OS=Homo sapiens GN=ETNK2 PE=2 SV=3 
{()QVRTKRFTDGITNKLVACYVEEdmQD-CVLVRVYgE---R--TelLVDRENEVRNFQLLrAHSCAPKLYCT-F-Q-NG-LCYEYMQGVALE--P----EHIRE--PRLFRLIALEMAKIHTIHangslpkpilwhkmhnyftlvkneinpslsadvpkvevlerelawlkehlsqlespvvFCHNDLLCKNIIYdsiKG--HVRFIDYEYAGY-NYQAFDIGNHFNEFA-G()}*

$7=215(164):
>sp|Q9D4V0|EKI1_MOUSE  {|47(101)|}Ethanolamine kinase 1 OS=Mus musculus GN=Etnk1 PE=2 SV=2 
{()EVTLQLFTDGITNKLIACYVGDtmED-VVLVRIYgN---K--TelLVDRDEEVKSFRVLqAHGCAPQLYCT-F-N-NG-LCYEFIQGEALD--P----QHVCN--PAIFRLIARQLAKIHAIHahngwipksnlwlkmgkyfsliptgfadeninkrflseipspqllqeemtwmkellsslgspvvLCHNDLLCKNIIYnekQG--DVQFIDYEYSGY-NYLAYDIGNHFNEFA-G()}*

$8=215(164):
>sp|Q9HBU6|EKI1_HUMAN  {|136(101)|}Ethanolamine kinase 1 OS=Homo sapiens GN=ETNK1 PE=1 SV=1 
{()EVTLQLFTDGITNKLIGCYVGNtmED-VVLVRIYgN---K--TelLVDRDEEVKSFRVLqAHGCAPQLYCT-F-N-NG-LCYEFIQGEALD--P----KHVCN--PAIFRLIARQLAKIHAIHahngwipksnlwlkmgkyfsliptgfadedinkrflsdipssqilqeemtwmkeilsnlgspvvLCHNDLLCKNIIYnekQG--DVQFIDYEYSGY-NYLAYDIGNHFNEFA-G()}*

$9=209(164):
>sp|A7MCT6|EKI2_MOUSE  {|80(96)|}Ethanolamine kinase 2 OS=Mus musculus GN=Etnk2 PE=1 SV=1 
{()QVRTKRFKDGITNKLLACYVEEdmRD-CVLVRVYgE---R--TelLVDRENEVRNFQLLrAHGCAPKLYCT-F-Q-NG-LCYEYVQGVALG--P----EHIRE--PQLFRLIALEMAKIHTIHangslpkptlwhkmhryftlvkdeispslsadvpkvevleqelawlkehlsqldspvvFCHNDLLCKNIIYdsdKG--RVCFIDYEYAGY-NYQAFDIGNHFNEFA-G()}*

$10=211(164):
>sp|Q9M9H6|CK1_ARATH  {|39(96)|}Probable choline kinase 1 OS=Arabidopsis thaliana GN=CK1 PE=2 SV=1 
{()-LEVVPLKGAMTNEVYQINWPTlngedVhR-KVLVRIYGdgv-------dlFFNRGDEIKTFECMsHHGYGPKLLGR-F-S-DG-RLEEFIHARTLS--A----DDLRV--AETSDFIAAKLREFHKLDmpgpknvllwerlrtwlkeaknlaspiemdkyrlegleneinlleerltrddqeigFCHNDLQYGNVMIdevTN--AITIIDYEYSSF-NPIAYDIANHFCEMAAN()}*

$11=211(164):
>sp|Q8L518|CK2_ARATH  {|39(100)|}Probable choline kinase 2 OS=Arabidopsis thaliana GN=At1g74320 PE=2 SV=1 
{()ALQVIPLKGAMTNEVFQIKWPTrekgpSR-KVLVRIYGegv-------eiFFDREDEIRTFEFMsKHGHGPLLLGR-F-G-NG-RIEEFLHARTLS--A----CDLRD--PEISGRIATRMKEFHGLEmpgakkallwdrlrnwltackrlaspeeaksfrldvmemeinmlekslfdndenigFCHNDLQYGNIMMdeeTK--AITIIDYEYSCY-NPVAYDIANHFCEMAAD()}*

$12=211(164):
>sp|Q9SZ92|CK3_ARATH  {|36(99)|}Probable choline kinase 3 OS=Arabidopsis thaliana GN=At4g09760 PE=2 SV=1 
{()SLEVKPMKGAMTNEVFMVSWPRketnlrCR-KLLVRVYGegv-------elFFNRDDEIRTFEYVaRHGHGPTLLGR-F-A-GG-RVEEFIHARTLS--A----TDLRD--PNISALVASKLRRFHSIHipgdrimliwdrmrtwvgqaknlcsnehstefglddiedeinlleqevnneqeigFCHNDLQYGNIMIdeeTN--AITIIDYEYASY-NPIAYDIANHFCEMAAD()}*

_0].
[0_(1)=ePK(15){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=189(164):
>sp|P17612|KAPCA_HUMAN  {|43(119)|}cAMP-dependent protein kinase catalytic subunit alpha OS=Homo sapiens GN=PRKACA PE=1 SV=2 
{()FERIKTLGTGSFGRVMLVKHKETGNHYAMKILDKQKVVKlKQIEHTlNEKRILQAVNFPFLVKLEFSFKDNSNLYMVMEYVPGGEMFSHLRRIGRFSEPHARFYAAQIVLTFEYLHSLDLIYRDLKPENLLIDQQGYIQVTDFGFAKRvkgrtwtlcgtpeylapeiilskGYNKAVDWWALGVLIYEM()}*

$2=189(164):
>sp|P22694|KAPCB_HUMAN  {|43(119)|}cAMP-dependent protein kinase catalytic subunit beta OS=Homo sapiens GN=PRKACB PE=1 SV=2 
{()FERKKTLGTGSFGRVMLVKHKATEQYYAMKILDKQKVVKlKQIEHTlNEKRILQAVNFPFLVRLEYAFKDNSNLYMVMEYVPGGEMFSHLRRIGRFSEPHARFYAAQIVLTFEYLHSLDLIYRDLKPENLLIDHQGYIQVTDFGFAKRvkgrtwtlcgtpeylapeiilskGYNKAVDWWALGVLIYEM()}*

$3=189(164):
>sp|P22612|KAPCG_HUMAN  {|43(119)|}cAMP-dependent protein kinase catalytic subunit gamma OS=Homo sapiens GN=PRKACG PE=1 SV=3 
{()FERLRTLGMGSFGRVMLVRHQETGGHYAMKILNKQKVVKmKQVEHIlNEKRILQAIDFPFLVKLQFSFKDNSYLYLVMEYVPGGEMFSRLQRVGRFSEPHACFYAAQVVLAVQYLHSLDLIHRDLKPENLLIDQQGYLQVTDFGFAKRvkgrtwtlcgtpeylapeiilskGYNKAVDWWALGVLIYEM()}*

$4=194(164):
>sp|P06493|CDK1_HUMAN  {|3(100)|}Cyclin-dependent kinase 1 OS=Homo sapiens GN=CDK1 PE=1 SV=3 
{()YTKIEKIGEGTYGVVYKGRHKTTGQVVAMKKIRLESEEEGVPSTAiREISLLKELRHPNIVSLQDVLMQDSRLYLIFEFLSM-DLKKYLDSiPPgqYMDSSLVKSYLYQILQGIVFCHSRRVLHRDLKPQNLLIDDKGTIKLADFGLARAfgipirvythevvtlwyrspevllgsaRYSTPVDIWSIGTIFAEL()}*

$5=195(164):
>sp|P50613|CDK7_HUMAN  {|11(140)|}Cyclin-dependent kinase 7 OS=Homo sapiens GN=CDK7 PE=1 SV=1 
{()YEKLDFLGEGQFATVYKARDKNTNQIVAIKKIKLGhrsEAKDGINRTAlREIKLLQELSHPNIIGLLDAFGHKSNISLVFDFMET-DLEVIIKDNSlVLTPSHIKAYMLMTLQGLEYLHQHWILHRDLKPNNLLLDENGVLKLADFGLAKSfgspnraythqvvtrwyrapellfgarMYGVGVDMWAVGCILAEL()}*

$6=193(164):
>sp|Q96PY6|NEK1_HUMAN  {|3(1062)|}Serine/threonine-protein kinase Nek1 OS=Homo sapiens GN=NEK1 PE=1 SV=2 
{()YVRLQKIGEGSFGKAILVKSTEDGRQYVIKEINISRMSSKEREESrREVAVLANMKHPNIVQYRESFEENGSLYIVMDYCEGGDLFKRINAQKgvLFQEDQILDWFVQICLALKHVHDRKILHRDIKSQNIFLTKDGTVQLGDFGIARVlnstvelartcigtpyylspeicenkPYNNKSDIWALGCVLYEL()}*

$7=189(164):
>sp|Q02750|MP2K1_HUMAN  {|67(137)|}Dual specificity mitogen-activated protein kinase kinase 1 OS=Homo sapiens GN=MAP2K1 PE=1 SV=2 
{()FEKISELGAGNGGVVFKVSHKPSGLVMARKLIHLEIKPAI-RNQIiRELQVLHECNSPYIVGFYGAFYSDGEISICMEHMDGGSLDQVLKKAGRIPEQILGKVSIAVIKGLTYLReKHKIMHRDVKPSNILVNSRGEIKLCDFGVSGQlidsmansfvgtrsymsperlqgtHYSVQSDIWSMGLSLVEM()}*

$8=189(164):
>sp|P36507|MP2K2_HUMAN  {|71(140)|}Dual specificity mitogen-activated protein kinase kinase 2 OS=Homo sapiens GN=MAP2K2 PE=1 SV=1 
{()FERISELGAGNGGVVTKVQHRPSGLIMARKLIHLEIKPAI-RNQIiRELQVLHECNSPYIVGFYGAFYSDGEISICMEHMDGGSLDQVLKEAKRIPEEILGKVSIAVLRGLAYLReKHQIMHRDVKPSNILVNSRGEIKLCDFGVSGQlidsmansfvgtrsymaperlqgtHYSVQSDIWSMGLSLVEL()}*

$9=207(164):
>sp|Q9UM73|ALK_HUMAN  {|1115(298)|}ALK tyrosine kinase receptor OS=Homo sapiens GN=ALK PE=1 SV=3 
{()ITLIRGLGHGAFGEVYEGQVSgmpndPSPLQVAVKTLPEVCSE-QDELDFlMEALIISKFNHQNIVRCIGVSLQSLPRFILLELMAGGDLKSFLRETRprpsqpsSLAMLDLLHVARDIACGCQYLEENHFIHRDIAARNCLLTCPgpgRVAKIGDFGMARDiyrasyyrkggcamlpvkwmppeafmegIFTSKTDTWSFGVLLWEI()}*

$10=196(164):
>sp|P21709|EPHA1_HUMAN  {|623(157)|}Ephrin type-A receptor 1 OS=Homo sapiens GN=EPHA1 PE=1 SV=4 
{()LMVDTVIGEGEFGEVYRGTLRlpsQDCKTVAIKTLKDTSPG-GQWWNFlREATIMGQFSHPHILHLEGVVTKRKPIMIITEFMENGALDAFLREREdQLVPGQLVAMLQGIASGMNYLSNHNYVHRDLAARNILVNQNLCCKVSDFGLTRLlddfdgtyetqggkipirwtapeaiahrIFTTASDVWSFGIVMWEV()}*

$11=197(164):
>sp|P29320|EPHA3_HUMAN  {|620(166)|}Ephrin type-A receptor 3 OS=Homo sapiens GN=EPHA3 PE=1 SV=2 
{()ISIDKVVGAGEFGEVCSGRLKlpsKKEISVAIKTLKVGYTE-KQRRDFlGEASIMGQFDHPNIIRLEGVVTKSKPVMIVTEYMENGSLDSFLRKHDaQFTVIQLVGMLRGIASGMKYLSDMGYVHRDLAARNILINSNLVCKVSDFGLSRVleddpeaayttrggkipirwtspeaiayrKFTSASDVWSYGIVLWEV()}*

$12=207(164):
>sp|P06213|INSR_HUMAN  {|1022(153)|}Insulin receptor OS=Homo sapiens GN=INSR PE=1 SV=4 
{()ITLLRELGQGSFGMVYEGNARdiikgEAETRVAVKTVNESASL-RERIEFlNEASVMKGFTCHHVVRLLGVVSKGQPTLVVMELMAHGDLKSYLRSLRpeaennpgrpPPTLQEMIQMAAEIADGMAYLNAKKFVHRDLAARNCMVAHDFTVKIGDFGMTRDiyetdyyrkggkgllpvrwmapeslkdgVFTTSSDMWSFGVVLWEI()}*

$13=190(164):
>sp|O14965|AURKA_HUMAN  {|132(81)|}Aurora kinase A OS=Homo sapiens GN=AURKA PE=1 SV=2 
{()FEIGRPLGKGKFGNVYLAREKQSKFILALKVLFKAQLEKaGVEHQLrREVEIQSHLRHPNILRLYGYFHDATRVYLILEYAPLGTVYRELQKLSKFDEQRTATYITELANALSYCHSKRVIHRDIKPENLLLGSAGELKIADFGWSVHapssrrttlcgtldylppemiegrMHDEKVDLWSLGVLCYEF()}*

$14=190(164):
>sp|Q96GD4|AURKB_HUMAN  {|76(78)|}Aurora kinase B OS=Homo sapiens GN=AURKB PE=1 SV=3 
{()FEIGRPLGKGKFGNVYLAREKKSHFIVALKVLFKSQIEKeGVEHQLrREIEIQAHLHHPNILRLYNYFYDRRRIYLILEYAPRGELYKELQKSCTFDEQRTATIMEELADALMYCHGKKVIHRDIKPENLLLGLKGELKIADFGWSVHapslrrktmcgtldylppemiegrMHNEKVDLWCIGVLCYEL()}*

$15=195(164):
>sp|Q96J92|WNK4_HUMAN  {|173(875)|}Serine/threonine-protein kinase WNK4 OS=Homo sapiens GN=WNK4 PE=1 SV=1 
{()LKFDIEIGRGSFKTVYRGLDTDTTVEVAWCELQTRKLSRAERQRFsEEVEMLKGLQHPNIVRFYDSWKSvlRGQvcIVLVTELMTSGTLKTYLRRFREMKPRVLQRWSRQILRGLHFLHSRVppILHRDLKCDNVFITGPtGSVKIGDLGLATLkrasfaksvigtpefmapemyeeKYDEAVDVYAFGMCMLEM()}*

_0].
[0_(1)=Fam20(3){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=164(164):
>Fam20.fas consensus seq 
{()IKHVHIMDGGTQVKXFVFTFKXNDKQAVFKPMRFDYESDFFERHHAEIATFHVLGFRRAIPTVGRVLNMTKEGSVQVFLPDKHNRSPYRVKTKKQYAHXGRRLLDLVDIHILDYLXXIGNQDRHHXXFESFVFSYAIHLDHGRAFGXRDDDDIILXXRQXCCII()}*

$2=248(164):
>tr|A8X4B8|A8X4B8_CAEBR Protein CBG07668 OS=Caenorhabditis briggsae GN=CBG07668 PE=4 SV=1 
{()IKHVHIMDGGTQVK-FVFTFK-NDKQAVFKPMRFgrDYESDpnhfyFsdFERHHAEIATFHldrVLGFRRAIPTVGRVLNMTtelfekaekklkktfffspaknfcfvsrcdyycdtthaicglpdmKEGSVQVFLPDesavprKHNRSPYRrtyskknqvaewqssisyctdkVKTKKQYAH-GRRLLDLVDIHILDYL--IGNQDRHH--FESFnVFtdlpSYAIHLDHGRAFG-RsdiDDDDIIlpL--RQ-CCII()}*

$3=248(164):
>tr|E3MCC2|E3MCC2_CAERE Putative uncharacterized protein OS=Caenorhabditis remanei GN=CRE_15436 PE=4 SV=1 
{()IKHVHIMDGGTQVK-FVFTFK-NDKQAVFKPMRFgrDYESDpnhfyFsdFERHHAEIATFHldrVLGFRRAIPTVGRVLNMTtelfekaekklkktfffspaknfcfvsrcdyycdtthaicglpdmKEGSVQVFLPDesavprKHNRSPYRrtyskknqvaewqssmnyctekVKTKKQYAH-GRRLLDLVDIHILDYL--IGNQDRHH--FESFnVFtdlpSYAIHLDHGRAFG-RsdiDDDDIIlpL--RQ-CCII()}*

_0].
[0_(1)=FruK(8){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=196(164):
>tr|B4DXG0|B4DXG0_HUMAN  {|8(55)|}cDNA FLJ57651, highly similar to Ketosamine-3-kinase (EC 2.7.1.-) OS=Homo sapiens PE=2 SV=1 
{()LTAILKTNTvkvpkP----IKVLDAP-GGGSVLVMEHMDMR--HLsSHaaklgaqladlhldnkklgemrlkeagtvgrgggqEERPFVARFGFDVVtccGYLPQVNDW-Q-EDW--VV-FYARQRiqP----------Q-----MDMVEKESGdreaLQLWSALQLKipdlfrdleiipalLHGDLWGGNVAEDSS-GPVIFDP-ASFY-GHSEYDLA-IAgMF--GG()}*

$2=189(164):
>tr|B1MFL2|B1MFL2_MYCA9  {|40(95)|}Putative aminoglycoside phosphotransferase OS=Mycobacterium abscessus (strain ATCC 19977 / DSM 44196) GN=MAB_0313c PE=4 SV=1 
{()ESVVVKRAVErlrvqAQW-VVPLERN-TFEARYLAAAATIV--PAfAPqivvadddlhafamtdlgpipswrddlaagrvdadVGAVLGDRLAAIHAqtaGNDSTAREF-A-SD----H-LFEALRieP----------Y-----LRYSAVRSPahaaalSELAHDLADThialVHGDVSPKNIIVTRD-GPVLTDA-ECaWY-GDPAFDLA-FCLN--HL()}*

$3=182(164):
>tr|Q056I6|Q056I6_LEPBL  {|55(59)|}Fructosamine-3-kinase OS=Leptospira borgpetersenii serovar Hardjo-bovis (strain L550) GN=LBL_0139 PE=4 SV=1 
{()NKEMAETEKEsleelFRIGVRVPEC---YGVVQTENS----------wllvmdyiesglastvredlirnlklLYRKDSNSWGWKRNnfiGTLSQKNQW-Y-STF--EK-FFWESRlsT----------Q-----LDLAFSRKLiagkdvenvKYVFQKFTEEwslnrssprlIHGDLWSGNIL-TGK-NghSYLIDP-SISF-SHPEQDLS-MLnLF--GS()}*

$4=194(164):
>tr|L2G418|L2G418_COLGN  {|67(61)|}Phosphotransferase enzyme family protein OS=Colletotrichum gloeosporioides (strain Nara gc5) GN=CGGC5_775 PE=4 SV=1 
{()LNAIHDAVPNfcpksHAHGKYQSS---PGTHFMVTDFLDLG--SSaSGgsglslasklakmhttpAPIPEgydKPMYGFPVTtccGSTPQDNSW-K-ESW--AE-FYAGNRlrS----------I-----LRAGIQNNGtdgelSKAVETVASRvvprligddhvkgmkpvlIHGDLWSGNHSWGQiagkggA-EEVVYDS-SAVY-GHSEYELG-IMrMF--GG()}*

$5=185(164):
>tr|M2PYD0|M2PYD0_CERS8  {|58(66)|}Uncharacterized protein OS=Ceriporiopsis subvermispora (strain B) GN=CERSUDRAFT_110451 PE=4 SV=1 
{()LKAIHRVAPGlaprlIACGVIdndtaESDRD-VGRPYFLSEYIDMS--TQtDAaakvlakrlatemhtykSTTG----FGFHVPtfcGRTRQENGW-F-DTW--EE-CY-DTLigG----------L-----LSSLRNT--ggydalCSDGEDIRRRvipallrplviqpvlLHGDLWSGNTGTDRrtG-EPVIFDP-SSYF-GHNEADLA-IArIF--GG()}*

$6=182(164):
>tr|J9VPD5|J9VPD5_CRYNH  {|59(62)|}Fructosamine kinase OS=Cryptococcus neoformans var. grubii serotype A (strain H99 / ATCC 208821 / CBS 10515 / FGSC 9487) GN=CNAG_02542 PE=4 SV=1 
{()LLAMSKTSTGlvpevLGFEV---SPD-GKEATMVTQWFDLS--SArGGhtqrglgsklaqmhmppPEGTEgyEGKYGFPVPthcGATEQDNTW-E-ESW--EV-FWRDRRlgN----------L-----VNRI-----gdkeiNALWEDMKRRavplllhsfspapqpviLHGDLWSGNAGYDEttS-SPVIFDP-ASYY-GHNEADLG-ITrMF--GG()}*

$7=193(164):
>tr|J0E1D8|J0E1D8_LOALO  {|34(54)|}Fructosamine kinase, variant OS=Loa loa GN=LOAG_00692 PE=4 SV=1 
{()LRTIQQLLEitkikS----FKSIS-D-SNRHCLVTEYIDLH--G-sSKpsqlgrdlarmhmhnafllkekerassfvggqekaAEP--ITQFGFHVPtccGYLPQMNEW-C-DDW--MN-FFVQNRlkY----------Q-----IDMLLEKHGdrdmLSLWPQLERKiptffkdtgnivpalVHGDLWSGNYSYCAD-GPVVFDP-ASFY-AHSEYELG-IMkMF--GG()}*

$8=196(164):
>tr|D2HUY7|D2HUY7_AILME  {|12(55)|}Putative uncharacterized protein (Fragment) OS=Ailuropoda melanoleuca GN=PANDA_016128 PE=4 SV=1 
{()LEALQTTGLvrvprP----IKVIDLP-GGGAAFVMEHLKMR--SLsSQasklgdqmadlhlynqklrdkskaeentvgrraegAEPQYVAKFGFHTVtccGFIPQVNEW-E-DDW--PT-FFARHRlqA----------Q-----LDLIEKDYAdreaRELWSQLQVKipdlfcgleivpalLHGDLWSGNVAEDDS-GPVIYDP-ASFY-GHSEFELA-IAlMF--GG()}*

_0].
[0_(1)=HSK2(9){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=203(164):
>gi|255320260|ref|ZP_05361445.1|  
{()QG---GI----QNTNYFLvNED-G-SQFVLTVFEE----MNAAQAGELVPVLEHLgNAHlPVAVPLKqaeaaIGQIAGKPAQIAPRLVGEH----PI---PA-----NTAQTAEVARAQAKlHVAlqdfdferqsyrnhsywsqvaaelrpdmdvqdqllldsvfqqfevfqakypqrplGFIHSDLFRDNTLF-EGDTLkGILDFYELNH-DELLFDIAITINdFCSDY()}*

$2=202(164):
>HSK2_consensus_seq  {|1(0)|}consensus seq 
{()-G---GI----QNTNYFLvCVN-R-KQYVLTVFEE----MDAQGAGELVPVLECLgTHGvPVAVPLKhsgqaIHSIAGKPAQIAPRMMGHH----PM---QT-----TVAQVAAIADAQAKmHVAlqdfplereyrrnhqywsgvaeqlkpnmsqddqtlleqvyqafnaktaqysnrptGFIHSDLFRDNTLF-EGDQLqGILDFYELNQ-DEWLFDIAITINdFCTEY()}*

$3=203(164):
>gi|479975986|gb|ENV20029.1|  
{()QG---GI----QNTNYFIiCRD-G-TQFVLTLFEN----MDAQAAGELIPVLEHLgEYGlAVPVPLKyegqsIHTLKDKPAQIAPRMIGKH----PM---PS-----SIAQVEQIAMAQAKmHIAlkdfplqrkiardhdywynvakqikptlstadkalldnllglyqaltqmypnrpkGWIHSDLFRDNTLF-EGDNLkGILDFYEMNQ-DELLFDIAITLNdFCTEY()}*

$4=203(164):
>gi|480028929|gb|ENV69308.1|  
{()QG---GI----QNTNYFLrCDN-Q-NEYVLTIFEE----LDQQAAGELIPVLQHLgQAGlPVAVPLSysgqaIHQLKEKPAQIAPRLIGQH----PT---LA-----TVAQVEAIAIAQAKmHVAlqnfplqrdhvrdhaywagvarqlkpsmhvadqallskllglyealtavypqrpkGFIHSDLFRDNTLF-VDDQLqGILDFYELNQ-DELLFDIAITLNdFCTDF()}*

$5=203(164):
>gi|480214874|gb|ENX50506.1|  
{()QG---GI----ENTNYFLvTQD-H-QQYVLTVFEE----LDLQGANELPPVLQYLgQQDiPVAVPLSyaeqfIHIIADKPAQIAPRIAGKH----PI---PP-----TLKQIFAIGQAQAQlHLAlqnvqferqsyrnhaywtsvadqlkaymsqddlelldrvyrlfnqqrerhcdlpkGWIHSDLFRDNTLF-EGDQLqGILDFSEMNQ-DDLLFDIAIAINdFCTEH()}*

$6=203(164):
>gi|480340253|gb|ENX60443.1|  
{()QG---GI----QNTNYFLvDQS-Q-KHYVLTVFEE----LDAEGAGELVPVLDCLgEAGvPVAVPLKhsgqaIHTIADKPAQIAPRLMGEH----PE---DA-----SIIQIQAIAQAQAKlHLAlqgfplerdfnrnhqywsdvaeqlkpqmnaqdqallaqvfqqfaqvtqqhpdrptGFIHSDLFRDNTLF-EGDQLqGILDFYELNQ-DEWLFDIAISINdFCTAY()}*

$7=202(164):
>gi|479988743|gb|ENV31764.1|  
{()QG---GI----QNTNYFMvCED-Q--QYVLTVFEE----MNQEQAGELVPVLDHLsAHGvPVAVPLKhsgqaIHFIANKPAQIAPRVLGSH----PE---ST-----SIEQVQAIAKAQAQlHVAlqdfplqrhynrnhsywkqvahdlklkmdqddaelleqvltlfqnkknqypdrptGFIHSDLFRDNTLF-NGDQLqGILDFYELNK-DEFLFDIAITTNdFCTDY()}*

$8=202(164):
>gi|480188013|gb|ENX24271.1|  
{()QG---GI----QNTNYFLvCED-Q--QYVLTVFED----MDEQAAGELVPVLEHLgHAGlAVPVPLShsgkaIHSIKDKPAQIAPRLMGEH----PM---PS-----TVAQVEAIAVAQAKmHVAlkdfklernfvrdhaywlavsqeikpslspadkvllgkllglyealtavypdrprGFIHSDLFRDNTLF-DGDQLnGILDFYELNK-DEWLFDIAITLNdFCTEY()}*

$9=203(164):
>gi|126643252|ref|YP_001086236.1|  
{()QG---GI----QNTNYFLvDVN-R-KQYVLTVFEE----LDAQGAGELIPVLEQLgTHDvPVAVPLKhsgqaVHFIAGKPAQIAPRLMGHH----PM---QT-----TVAQVAAIADAQAKlHVAlqdfplereyrrdhqywtgvaeqlkpnmtqddqtlleqvyqafnaktaqysnrptGFIHSDLFRDNTLF-EGEQLqGILDFYELNQ-DELLFDIAITINdFCTEY()}*

_0].
[0_(1)=IDHK(11){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=164(164):
>IDHK.fas consensus seq 
{()CDQFISAPKGMVMCVFTLSXXXXXFPYVFKVIKXDKFSPQREHVMACYQLVKEHRVGRMADTXQEYSNVRHLYXIERRMXXTPLNLWLQQCFDSGXWQSRQAIEDYGCAIKQMASANIFPGDMLWKNFGVTRHGRVIFYDYDEICYTVFPEEXXXFXRHWLCAD()}*

$2=247(164):
>PDB|3lcb_IDHK_APK 
{()CNQFIEPGxRGMVMLVFTLxG-----FDRVFKVIK-DKFAPQexxxxxxmAAHVRACYQLVKEHxRVGRMADT-QEFENExxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRHLY-IERRM--VPLNIWLEQVXXxxxxxxxxxxxxxxxEG-QQLRDAIEExxxxYGNAIRQLAAANIFPGDMLFKNFGVTRHGRVVFYDYDEICYmSxxxxxxxxxxxxxxxxxxxxxxxxxxxxxVFPEE---F-RHWLCAD()}*

$3=247(164):
>ORIGPDB|3lcb_IDHK_APK 
{()NEQFIEAPgiRGMVMLVFTLpG-----FDRVFKVIK-DKFAPQkemxxxsAAHVRACYQLVKEHdRVGRMADT-QEFENFvlekrhispalmelllqeaaekitdlgeqivixxxxxxRHLY-IERRM--VPLNIWLEQVXXxxxxxxxxxxxxxxxEG-QQLRDAIEExxxxYGNAIRQLAAANIFPGDMLFKNFGVTRHGRVVFYDYDEICYmTevnfrdippprypedelasepwysvspgdVFPEE---F-RHWLCAD()}*

$4=204(164):
>lcl|UniRef90_C9YED0  {|1(42)|}unnamed protein product 
{()-DKFRIAAgiKGMVMLVFDLpS-----YPYVFKVIK-DYFPHQketxxxtREQVMGKYQLVKQHdRVGRMADT-LDYGDVafprdrfseeliaelqqfapsqleisdrdgdgrtevilKHVY-IERRM--IPLNMYLQEAFDaggadrnnqspaalrAR-HQIERSVIDxxxxYGYAIKDMVAANIFPGDMLWKNFGITRNNKVVFYDYDEIEYlT-----------------()}*

$5=205(164):
>lcl|UniRef90_A4CAN3  {|0(42)|}unnamed protein product 
{()KEQFESAAgtKGMVMTVFTLpS-----FPYVFKVIK-DKFGEGkpfxxxgRDTVLARYQLVKRHdRVGRMADT-IEYSNVafplctisdslleelkttvasalefrdnlliixxxxxxKHLY-IERRM--TPLNLYLQTAXXxxxxxxxxxxxxxxxTL-EQTRTVMDExxxxYGQALKEMISVNIFPGDMLLKNFGVTSHQRVIFYDYDEVQYlT-----------------()}*

$6=205(164):
>gi|197122246|ref|YP_002134197.1|  {|0(42)|}unnamed protein product 
{()EDRFVPARgdRGLVMCVFTLpG-----LDVIFKVIR-DRFAPPkqtxxxtRREVMDRYRHVFRHdRAGRLVDA-QEYEHLafpagrfapalleelrtecgdgvrvadgevaixxxxxxRHLY-AERRV--TPLNLFVREAXXxxxxxxxxxxxxxxxDE-WTARQAVLDxxxxFGCALRDLAATDTFPGDLLLKNFGVTRHGRVIFYDYDELTRvT-----------------()}*

$7=205(164):
>lcl|UniRef90_Q487A4  {|0(42)|}unnamed protein product 
{()DDQFELAAgiKGMVMSVFTLpS-----YPYVFKIIK-DKFSPSknixxxtKKDVKGKYRLVKLHdRVGRMADT-MEYSEVafpksrfndellaelqkvapsiiryegegeealiiixxEHLY-IERRM--VPLNLYLMDAXXxxxxxxxxxxxxxxxLK-NKAQQKIDDalfgYGQAIKQLISADIFPGDMLLKNFGVTRHGRVIFYDYDEIAYmN-----------------()}*

$8=205(164):
>gi|81564313|sp|Q6NCX0.1|ACEK_RHOPA  {|0(42)|}unnamed protein product 
{()KEKLATAPgfKGTVAIAFTMpS-----SAYVLKIIR-DHPTDDykfdyfdgLDEVLRKYNLVHEIdRAGSMLDN-IIYSNVkldramfapelldelleagigtvtldrgalvfxxxxxRHLI-VQIKL--TPLPLYLANAXXxxxxxxxxxxxxxxxSA-AESRAAVINxxxxLGDCIKNNAAADIFNKDLDGRNYGVSRIRKVYLFDYDAVEPlT-----------------()}*

$9=205(164):
>lcl|UniRef90_A3I303  {|0(42)|}unnamed protein product 
{()KDQFVVAPgiKGMVMTVFTLpS-----LNIVFKLIK-DHFEPPknmxxxtRQEVREKYKLVSLHdRVGRMADT-HEFEYFKipkdrispelvrelrnttnsllnftethlilxxxxxxkHLY-TERKM--EPLNLYLGHCXXxxxxxxxxxxxxxxxST-EEGEKVVEDxxxxYGRAILQLAQANIFPGDMMTKNFGLTRQKRVIFYDYDEIEFlT-----------------()}*

$10=205(164):
>lcl|UniRef90_F3KDJ5  {|0(42)|}unnamed protein product 
{()DDQFIVTPgiKGMVMTVFTMpN-----FPYVFKVIK-DKFGGTktlxxxtREGVKERYLLVKRHdRVGRMADT-MEYSRValpidrltdellqelketapsllefdgdrviixxxxxxNHLY-AQRKM--IPLNLYIDDAXXxxxxxxxxxxxxxxxEP-EEIEAAIYDxxxxYGLALKEMVSANIFPGDMLLKNFGLTRHKRVVFYDYDEVQYlS-----------------()}*

$11=94(164):
>gi|213421363|ref|ZP_03354429.1|  {|111(42)|}unnamed protein product 
{()---------------------------------------------------------------------RHLY-IERRM--VPLNIWLEQVXXxxxxxxxxxxxxxxxEG-QQLRDAIEExxxxYGNAIRQLAAANIFPGDMLFKNFGVTRHGRVVFYDYDEICYmT-----------------()}*

_0].
[0_(1)=Kdo(8){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=178(164):
>KDKA_PASPI/28-232 
{()KIIGSAQGRGTtwfvqgeklAMALRHYrR-GglfG-------KLVADSYL---FsgWDktrSVAEFSLLNHLIahQVNVPKP-VAaravrlgwfrYQ---ADIL-VEkIANSRDLVGI----LGQETL-SQQVWFDVGAMIKRMHDAGVCHTDLNCHNIILDDHKTVWIIDFDK-CYRLEGanwqEKNLARLHRSFIkEQ()}*

$2=177(164):
>Q8RJK9_VIBCL/45-248 
{()KVLGSATGRGTtwfvqlqhtQGALRHYrR-GglfG-------KLVADSYG---FtgWEktrSYQEFMLLNHLRdaGVNVPRP-IAarvqkhgllYK---ADLL-SEkVPNARDLVSI----LQESPI-SDELYRKIGSEIRKMHDAQVNHTDLNIHNILIDEQEKVWIIDFDK-CYVQSGhswkQDNLNRLKRSFEkEV()}*

$3=177(164):
>KDKA_VIBVU/29-232 
{()KVVGSATGRGTtwfvqlpkiTAALRHYrR-GglfG-------KLVKDHYW---FqsWSatrSFAEFHLLKQLReaGVNVPRP-IAayamrkglfYQ---ADLL-SErIANAQDLVTI----LQKHSL-SAELYQKIGVEIAKMHRVGVNHTDLNIHNILIDAQETIWIIDFDK-CYPQAGdgwkQENLDRLKRSFNkER()}*

$4=184(164):
>KDKA_PASMU/28-238 
{()RILGSAQGRGVtwflqtkdlfgvNAALKHYyR-GglwG-------KINRDYYR---FstLSnarSFAEFSLLKQLHeaGLAVPKP-LGacvekvafgfYR---ADLL-TEkIEHAQDLTVY----LQQGKL-TEQDWHKVGQLIHQLHSLQVCHTDLNAHNILVqqrDDGRKFWLIDFDK-CAHQLGerwkSENLNRLHRSFMkEV()}*

$5=185(164):
>KDKA_HAEIN/28-239 
{()RVIGSAKGRGItyflqtedwfgvNCALRHYyR-GglwG-------KLNKDRYR---FsaLEttrSFAEFHLLQRLYeaGLPVPKP-IAariqkgklgicYQ---ADIL-TEkIENAQDLTAL----LQTQTL-PKETWMQIGRLIRKLHDLQICHTDLNAHNILLqqtEQGQKCWLLDFDK-CGEKSAdfwkVQNLNRLKRSFEkEV()}*

$6=183(164):
>Q8E8I7_SHEON/32-243 
{()-VVGSSKGRYTtwfvafehsHWVLRHYwR-GgmmE-------KFSKDAYF---YtgLEntrAMGELRLLDALYreQFAVPKP-IAanivrdglfYR---ADII-IErVDGAEDLVAK----LTKGTM-TQAQWHALGATIAQFHRRGVYHADLNAKNILWQpqqtdsAQERFYLIDFDR-GELKTpNakwqKANLDRLLRSFNkEQ()}*

$7=173(164):
>KDKA_XYLFA/39-241 
{()----SEGGRGSawfveasfgNAVLRQYrR-GgmiA-------MLNRDRYF---WcgGHrtrSVLEFRLMRELIsrGLPVPTP-LAacyvrygvqYR---AAIL-MErLEGVSSLAMC----VRGNS--KETHWEQIGRMISRFHREGLDHADLNAHNILLDQAGQCWLIDFDR-GALRIpAtkwrERNLARLLRSLLkIR()}*

$8=173(164):
>KDKA_XANAC/39-241 
{()----DAGGRGGawfvdapfgHSVLRQYlR-GgmaA-------RVSRDRYL---WkgAGrtrSFAEFRLMRELIkrKLPVPRP-LAacylreglgYR---AALL-MErLENVRSLAEH----AQVAG--RGAPWEATGQLIARFHRAGLDHADLNAHNILFDAGGHGWLIDFDR-GVLRIpAtrwrERNLKRLHRSLLkLR()}*

_0].
[0_(1)=MalK(8){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=276(164):
>sp|Q7DAF6|MAK2_MYCTO  {|136(43)|}Maltokinase OS=Mycobacterium tuberculosis (strain CDC 1551 / Oshkosh) GN=mak PE=3 SV=1 
{()R----VCDaE-QsntsviFDRR-----------AILKVFrRVssginpdiE-----LNRVLTRA-GNPHVARLLGAYQFGRPnrsptdalayA---LGM-VTEYEanaaegwamatasvrdlfaegdLYAhevggdfagesyrlgeavasvhATladslgtA-----------QatfpvdrmlaRL--------SStvaVVpELreyaptieqqFqklaaeaitvqR--V-HGDLHLGQV-LRTpeswllidfegepgqplderrapdsplrD----VagvlRsfEYAaYG----plvdqatdkqlaararewvernraafcDGYAVASGID-----P()}*

$2=270(164):
>sp|Q7WUM3|MAK_ACTMI  {|125(42)|}Maltokinase OS=Actinoplanes missouriensis GN=mak1 PE=1 SV=1 
{()----RVFDaE-QsntsviFDED-----------AILKLFrRVtcgvnpdiE-----LNRVLGRA-GNPHVARLLGSLQSADDsgpcS---LGM-VTEYAansaegwamatasardlfadaemrad---evggdfqgesyrlgeavasvhRTlaE-----------ElgtgpapfpldavlaRVRTAAAAVPE----LqqfvpaitarFealtgaevvvqR--V-HGDLHLGQV-LRTpeawllidfegepgqplderrmpdsplrD----VagvlRsyEYAaYQL--LvdqdddehlaararewvdrnraafcDGYTNVAGADP-----()}*

$3=275(164):
>sp|A0R6D9|MAK_MYCS2  {|124(42)|}Maltokinase OS=Mycobacterium smegmatis (strain ATCC 700084 / mc(2)155) GN=mak PE=3 SV=1 
{()----RVFGaE-QsntsvvFGED-----------AIFKLFrRItpgvhpdiE-----LNRVLARA-GNPHVARLLGSFE-TEWegepyA---LGM-VTEFAansaegwdmattstrdlfaegdLYAeevggdfageayrlgeavasvhAClahelgtE-----------EvpfpadvmaqrlaaavdAV------------pELrehvpqieerYhkladttmtvqR--V-HGDLHLGQV-LRTpkgwllidfegepgqplderrrpdtpvrD----VagilRsfEYAaHQR--LvdqagddddrarqlaararewvtrncasfcDGYAAEAGTDP-----()}*

$4=274(164):
>sp|A1UNJ0|MAK_MYCSK  {|127(43)|}Maltokinase OS=Mycobacterium sp. (strain KMS) GN=mak PE=3 SV=1 
{()----RVSSaE-QsntsviFDQD-----------AILKVFrRItpginpdiE-----LNRVLARA-GNPHVARLLGSFE-TTLdrepyA---LGM-VTEFAansaegwdmaltstrdlfaegdLYAdevggdfageshrlgeavasvhSTlaaelgtS-----------QvpfpldtvleRLQSVADAVPE----LqphaqsieerYrkladqeitvhR--V-HGDLHLGQV-LRTtegwllidfegepgqplderrrpdspmrD----VagmlRsyEYAaYQR--LierggdaqhdkqlaararewvnrnvssfcDGYAAASGTDP-----()}*

$5=261(164):
>sp|E6TMM3|MAK_MYCSR  {|127(42)|}Maltokinase OS=Mycobacterium sp. (strain Spyr1) GN=mak PE=3 SV=1 
{()---AKVSGaE-QsntsviFGKD-----------AMLKVFrRVtpgvnpdiE-----LNRVLARA-GNPHVATLLGAFET---sscA---LGM-VTAFAansaegwdmatasvhdlfan---evggdfadesrrlgaavasvhATladtlgtS-----------VtqfpidtvleRLRSA-------tra-ApELapyapqieqrFrrladqpirvhR--I-HGDLHLGQV-LRTpeswllidfegepgqpleerrrpdsplrD----VagvlRsfEYAaYQQ--VvtgggdpqmadrarswvdrnvdafcAGYAAVAGEDP-----()}*

$6=278(164):
>sp|Q73U39|MAK_MYCPA  {|131(43)|}Maltokinase OS=Mycobacterium paratuberculosis (strain ATCC BAA-968 / K-10) GN=mak PE=3 SV=1 
{()---AHVSDaE-QsntsviFDRD-----------AIFKVFrRVssginpdiE-----LNRVLGRA-GNPHVARLLGTYEMAGAdgtpeTAWPLGM-VTEFAanaaegwamatasvrdlfaegdLYAhevggdfagesyrlgeavasvhATlaetlgtS-----------QaafpvdnvlaRL--------SStaaLVpELteyaatieerFaklatetitvqR--V-HGDLHLGQV-LRTpeswllidfegepgqpleerrapdsplrD----VagvlRsfEYAaYGP--LveqgaqntdkqlaararewvernrtafcDGYAAASGIDP-----()}*

$7=276(164):
>sp|A0PWI9|MAK_MYCUA  {|136(43)|}Maltokinase OS=Mycobacterium ulcerans (strain Agy99) GN=mak PE=3 SV=1 
{()----RVSDaE-QsntsviFdrGQA-----------AILKVFrRVssginpdiE-----LNRVLGRAHN-PHVARLLGTYEIGIPgeppEAACpLGM-ATAYAanaaegwamatasvrdlfaegdLYAhevggdfagesrrlgeavasvhATlaeqlgtA-----------QatfpvdhvlaRL--------SStaaAVpELqqyagtieerFvklvdetisvqR--V-HGDLHLGQV-LRTpeswvlidfegesgqplrerrapdsplrD----VagvlRsfEYAaYGP--LvdhaddkqlaararewitrnrtafcEGYAAASGNDP-----()}*

$8=271(164):
>sp|A1TH50|MAK_MYCVP  {|128(42)|}Maltokinase OS=Mycobacterium vanbaalenii (strain DSM 7251 / PYR-1) GN=mak PE=3 SV=1 
{()----KVSSaE-QsntsviFGKD-----------AMLKVFrRVtpginpdiE-----LNRVLAQA-GNRHVARLLGSFEtsWAGPgtdrcA---LGM-VTAFAansaegwdmatasaremfadvvgs---dfadesyrlgnavasvhATlaealG-----------TstepfpvdtvlaRLQSAARSAPE----LagraaaveerYrrldgraitvqR--V-HGDLHLGQV-LRTpddwllidfegepgqplderrrpdsplrD----VagvlRsfEYAaYQK--LvelapeqdadgrladrarnwvdrnsaafcAGYAAVAGDDP-----()}*

_0].
[0_(1)=MTRK_Bac(12){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=164(164):
>MTRK_Bac.fas consensus seq 
{()LTCXQEIGDGNLNYVFHIYXDQQTKSLIIKQALPXYWXPLLKRAWIESNALRREYVPHYVRVYYSDEEXXMAVTVMEDLSLTIARKGLIQGXXENYPHLSXXXQHIGEYLAKTLFETLIHGDLHTGSIFASEQXETKVIDPEFAFYXGPFGFDVGQFIANLFFN()}*

$2=240(164):
>PDB|2olc_MTRKBac_ELK 
{()LTC-QEIGDGNLNYVFHIY-DXXXRALIIKQAVP-YxxxxxxxW-PLtIDRARIESSALIRxxEHVPHLVxRVFYSDTE--MAVTVMEDLSxLKIARKGLIEG--ENYPHLS---QHIGEFLGKTLF-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsTLIHGDLHTGSIFASEH-ETKVIDPEFAFY-GPIGFDVGQFIANLFLN()}*

$3=240(164):
>ORIGPDB|2olc_MTRKBac_ELK 
{()LTC-QEIGDGNLNYVFHIY-DXXXRALIIKQAVP-YxxxxxxxW-PLtIDRARIESSALIRqgEHVPHLVpRVFYSDTE--MAVTVMEDLShLKIARKGLIEG--ENYPHLS---QHIGEFLGKTLFyssdyalepkvkkqlvkqftnpelcditerlvftdpffdhdtndfeeelrpfveklwnndsvkieaaklkksfltsaETLIHGDLHTGSIFASEH-ETKVIDPEFAFY-GPIGFDVGQFIANLFLN()}*

$4=240(164):
>tr|C2QGI4|C2QGI4_BACCE Methylthioribose kinase OS=Bacillus cereus R309803 GN=mtnK PE=3 SV=1 
{()VVC-HEIGDGNLNYVFKLD-DGVXKSIIIKQALP-YakvvgesW-PLsIKRATIESKALQIfaKYVPEYVpVVYSHDEE--LAVTVIEDLSrLTITRKGLIDG--EEYPLLS---QHIGRFLANVLFytsdfgldseekrilegafvnpdlckitedlvftdpfghcdtndyepelqltvdelwsdktlklkvaqykykfltrkEALIHGDLHTGSIFSSPS-ETKVIDPEFATY-GPFGFDIGQFIANLLVN()}*

$5=241(164):
>tr|B4AES1|B4AES1_BACPU Methylthioribose kinase OS=Bacillus pumilus ATCC 7061 GN=mtnK PE=3 SV=1 
{()LTC-TEIGDGNLNYVFHIF-DHKQeKGLIIKQALP-YakvvgesW-PLtLDRARIESTALIKqsEYTPHLVpAVYYSDTA--LAVTAMEDLShLEIVRKGLIAG--KQYPHLS---DHVGEFLGKTLFytsdfatnpkikkqfvkqftnpdlcditeklvftdpffdsgtndfeeelreaaealwadlevqakaaelkrifltsaETLVHGDLHTGSIFASET-ETKIIDPEFAFY-GPFGFDIGHFIANLFLN()}*

$6=241(164):
>tr|B7GLF7|B7GLF7_ANOFW Methylthioribose kinase OS=Anoxybacillus flavithermus (strain DSM 21510 / WK1) GN=mtnK PE=3 SV=1 
{()LSS-QEIGDGNLNLVFRIV-NERTnDSIIIKQALP-YakvvgesW-PLtLKRATIESQALRTfaSYVPHYVpKVYYSDEI--LAITVMEDLShLQIARRGLIEG--KTFPKLS---EHIGEFVAKTAFytsdfgmnqqqkkklvqqftnpelckitedlvftdpffdhdtnnfeaelradveqlwhddelkretaklkrlfltkaDVLLHGDLHTGSIFANDE-ETKVIDPEFAFY-GPIGFDLGQFFANLLLN()}*

$7=241(164):
>tr|M3EMJ7|M3EMJ7_9BACL Methylthioribose kinase OS=Planococcus halocryophilus Or1 GN=mtnK PE=3 SV=1 
{()LSC-REIGDGNLNYVFQIE-D---SKtgtrVIIKQALP-YakiigesW-PLtLKRATIEANVLRKhgEFVPSLVpVVYAADET--LAITVMEDLShLVIAREGLING--QNFPRLS---QDIGEYLAQTLFhtsdyglhpfekkrlavefsnpelckitedlvftdpffdletnnyepqlqsaaeaiwrdqklqlevaklkksfltesEALLHGDLHTGSIFASTT-ETKVIDPEFGFY-GPIGFDVGLFLGNLIFQ()}*

$8=241(164):
>tr|D5DCZ3|D5DCZ3_BACMD Methylthioribose kinase OS=Bacillus megaterium (strain DSM 319) GN=mtnK PE=3 SV=1 
{()LTA-REIGDGNLNLVFHII-NDQTnKSIIVKQALP-YakvvgesW-PLsLNRATIESNALKQfgAFTPELVpAVYYHDET--LAVTVMEDLShLTISRAGLIQG--ESYPLIS---QHIGSFLGHIAFktsdfalkpqdkkeevvkysnpdlcnitedlvftdpffdidtnefekalrpdveelwndgdvklqaaklkykfltnaQTLIHGDLHTGSIFASSQ-ETKVIDPEFAFY-GPFGFDLGQFVANLFLN()}*

$9=241(164):
>tr|Q2B5J2|Q2B5J2_9BACI Methylthioribose kinase OS=Bacillus sp. NRRL B14911 GN=mtnK PE=3 SV=1 
{()LAT-EEIGDGNLNLVFRIK-ENGTgKSLIIKQALP-FakvvgesW-PLtLKRAAIEAEALKlfKKLAPDFVpEVFYNDEE--LAVTVMEDLShLEISRTGLISG--KEYPLLS---GHLGNYLARTLFytsdygllqadkkklakdfynpelckitedlifadpyfnsptndfeeelkgdaealwadsvlkleaarlrkiflneaEALLHGDLHTGSIFASDA-ETKVIDPEFAFY-GPAGFDIGQVIANLLFQ()}*

$10=241(164):
>tr|I3E801|I3E801_BACMT Methylthioribose kinase OS=Bacillus methanolicus MGA3 GN=mtnK PE=3 SV=1 
{()LTS-SEIGDGNLNLVFRVK-DQLSgKSVIIKQALP-YakvvgesW-PLtLKRAKIEADALKlfRELAPEFVpEVYFSDET--FAITVMEDLShLSIARTGLIQG--EEYPLIS---KHLGKYLAKTLF---ltsdfglepsdkkelvqkfinpelckitedlvftdpffdsetnsfeeelidevkklwsdekvkletallkqsflteaeslLHGDLHTGSIFASKT-ETKVIDPEFAFY-GPAGFDLGQVIANLLFQ()}*

$11=241(164):
>tr|D3FZB4|D3FZB4_BACPE Methylthioribose kinase OS=Bacillus pseudofirmus (strain OF4) GN=mtnK PE=3 SV=1 
{()LLC-KEIGDGNLNYVFHLS-DSLSgQSLIIKQALP-YakvvgesW-PLtLRRAKIEAEALQKaaETVPSLVpNVIHTDDD--LAITIMEDLSdYTILRKGLIDG--ETYPNLG---RHIGIYLAKTIfhsspfgesqqeikeisvrftnpelckitedlvftdpffdhdtndfedhlkdtvnelwkneelkleaaklkytfltkSEALIHGDLHTGSVFVTAE-ETKVIDPEFAFY-GPAGFDLGLFIANILLN()}*

$12=239(164):
>tr|C4KZ49|C4KZ49_EXISA  {|1(0)|}Methylthioribose kinase OS=Exiguobacterium sp. (strain ATCC BAA1283 / AT1b) GN=mtnK PE=3 SV=1 
{()-EC-EEIGDGNLNLVFRIR-Q---GetxSLIVKQALP-YakvvgesW-PLtLDRSWIEQLALREfsKVVPQFVpDVLGSDRE--LAYTILEDLSaFEIVRTGFLKG--ESYPNLA---RHVGTFLGETLFatsdyangplvkkqiekeyynpelcditekliftdpyrdadsnqieaalrpdveqlwsdlalkievtklkvgfvtkhEALLHGDLHTGSIFATRQ-ETKIIDPEFAFY-GPFGFDIGQIIAHLVFS()}*

_0].
[0_(1)=MTRK_Plant(6){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=164(164):
>MTRK_Plant.fas consensus seq 
{()SWVIKEVGDGNLNFVYIVQXGSSXGSFVVKQALPYVGESWTERAYFEASTLRKHCPEHVPEVYHFDRXTXMSLIGMRYLEHIILRKGLIAXXGVEYPXXXFLADHMSDYMAKTLFQALIHGDLHTGSVMCTQDXSTQVIDPEFSFYXGPMGFDIGALGNLILAY()}*

$2=244(164):
>PDB|2pyw_MTRKPlant_ELK  {|0(1)|} 
{()SLVIKEVGDGNLNFVFIVV-GSS-GxSLVIKQALPYxxxIGESWpmTxERAYFEATTLRKHxxxNSDHVPEVYHFDR-T-MALIGMRYLExxHIILRKGLIA--GIEYP---FLADHMSDYMAKTLF-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtALIHGDLHTGSVMVTQD-STQVIDPEFSFY-GPMGFDIGAxLGNLILA-()}*

$3=245(164):
>ORIGPDB|2pyw_MTRKPlant_ELK 
{()DLVIKEVGDGNLNFVFIVV-GSS-GxSLVIKQALPYIrciGESWpmTkERAYFEATTLRKHgnlSPDHVPEVYHFDR-T-MALIGMRYLEppHIILRKGLIA--GIEYP---FLADHMSDYMAKTLfFtsllyhdttehrravtefcgnvelcrlteqvvfsdpyrvstfnrwtspyldddakavredsalkleiaelksmfceraQALIHGDLHTGSVMVTQD-STQVIDPEFSFY-GPMGFDIGAyLGNLILAF()}*

$4=245(164):
>tr|D8R980|D8R980_SELML Putative uncharacterized protein OS=Selaginella moellendorffii GN=SELMODRAFT_88229 PE=3 SV=1 
{()DWAVKEVGDGNLNFVYIVT-GAS-GxSFVLKQALPYVrcvGDSWplSlERAYFEVTALQEHgkhCPEHVPEVYHFDH-P-MALFAMRYLAppHIILRKGLIA--GTKYP---LLAEHMSDYMARTLFntsllatsttehkaavakfcgnvelcrlteqvvftepymiasnnhwttpqldedaksireddylkleiaslkskfceksQALIHGDLHTGSVMVTQE-STQVIDPEFAFY-GPMGFDIGAfLGNLALAY()}*

$5=246(164):
>tr|A4S069|A4S069_OSTLU Predicted protein (Fragment) OS=Ostreococcus lucimarinus (strain CCE9901) GN=OSTLU_3520 PE=3 SV=1 
{()SWSAKEIGDGNINFVYIVT-GAS-AnkSVIVKQGLPFVrcvGESWplTqERVRYEAEALIQAhkfCPEHVPEVYVYDA-T-MATIVMRYLEppHIILRGGIIE--GKVYP---KLAEHVGEYLATTLYkssalavggaelrrarqafgqnedmcelteqviftepygkadnnhwttpqlddivgeiqsdaalkrainalkekfmteaQALLHGDLHTGSLMCTET-TTFAIDNEFAFY-GPMGFDIGAfLANLLLAY()}*

$6=245(164):
>tr|K7U7S9|K7U7S9_MAIZE Uncharacterized protein OS=Zea mays GN=ZEAMMB73_530100 PE=3 SV=1 
{()SIEIKEVGDGNLNFVYIVQ-SEA-GxAIVVKQALPYVrcvGDSWpmTrERAYFEASTLREHgrlCPEHTPEVYHFDR-T-LSLMGMRYIEppHIILRKGLVA--GVEYP---LLADHMSDYMAKTLFftsllynnttdhkngvakysanvemcrlteqvvfsdpyrvskfnrwtspyldkdaeavreddelklevaglksmfieraQALIHGDLHTGSIMVTEG-STQVIDPEFGFY-GPMGFDIGAfLGNLILAY()}*

_0].
[0_(1)=PI3K_inos(9){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=185(164):
>sp|Q61194|P3C2A_MOUSE  {|1110(391)|}Phosphatidylinositol 4-phosphate 3-kinase C2 domain-containing subunit alpha OS=Mus musculus GN=Pik3c2a PE=1 SV=2 
{()F-FSSN-AMPLK-VTMVNADPlGEEINVMFKV--GE-----D--LRQDMLALQMIKimdkiwlkegLDLrMVIFRCLSTGRDRG--MVELVpASDTLRKIQVeygvtgsfKDKPLAEWlrkynpseeeyekASENFIYSCAGCCVATYV--LGICDRHNDNIMLRSTGHMFHIDFGKFLGHAQMfgsfkrdrapF----VL---TSDM()}*

$2=190(164):
>sp|P48736|PK3CG_HUMAN  {|801(111)|}Phosphatidylinositol 4,5-bisphosphate 3-kinase catalytic subunit gamma isoform OS=Homo sapiens GN=PIK3CG PE=1 SV=3 
{()KVMASK-KKPLW-LEFKCADPtalsNETIGIIFKH--GD-----D--LRQDMLILQILRimesiwetesLDLcLLPYGCISTGDKIG--MIEIVkDATTIAKIQQstvgntgafKDEVLNHWlkekspteekfqaAVERFVYSCAGYCVATFV--LGIGDRHNDNIMITETGNLFHIDFGHILGNYKSflginkervpF----VL---TPDF()}*

$3=190(164):
>sp|O02697|PK3CG_PIG  {|801(111)|}Phosphatidylinositol 4,5-bisphosphate 3-kinase catalytic subunit gamma isoform OS=Sus scrofa GN=PIK3CG PE=1 SV=2 
{()KVMASK-KKPLW-LEFKCADPtalsNETIGIIFKH--GD-----D--LRQDMLILQILRimesiwetesLDLcLLPYGCISTGDKIG--MIEIVkDATTIAKIQQstvgntgafKDEVLSHWlkekcpieekfqaAVERFVYSCAGYCVATFV--LGIGDRHNDNIMISETGNLFHIDFGHILGNYKSflginkervpF----VL---TPDF()}*

$4=213(164):
>sp|P0C5E7|AGE1_CAEBR  {|863(83)|}Phosphatidylinositol 3-kinase age-1 OS=Caenorhabditis briggsae GN=age-1 PE=3 SV=1 
{()V-LGSA-KQPLR-LVWKNKNPksdlHLPFcEVIFKN--GD-----D--LRQDMLVLQVLEvmdtiwkaanIDCcLSPYGVLPMGEMIG--IIEVVpNCKTIFEIQHkaglvntaaRSvdsNFMNKWirkqcgfvdekkkykkgggptvdsaqatkkyfeSVDRFLYSCVGYSVATYI--MGIKDRHSDNLMLTEDGKYFHIDFGHILGHGKTklgiqrdrqpF----IL---TEQF()}*

$5=218(164):
>sp|Q94125|AGE1_CAEEL  {|858(106)|}Phosphatidylinositol 3-kinase age-1 OS=Caenorhabditis elegans GN=age-1 PE=1 SV=6 
{()V-LGSA-KRPLM-LHWKNKNPksdlHLPFcAMIFKN--GD-----D--LRQDMLVLQVLEvmdniwkaanIDCcLNPYAVLPMGEMIG--IIEVVpNCKTIFEIQVgtgfmntavRSidpSFMNKWirkqcgiedekkkskkdstknpiekkidntqamkkyfeSVDRFLYSCVGYSVATYI--MGIKDRHSDNLMLTEDGKYFHIDFGHILGHGKTklgiqrdrqpF----IL---TEHF()}*

$6=185(164):
>sp|O75747|P3C2G_HUMAN  {|893(367)|}Phosphatidylinositol 4-phosphate 3-kinase C2 domain-containing subunit gamma OS=Homo sapiens GN=PIK3C2G PE=1 SV=3 
{()Y-FTSN-ALPLK-ITFINANPmGKNISIIFKA--GD-----D--LRQDMLVLQLIQvmdniwlqegLDMqMIIYRCLSTGKDQG--LVQMVpDAVTLAKIHRhsgligplKENTIKKWfsqhnhlkadyekALRNFFYSCAGWCVVTFI--LGVCDRHNDNIMLTKSGHMFHIDFGKFLGHAQTfggikrdrapF----IF---TSEM()}*

$7=185(164):
>sp|O70167|P3C2G_MOUSE  {|954(367)|}Phosphatidylinositol 4-phosphate 3-kinase C2 domain-containing subunit gamma OS=Mus musculus GN=Pik3c2g PE=2 SV=1 
{()Y-FTSN-ASPLK-ITFINANPmGKNISVIFKA--GD-----D--LRQDMLALQIIQvmdnawlqegLDMqMITYGCLSTGRAQG--FIEMVpDAVTLAKIHLhsgligplKENTIKKWfsqhnhlkedyekALRNFFYSCAGWCVVTFI--LGVCDRHNDNIMLTKSGHMFHIDFGKFLGHAQTfggikrdrapF----IF---TSEM()}*

$8=185(164):
>sp|O70173|P3C2G_RAT  {|953(367)|}Phosphatidylinositol 4-phosphate 3-kinase C2 domain-containing subunit gamma OS=Rattus norvegicus GN=Pik3c2g PE=2 SV=1 
{()Y-FTSN-ALPLK-ITFINANPmGKNISVIFKA--GD-----D--LRQDMLVLQIIQvmdnvwlqegLDMqMIIYGCLATGKAQG--FIEMVpDAVTLAKIHLhsgligplKENTIKKWfsqhnhlkedyekALRNFFYSCAGWCVVTFI--LGVCDRHNDNIMLTKSGHMFHIDFGKFLGHAQTfggikrdrapF----IF---TSEM()}*

$9=178(164):
>sp|P50520|VPS34_SCHPO  {|519(104)|}Phosphatidylinositol 3-kinase vps34 OS=Schizosaccharomyces pombe (strain 972 / ATCC 24843) GN=vps34 PE=2 SV=2 
{()T-V----FKSTM-QPLRLLF-KcqdgSKYPIIFKN--GD-----D--LRQDQLVIQILTLmdkllkkeklDLhLKPYRILATGPTHG--AVQFVPSKTLATILAEyhgsvlaylrennpddglnsaNYGIDPVAMDNYVRSCAGYCVITYL--LGVGDRHLDNLLITKDGHFFHADFGYILGRDPKlfspaM----KL---SKEM()}*

_0].
[0_(1)=PI3K_prot(9){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=202(164):
>sp|P42345|MTOR_HUMAN  {|2161(186)|}Serine/threonine-protein kinase mTOR OS=Homo sapiens GN=MTOR PE=1 SV=1 
{()V-ITSK-QRPRK-LTLMGSN-GHEFVFLLKG--HE-----D--LRQDERVMQLFGlvntllandptslrKNLsIQRYAVIPLSTNSG--LIGWVPhCDTLHALIRD----YREKkkillniehrimlrmapdydhltlmqkvevfehavnntagddlakllwlkspssevwfdrRTNYTRSLAVMSMVGYI--LGLGDRHPSNLMLDRlSGKILHIDFGDC------F------------E()}*

$2=223(164):
>sp|Q13315|ATM_HUMAN  {|2691(142)|}Serine-protein kinase ATM OS=Homo sapiens GN=ATM PE=1 SV=4 
{()L-AGGV-NLPKI-IDCVGSD-GKERRQLVK---GrD-----D--LRQDAVMQQVFQmcntllqrntetrkRKLtICTYKVVPLSQRSG--VLEwctgtvpigeFLvnnedgahkryrpndfsAFQCQKKMMevqkksfeekyevfmdvcqNFQPVFRYfcmekfldpaiwfEKRLAYTRSVATSSIVGYI--LGLGDRHVQNILINeQSAELVHIDLGVAFEQGKIlptpetvpF----RL---TRDI()}*

$3=250(164):
>sp|Q96Q15|SMG1_HUMAN  {|2129(1282)|}Serine/threonine-protein kinase SMG1 OS=Homo sapiens GN=SMG1 PE=1 SV=3 
{()I-LPTK-TKPKK-LLFLGSD-GKSYPYLFKG--LE-----D--LHLDERIMQFLsivntmfatinrqeTPRFhARHYSVTPLGTRSG--LIQWVDGATpLfglykrwqqREAALQaqkaqdsyqtpqnpgivprpselyyskigpalktvglsldvsrrdwplhvmkavleelmeatPPNLlakELWsscttpdewwrVTQSYARSTAVMSMVGYI--IGLGDRHLDNVLIDmTTGEVVHIDYNVCFEKGKSlrvpekvpF----RM---TQNI()}*

$4=250(164):
>sp|Q8BKX6|SMG1_MOUSE  {|2127(1281)|}Serine/threonine-protein kinase SMG1 OS=Mus musculus GN=Smg1 PE=1 SV=3 
{()I-LPTK-TKPKK-LLFLGSD-GKSYPYLFKG--LE-----D--LHLDERIMQFLsivntmfatinrqeTPRFhARHYSVTPLGTRSG--LIQWVDGATpLfglykrwqqREAALQaqkaqdsyqtpqnpsivprpselyyskigpalktvglsldvsrrdwplhvmkavleelmeatPPNLlakELWsscttpdewwrVTQSYARSTAVMSMVGYI--IGLGDRHLDNVLIDmTTGEVVHIDYNVCFEKGKSlrvpekvpF----RM---TQNI()}*

$5=214(164):
>sp|P38111|ATR_YEAST  {|2054(100)|}Serine/threonine-protein kinase MEC1 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) GN=MEC1 PE=1 SV=1 
{()V-FSSL-KKPKQ-LNIIGSD-GNIYGIMCK---KE-----D--VRQDNQYMQFAttmdfllskdiasrKRSLgINIYSVLSLREDCG--ILEMVPNVVtLRSILSTkyeslkikyslkslhdrwqhtavdgklefymeqvdkfPPILYQWflenfpdpinwfnARNTYARSYAVMAMVGHI--LGLGDRHCENILLDiQTGKVLHVDFDCLFEKGKRlpvpeivpF----RL---TPNL()}*

$6=219(164):
>sp|Q9FKS4|ATR_ARATH  {|2371(112)|}Serine/threonine-protein kinase ATR OS=Arabidopsis thaliana GN=ATR PE=2 SV=2 
{()I-LSSL-QRPKK-IILLGND-GIEYPFLCKP--KD-----D--LRKDARMMEFTaminrllskypesrRRKLyIRTFAVAPLTEDCG--LVEWVPHTRgLRHILQDiyiscgkfdrqktnpqikriydqcavkkeyemlktkilpmfPPVFHKWflttfsepaawfrSRVAYAHTTAVWSMVGHI--VGLGDRHGENILFDsTSGDCVHVDFSCLFDKGLQlekpelvpF----RL---TQNM()}*

$7=223(164):
>sp|Q62388|ATM_MOUSE  {|2701(142)|}Serine-protein kinase ATM OS=Mus musculus GN=Atm PE=1 SV=2 
{()L-AGGL-NLPKI-IDCVGSD-GKERRQLVK---GrD-----D--LRQDAVMQQVFQmcntllqrntetrkRKLtICTYKVVPLSQRSG--VLEwctgtvpigeYLvnsedgahrryrpndfsANQCQKKMMevqkksfeekydtfmticqNFEPVFRYfcmekfldpavwfEKRLAYTRSVATSSIVGYI--LGLGDRHVQNILINeQSAELVHIDLGVAFEQGKIlptpetvpF----RL---SRDI()}*

$8=215(164):
>sp|P38110|ATM_YEAST  {|2441(131)|}Serine/threonine-protein kinase TEL1 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) GN=TEL1 PE=1 SV=3 
{()T---GL-SLPKI-VTFNISD-GTTQKALMKG--SnD-----D--LRQDAIMEQVFQqvnkvlqndkvlrnLDLgIRTYKVVPLGPKAG--IIEfvanstslhqILsklhtndkitFDQARKGMKavqtksneerlkaylkitnEIKPQLRNfffdsfpdpldwfEAKKTYTKGVAASSIVGYI--LGLGDRHLNNILLDcSTGEPIHIDLGIAFDQGKLlpipelvpF----RL---TRDI()}*

$9=202(164):
>sp|Q0DJS1|TOR_ORYSJ  {|2056(207)|}Serine/threonine-protein kinase TOR OS=Oryza sativa subsp. japonica GN=TOR PE=2 SV=3 
{()V-ITSK-QRPRK-LTIHGSD-GNDYAFLLKG--HE-----D--LRQDERVMQLFGlvntllensrktseKDLsIQRYAVIPLSPNSG--LIGWVPnCDTLHALIRE----YRDArkiflnqehrcmlsfapdydhlpliakvevfqhalensegndlakvlwlksrtsevwlerRTNYTRSLAVMSMVGYL--LGLGDRHPSNLMLDRySGKILHIDFGDC------F------------E()}*

_0].
[0_(1)=RevK(10){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=300(164):
>gi|167537263|ref|XP_001750301.1|  {|126(11)|}gi|167537263|ref|XP_001750301.1| 
{()L------QsSG--ENA-E---------YLVVMhqFpQ------------DALmsAIAArgQ-------------------L-QPKHL------D--------------MLTDALVRI----HaqcelirdedyvkqmdwivtdnTqEllhfaevaplVqqrtfiaptlrsfqeglvsldcvqrlaqqqaialdaarpslqrrqveghVrCcHGDLHTRNI-VILhdevylfdcisfnddltridvlyD----LafllMDlqhlglnklanrC-LNRYlaetgdyedvhllrlytstratirakvacasapalppgdhhkpsaeaqiytelalsllaapppvivviagnsgsgkstvaaelagsigsgigaI-----HV---RSDIVRK()}*

$2=284(164):
>gi|116072951|ref|ZP_01470213.1|  {|89(12)|}gi|116072951|ref|ZP_01470213.1| 
{()--LDPTATpNE--SLL-D---------AAVQMvqFeP------------QALlsRALTngS-------------------V-TRAML------T--------------DLAWTLGLF----HlraemadpdssfgTpAavcepvitnlSvlaplttqadqhavlsqhrqwidqqqqrllprftqrkergaIrEcHGDLHCANI-RRdRngrlevfdaidfnaglrwidpisE----MaflvMDlkmrgdpgrgleV-LNTWlectgaydgldlwpwysayramvrakvsalqakacrepeqrqqllgeldsylhtascweqppqaglvlmhglsgsgksslsgqlirplqaV-----RI---RSDRER-()}*

$3=277(164):
>gi|87302658|ref|ZP_01085475.1|  {|97(11)|}gi|87302658|ref|ZP_01085475.1| 
{()------LGdER--QGG-E---------HMVVMrrFaA------------GDTlaARLAagR-------------------V-SDAEL------Q--------------GLGRRIARF----HeqyplppelargAdRrfaavlrnnvRatrrsvpalfppgvhgllvsglalallrsrdllrrrleagqAvDgHGDLRLEHV-LLQpelavvdgvefnadlrqvdrasD----LaflvMDlqaaghgdrvaaL-LAGYgqaiepavlqlfcayrahvrakveaatseeaeipgpqrqaaegmarrylslalayasrglrppalvllhgvsgsgkshlaarlapwllaE-----HQ---RSDLIRK()}*

$4=288(164):
>gi|224369379|ref|YP_002603543.1|  {|84(11)|}gi|224369379|ref|YP_002603543.1| 
{()------LGkTG--SIV-E---------YAVKMrqLdE------------SwSLqqRIKTqtQ-------------------K-NSADI------RSDVI----------ALGEKLGQF----YltaptgkgddmgddGwKnifhacnenfNqtqgftknlldektwkmvrdetlaflkarkpcfmarlksgkVkDcHGDLRCGHI-YFTdrniqiidciefndslrhidiinD----LafllMDldfcdqqvlgdlL-LDRYleqtgdlqafmlldfykcyrafvrckvnaiflastqptpgekekkksealkyldlardyalrfshpgifvihglpatgkstmakalgcalevE-----PI---RSDIVRK()}*

$5=282(164):
>gi|77919469|ref|YP_357284.1|  {|84(11)|}gi|77919469|ref|YP_357284.1| 
{()------LDgPG--QTI-D---------YAVVMkrFpE------------ENMlpQLLSrnDP------------------A-LPERM------A--------------DLARHIAER----YqhlqsvgpgedtaHlDvvknnweenfVqtepfigttissgtferlaeyvrsfedsqaelfhrreaqgwVrDgHGDLHCEHI-CFLdgniaiydciefnrrfriadvlaD----LafllMDlelrgrydlatiV-RDNYlevigisddtdlllpfyqiyrayvrgkvdsflssdpnadqnnrsaaagrakkyfnqalgylcrtpkliltcglmgvgkstiarelatplgaE-----LL---SSDALRK()}*

$6=284(164):
>UniRef90_H8ITV9  {|83(10)|}UniRef90_H8ITV9 
{()-----RIGgKS--GSG-E---------PAVWMrrLpE------------EGMlpAKLArgE-------------------V-DLRLA------R--------------RIGRTLAKL----HgrtetgpdidaygSpSsvaanwqenfDqmgpfvgrtissdvndhirryvaeflrtrapvlerrvadghVrDgHGDLHAASI-CIDdgqillfdslqfapryrcadlasE----VaflaMDleyhgradvawaF-VDSYvrasgdaglldlldfyacyrayvrgkvrslrlaqteqasggdnreliaesrayfdlawahggglprppmvvtmglpasgkttlaralagrlglV-----HL---SSDVARK()}*

$7=283(164):
>UniRef90_H2CAE3  {|88(6)|}UniRef90_H2CAE3 
{()--------EG--EIV-E---------YCVAMheIdS------------DRFlgNRMLdg------------------------GHI------DDADFPLT-------QLVDRLAPF----Ykeasslhfdpdtyy-Qnvrqpmlen-lppyqsypievavemldllryffetslekhrrllmqrmqdgcVgDfHGDLHLQHI-VLPsqehpslcifdciefndafrqidraaD----IaflcMDldyrgyrkqseaF-CKAVadalndnglialqpllrayracvrakvnvltstneelspeersacvntarryvnlalryalagtekkaiavvgkvgsgksafarfaasclgvE-----AQ---SSDRIRK()}*

$8=266(164):
>UniRef90_UPI00020A9AE0  {|90(10)|}UniRef90_UPI00020A9AE0 
{()--------------V-E---------YCIKMveLpQ------------DSLmsNLLIkdR-------------------V-SVEDI------K--------------RVAEKVVYF----HsiankikdtqFvNsirynleenfRqtenivnwliskhnfnlikdnslyffdshksdflkrmendlFvDgHGDLHSKNI-SIMpddiyifdciefnerfriqdvasE----IsflsMDldfnnksdlslaY-INSYqklsnknitsylnffksylayvrgkvlsfsflseqtnkglertirryfrlsakyfdsalptifvicglsgtgksalakrlsakaniK-----RL---SSDEIRK()}*

$9=281(164):
>UniRef90_H0E6X2  {|73(10)|}UniRef90_H0E6X2 
{()-----RLVdDD--DPaaV-E---------YVVEMrrFdE------------ARTlrALLDdgA-------------------D-VSSEI------E--------------RVAATIAGF----HerarrirrpartamaEaSakrmiaaddvEltgllrgpldaarlhaidrrlrtfvrdhaeeladrgrrgmVrEgHGDLRAEHV-LL-dgdvrivdriefrddlreidvadD----VaflvMDlvalgrpdaarsF-VDAYrahhgdpgsdaliafyaahralvrakvallrdpdsarqrpdllearrllavaegfawrtrqplalivsgpsgvgksrlaatlarrsgfA-----VC---SSDVVRK()}*

$10=272(164):
>UniRef90_C9Z3E0  {|63(8)|}UniRef90_C9Z3E0 
{()--------DA--EVS-E---------PLVVMrrMpA------------GRRlsALVRagA-------------------D-VDEVL------R--------------AVARRLAAW----HataprgrdvdeqgTrDalasrweasfEqvratteggsgfdgvpevqrlvrrylagrealfdsrieqrrVvDgHGDLLAEDI-FCLddgprvldclefddhlryvdgldD----AaflaMDleqlgapaaaarF-LARYgeysgdpappslwhhyvayrafvrakvsliqaeqgapgvrsaarrlvsttlrhlrtsavgltlvgglpgsgkstlsgaladrlgvT-----LL---SSDRLRK()}*

_0].
[0_(1)=RIO(21){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=193(164):
>sp|Q9BRS2|RIOK1_HUMAN  {|178(197)|}Serine/threonine-protein kinase RIO1 OS=Homo sapiens GN=RIOK1 PE=1 SV=2 
{()ITEiNGCISTGKEANVYHASTANGESR-AIKIYKTsilvfkdrdkyvsgefrfrhgyckgnprKMV---KTWAEKEMRNLIRLNtaEIPCPEP-IMLR---ShvLV-M-SFIGkddmpAPLLKNV------QLSEsKARELYLQVIQYMRRMYQdARLVHADLSEFNMlyHG---GGVYIIDVSQSVE-HdhphaleFLRKDCANVNDFFMRH()}*

$9=191(164):
>sp|O28471|RIO1_ARCFU  {|51(16)|}RIO-type serine/threonine-protein kinase Rio1 OS=Archaeoglobus fulgidus (strain ATCC 49558 / VC-16 / DSM 4304 / JCM 9628 / NBRC 100126) GN=rio1 PE=1 SV=1 
{()---GGVISTGKEANVFYADGVfdGKPVAMAVKIYRIEtsefdkmdeylygderfdmrrispkeKV---FIWTEKEFRNLERAKeaGVSVPQP-YTYM---KnvLL-M-EFIGedelpAPTLVEL----GRELKElDVEGIFNDVVENVKRLYQeAELVHADLSEYNiMYI---DKVYFIDMGQAVtlrhpmaE-SYLERDVRNIIRFFSKY()}*

$2=129(164):
>sp|Q8SVI7|RIO1_ENCCU  {|122(136)|}Probable serine/threonine-protein kinase RIO1 homolog OS=Encephalitozoon cuniculi (strain GB-M1) GN=RIO1 PE=3 SV=2 
{()----------------------------------KLI---KVWAEKEVRNLKRLNnaGIPSPEP-IYLK---NniLV-M-TQIGrcsevAPRLRDA----SIK---DLEGCYQQCVKIIRDMYkKAGLVHADLSEFNLlyFE---GVVYVIDVGQSVEIdhdnaqRFLIMDINNINSFFSRK()}*

$3=189(164):
>sp|Q54VD8|RIO1_DICDI  {|185(200)|}Serine/threonine-protein kinase rio1 OS=Dictyostelium discoideum GN=rio1 PE=3 SV=1 
{()---NGCISTGKEANVYHAFTPNEEER-AVKVYKTsilvfkdrdryvtgefrfrrgyskhnprKMV---KVWAEKEFRNLTRLKnaGIPCPTP-LILR---NhiLV-M-TFIGkdgyaAPRLKDA-----TVSQEKFGVIYLDCIKMMRTLFhKCRLVHADLSEYNMlyYK---NQLYIIDVSQSVE-HdhphsldFLRMDCSNVTDFFRKK()}*

$4=183(164):
>sp|D4GYY1|RIO1_HALVD  {|81(26)|}RIO-type serine/threonine-protein kinase Rio1 OS=Haloferax volcanii (strain ATCC 29605 / DSM 3757 / JCM 8879 / NBRC 14742 / NCIMB 2012 / VKM B-1768 / DS2) GN=rio1 PE=1 
{()---GGPISTGKEANVFEALGGDDADV-AVKIYRInasdfrhmrdylegdprfenighdkgQVV---RAWVRKEFANLERAQraGVRVPKP-IAVQ---RnvLV-M-ELVG--VVDDR----ARRLSevrvENPQTAYEVVREYMRRLHRAGLVHGDLSEYNliIHD---GELVVIDLGQAVtvhhpnaE-EFLRRDCRNVANFFRR-()}*

$5=182(164):
>sp|Q57886|RIO1_METJA  {|79(29)|}RIO-type serine/threonine-protein kinase Rio1 OS=Methanocaldococcus jannaschii (strain ATCC 43067 / DSM 2661 / JAL-1 / JCM 10045 / NBRC 100440) GN=rio1 PE=3 SV=1 
{()----GIVNSGKEAVVFKARK-GKFYR-AVKVYRVatcdfktmskyiqgdprfhlrkssrrQII---HAWVEKEFRNLRRASEIINAPK-ARLR---RenvLV-M-DFVGyrgipAPKLKDM-------QDLDWEKYFKIIKESMKKLYeEGELVHGDLSEYNILVKD-DEPVFIDFSQSVitqhplaH-PLLIRDCINICNFFRRK()}*

$6=189(164):
>sp|O42650|RIO1_SCHPO  {|122(186)|}Serine/threonine-protein kinase rio1 OS=Schizosaccharomyces pombe (strain 972 / ATCC 24843) GN=rio1 PE=3 SV=2 
{()---NGCISTGKEANVYHATNEDGKHF-AIKIYKTsilvfkdrdryvsgefrfrhgynkrnprKMV---RLWAEKEIRNLKRvaAAGIPCPEP-ILLK---QhvLL-M-SFLGdkkgwaYPKLKDI----DMTPGE-ATKLYQLVARNMRILFHvCHLVHADLSEYNLlyHK---GKVYFIDVSQSVE-HdhpqsidFLRMDILNISTFFRR-()}*

$7=137(164):
>sp|Q03021|RIO1_THEAC  {|33(16)|}RIO-type serine/threonine-protein kinase Rio1 OS=Thermoplasma acidophilum (strain ATCC 25905 / DSM 1728 / JCM 9062 / NBRC 15155 / AMRC-C165) GN=rio1 PE=3 SV=1 
{()---------------------DQRFS-KIRIDRNDIV---PVWVRKEYTNLMALENahVPAPKP-IgFFK---NiLV-M-SYIGtksgpAPQLKDV---------EIDEGIYDQVIDGMRRMYANRIVHADLSEYNMlFH---RKVYFIDLAQAVD-MdhpmaaeFLERDIVNVSNFFQKH()}*

$8=212(164):
>sp|Q12196|RIO1_YEAST  {|78(194)|}Serine/threonine-protein kinase RIO1 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) GN=RIO1 PE=1 SV=1 
{()---NGCLSTGKEANVYHAFAGTGKapvideetgqyevletdgsraEY-AIKIYKTsilvfkdreryvdgefrfrnsrsqhnprKMI---KIWAEKEFRNLKRIYQsgvIPAPKP-IEVK---NnvLV-M-EFLsrgngfASPKLKDY----PYKNRDEIFHYYHTMVAYMRLLYQvCRLVHADLSEYNtiVHD---DKLYMIDVSQSVEPehpmslDFLRMDIKNVNLYFEK-()}*

$10=188(164):
>sp|Q922Q2|RIOK1_MOUSE  {|182(197)|}Serine/threonine-protein kinase RIO1 OS=Mus musculus GN=Riok1 PE=2 SV=2 
{()----GCISTGKEANVYYASTPSGESR-AIKIYKTsilmfkdrdkyvtgefrfrrgyckgnprKMV---RTWAEKEMRNLCRLKtaNIPCPEP-IRLR---ShvLL-M-GFIGkddmpAPLLKNV------QLSEsKARELYLQVIQYMRKMYQdARLVHADLSEFNMlyHG---GDVYIIDVSQSVE-HdhphaleFLRKDCTNVNDFFSKH()}*

$11=201(164):
>sp|Q1RMT7|RIOK3_BOVIN  {|251(67)|}Serine/threonine-protein kinase RIO3 OS=Bos taurus GN=RIOK3 PE=2 SV=1 
{()-TITGCISTGKESVVFHAYGGSMEDGKedskviptecAIKVFKTtlnefknrdkyikddfrfkdrfsklnprKII---RMWAEKEMHNLTRMQraGIPCPTV-VLLK---KhiLV-M-SFIGhdqvpAPKLKEV-----KLSSEEMKDAYYQTLHLMQQLYdECTLVHADLSEYNMlwHA---GKVWLIDVSQSVEPthphglEFLFRDCRNVSQFFQK-()}*

$12=201(164):
>sp|O14730|RIOK3_HUMAN  {|251(67)|}Serine/threonine-protein kinase RIO3 OS=Homo sapiens GN=RIOK3 PE=1 SV=2 
{()-TITGCISTGKESVVFHAYGGSMEDEKedskviptecAIKVFKTtlnefknrdkyikddfrfkdrfsklnprKII---RMWAEKEMHNLARMQraGIPCPTV-VLLK---KhiLV-M-SFIGhdqvpAPKLKEV-----KLNSEEMKEAYYQTLHLMRQLYhECTLVHADLSEYNMlwHA---GKVWLIDVSQSVEPthphglEFLFRDCRNVSQFFQK-()}*

$13=201(164):
>sp|Q9DBU3|RIOK3_MOUSE  {|251(67)|}Serine/threonine-protein kinase RIO3 OS=Mus musculus GN=Riok3 PE=1 SV=3 
{()-TITGCISTGKESVVFHAYGGSLEDEKedgkaiptecAIKVFKTtlnefknrdkyikddfrfkdrfsklnprKII---RMWAEKEMHNLTRMQkaGIPCPTV-VLLK---KhiLV-M-SFIGhdqvpAPKLKEV-----KLSNEEMKDAYYQTLHLMQQLYnECTLVHADLSEYNMlwHA---GKVWLIDVSQSVEPthphglEFLFRDCRNVSQFFQK-()}*

$14=188(164):
>sp|P34649|YOT3_CAEEL  {|247(75)|}Putative RIO-type serine/threonine-protein kinase 3 OS=Caenorhabditis elegans GN=riok-3 PE=3 SV=1 
{()-SVDGIIATGKESAVLHAAQDSATSY-AIKVYKTTlsefknrseyvkddfrfknprgVL---KIWAEREFMNLSRMakHGLPCPQP-VKVR---RnvLV-M-SFLGdqglaAPRLKNV----EWEFftDDERRNVYDQVQSIMCRMYkECLLVHADLSEFNLLL---TpdnKVHVIDVSQAMD-LshprslqFLTRDIQNIITFFTR-()}*

$15=174(164):
>sp|O30245|RIO2_ARCFU  {|91(17)|}RIO-type serine/threonine-protein kinase Rio2 OS=Archaeoglobus fulgidus (strain ATCC 49558 / VC-16 / DSM 4304 / JCM 9628 / NBRC 100126) GN=rio2 PE=1 SV=1 
{()DAIGKLMGEGKESAVFNCYSEKF-GECVVKFHKVGhtsfkkvkekrdygdlhFSVLAIRSARNEFRALQKLQGLAVPKVYA-WEG--NA-VLMELIDAKELYRV-----RVE--NPDEVLDMILEEVAKFYHRGIVHGDLSQYNVLVSEE-GIWIIDFPQSVEvgeeGWreILERDVRNIITYFSRT()}*

$16=180(164):
>sp|Q54T05|RIO2_DICDI  {|94(248)|}Serine/threonine-protein kinase rio2 OS=Dictyostelium discoideum GN=rio2 PE=3 SV=1 
{()--VGNQIGVGKESDIYIVANDDN-QEMVLKLHRLGrvsfktiknnrdylkhrksasWLYLSRLAALKEFAYMKALyeNGFPVPTPID-YNR--HC-IVMSRARGYPLTQiV-----QLR--HPSKVYSDLMNLIVKLASYGLIHGDFNEFNILInDEE-EITLIDFPQMVStShlnaeMYFDRDVTCIRVFFEKR()}*

$17=184(164):
>sp|Q58473|RIO2_METJA  {|65(21)|}RIO-type serine/threonine-protein kinase Rio2 OS=Methanocaldococcus jannaschii (strain ATCC 43067 / DSM 2661 / JAL-1 / JCM 10045 / NBRC 100440) GN=rio2 PE=3 SV=1 
{()-AIGNKLGVGKEGDVYTVLLSDG-REAVLKFHKHGrtcftrgkryrgyladkhhisWLYVSRLTAEREFEILNEL--FPivkVPEPIE-WNR--HA-IIMGKVVGEELKRL-----DLsefmskE--EIKDLFWKIIEEVKKAYEIGYIHGDLSEFNILLDENGDFVIIDWPQAVpkyhpDaEFYLKRDIWNVIRYFKK-()}*

$18=181(164):
>sp|Q9P7W5|RIO2_SCHPO  {|94(69)|}Serine/threonine-protein kinase rio2 OS=Schizosaccharomyces pombe (strain 972 / ATCC 24843) GN=rio2 PE=3 SV=2 
{()YSVGNQIGVGKESDVYVVGDQKG-KQYILKIHRLGrisfrsvknnrdylrnrktgsWQYLSRLAATKEFAFMKILHehGFPVPAPID-HSR--HC-IIMEMIDAFPLRAV-----TdIR--DPPALYQTLMDIIVRFARNGLIHGDFNEFNIIVRED-GtAVVIDFPQMVStShpdaqFYFDRDVQCIVQYFEK-()}*

$19=188(164):
>sp|P40160|RIO2_YEAST  {|94(143)|}Serine/threonine-protein kinase RIO2 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) GN=RIO2 PE=1 SV=1 
{()YSVGNTIGVGKESDIYKVSDKNG-NPRVMKIHRLGrtsfhsvrnnrdylkksnqganWMHLSRLAANKEYQFMSMLysKGFKVPEPFD-NSR--HI-VVMELIEGYPMRRL-----RKhK--NIPKLYSDLMCFIVDLANSGLIHCDFNEFNIMIKDKLedendcGFVVIDFPQCISiqhqdaDYYFQRDVDCIRRFFKK-()}*

$20=182(164):
>sp|Q9BVS4|RIOK2_HUMAN  {|94(276)|}Serine/threonine-protein kinase RIO2 OS=Homo sapiens GN=RIOK2 PE=1 SV=2 
{()ESVGNQMGVGKESDIYIVANEEG-QQFALKLHRLGRTsfrnlknkrdyhkhrhnvswlYLSRLSAMKEFAYMKALyeRKFPVPKPID-YNR--HA-VVMELINGYPLCQI-----HhVE--DPASVYDEAMELIVKLANHGLIHGDFNEFNLILDES-DhITMIDFPQMVStShpnaeWYFDRDVKCIKDFFMKR()}*

$21=182(164):
>sp|Q9CQS5|RIOK2_MOUSE  {|94(271)|}Serine/threonine-protein kinase RIO2 OS=Mus musculus GN=Riok2 PE=2 SV=1 
{()ESVGNQMGVGKESDIYIVANEAG-QQLALKLHRLGRTsfrnlknkrdyhkhrhnvswlYLSRLSAMKEFAYMKALyeRKFPVPKPID-YNR--HA-VIMELINGYPLCQI-----HhVE--DPASVYDEAMELIVKLGNHGLIHGDFNEFNLMLDKD-DhITMIDFPQMVStShpnaeWYFDRDVKCIREFFMKR()}*

_0].
[0_(1)=UbiB(6){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=296(164):
>ADCK_consensus_seq 
{()SHIDPtPLGSASIAQVHRATLK-TGQEVVVKVQRPGIYETMaqDIslmrsaaklatpvmxrtkqvvdfnmvvdelwntfqqEMDflNEARNLEEFKRFCKDykyMDCPkpyreyCTQ----HVLVMEYIDGIPIDHPD----QLHEAGydLKEIGTKLVDNYCKQIlDDGFFHADPHPGNIWI-RGGQIVWIDLGMMGRlSNRdrsvlknmifavakndspalkdavlrfggxtxkgdxppdhpqlyadidvimkqygtcdfgdldigeflqqltqmakrhhiecphgvtmFARGLVTMEGVLDEC()}*

$2=299(164):
>ADCK1_HUMAN  {|153(78)|}Uncharacterized aarF domain-containing protein kinase 1 OS=Homo sapiens GN=ADCK1 PE=2 SV=2 
{()QSFDDtPLGTASLAQVHKAVLH-DGRTVAVKVQHPKVRAQSskDIllmevlvlavkqlfpefefmwlvdeakknlplELDflNEGRNAEKVSQMLRHfdfLKVPrihwdlSTE----RVLLMEFVDGGQVNDRD----YMERNKidVNEISRHLGKMYSEMIfVNGFVHCDPHPGNVLVrkH-PGtgkaEIVLLDHGLYQMlTEEfrlnychlwqsliwtdmkrvkeysqrlgagdlyplfacmltarswdsvnrgisqapvtatedleirnnaanylpqishllnhvprqmllILKTNDLLRGIEAAL()}*

$3=405(164):
>ADCK2_HUMAN  {|199(22)|}Uncharacterized aarF domain-containing protein kinase 2 OS=Homo sapiens GN=ADCK2 PE=2 SV=1 
{()-FENRePVGSGCVAQVYKAYANtafLETdsvqrlgrasclppfshtgavgglrelfgylgngrkppenladqsflerlllpkadlvgsnagvsraqvpghqpeATnlisVAVKVLHPGLLAQVhmDLllmkigsrvlgvlpgikwlslpeiveefeklmvqQIDlrYEAQNLEHFQVNFRNvkaVKFPtplrpfVTR----EVLVETYEESVPVS-------SYQQAGipvdlKRKIARLGINMLLKMIfVDNFVHADLHPGNILVqga-nGLSssqeaqlqqadicdtlvvavpsslcplRLVLLDAGIVAElQAPdlrnfravfmavvmgqgqrvaelilhharasecrdvegfktemamlvtqarkntitleklhvssllssvfkllmthkvklesnfasIVFAIMVLEGLGRSL()}*

$4=295(164):
>ADCK3_HUMAN  {|328(24)|}Chaperone activity of bc1 complex-like, mitochondrial OS=Homo sapiens GN=ADCK3 PE=1 SV=1 
{()EYFEErPFAAASIGQVHLARMKg--GREVAMKIQYPGVAQSInsDVnnlmavlnmsnmlpeglfpehlidvlrrelalECDyqREAACARKFRDLL-kGhpfFYVPeivdelCSP----HVLTTELVSGFPLDQAE----GLSQEIRNEICYNILVLCLRELfEFHFMQTDPNWSNFFYDPQQhKVALLDFGATREyDRSftdlyiqiiraaadrdretvraksiemkfltgyevkvmedahldaililgeafasdepfdfgtqsttekihnlipvmlrhrlvpppeetysLHRKMGGSFLICSKL()}*

$5=295(164):
>ADCK4_HUMAN  {|207(42)|}AarF domain-containing protein kinase 4 OS=Homo sapiens GN=ADCK4 PE=1 SV=2 
{()ASLEEvPFAAASIGQVHQG-LLrD-GTEVAVKIQYPGIAQSIqsDVqnllavlkmsaalpaglfaeqslqalqqelawECDyrREAACAQNFRQLLANdpfFRVPavvkelCTT----RVLGMELAGGVPLDQCQ----GLSQDLRNQICFQLLTLCLRELfEFRFMQTDPNWANFLYDASShQVTLLDFGASREfGTEftdhyievvkaaadgdrdcvlqksrdlkfltgfetkafsdahveavmilgepfatqgpydfgsgetarriqdlipvllrhrlcpppeetyaLHRKLAGAFLACAHL()}*

$6=296(164):
>ADCK5_HUMAN  {|198(86)|}Uncharacterized aarF domain-containing protein kinase 5 OS=Homo sapiens GN=ADCK5 PE=2 SV=2 
{()QEFDYqPIAAASLAQVHRAKLH-DGTSVAVKVQYIDLRDRFdgDIhtlelllrlvevmhpsfgfswvlqdlkgtlaqELDfeNEGRNAERCARELAHfpyVVVPrvhwdkSSK----RVLTADFCAGCKVNDVE----AIRSQGlaVHDIAEKLIKAFAEQIfYTGFIHSDPHPGNVLVrkGPDGkaELVLLDHGLYQFlEEKdraalcqlwraiilrddaamrahaaalgvqdyllfaemlmqrpvrlgqlwgshllsreeaaymvdmarerfeavmavlrelprpmllVLRNINTVRAINVAL()}*

_0].
[0_(1)=WbdD(7){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=164(164):
>WbdD.fas consensus seq 
{()QPFQHWQNQPRSRRYYFGEXXXXXXDYVCKFFYYDMMEESKYELHNEIKFLTQPGFDXAPAVLTHGENAQSGWLVMEKIPGELLSDMLAAGEPIDXXXREKILGSLLRQLAALEKQGFYHDDVRPWNVMVDARQCARLIDFGSIVTTSWPTNLVQSFFIFVNEL()}*

$2=179(164):
>ORIGPDB|4azs_WbdD_APK 
{()QPFQHWQNQPyxxxxxxxxRSRRYFFGE------DYVCKFFYYDMphgiltAEESqrnKYELHNEIKFLTQPpaGFD-APAVLAHGENAQSGWLVMEKLPGRLLSDMLAAGEEID---REKILGSLLRSLAALEKQGFWHDDVRPWNVMVDxARQHARLIDFGSIVTTpxxxSWPTNLVQSFFVFVNEL()}*

$3=175(164):
>lcl|157067203/221473  {|4(0)|}157067203/221-473 
{()-----WQNQPyagaglahkxRSRRYFFGE------DYVCKFFYYDMphgiltAEESqrnKHELHNEIKFLTQPpaGFD-APAVLAHGENAQSGWLVMEKLPGRLLSDMLAAGEEID---REKILGSLLRSLAALEKKGFWHDDVRPWNVMVDxARQHARLIDFGSIVTTpqdcSWPTNLVQSFFVFVNEL()}*

$4=175(164):
>lcl|376382842/221473  {|4(0)|}376382842/221-473 
{()-----WRDSPytgagfahkxQSRRYYFSS------DFICKFYRYSTvsglltDKESernRTELAHEEAFLKSPpsGLK-VPALFTSGGNEVAGWLVMEKIPGELLNDVLASGRHIN---REKVISDLLEQLVILEEHGLYHDDFRTWNVLIDxDNDCARLIDFGSVGSVqqdcSWPVNIFQSFIIFVNEL()}*

$5=175(164):
>lcl|257449995/222472  {|4(0)|}257449995/222-472 
{()-----WRNTPhsatdghpheGARRYYFGE------SFVCKFFTFQQignkdxTYQSqrnREELTREVQFLNDPpeGFN-VPTLIESKTSKYDSWLVMERIHGELVSSLLDKKQAIN---IDNLLTDLLSQLVVLEKAGLYHDDIRLWNIIYDqDNKCFSLIDYGSISSVkqdcGWPFNIFLSFFVFVNEL()}*

$6=175(164):
>lcl|84779616/221470  {|4(0)|}84779616/221-470 
{()-----WSKRPhsqsdnahhxDSRRYYFGD------DFFCKVYHAHVqnsalsDEERernRQELHQEIDFLSRApaEFP-VARLLASGENEWQGWNILSHLPGELLDGKLLALTPAE---RENLLHQILQQLAGLERAGLYHDDVRTWNILVDxAHLQVHIIDYGSISDKktdcAWPENLLLAFFITLNEI()}*

$7=162(164):
>lcl|388558822/224462  {|17(0)|}388558822/224-462 
{()--------QXGARRYYTSD------DYFVKVFRIDGMFGxxxxxxxPrnQKELAQEAEFLRHPpaGFD-TPRHFTSGGNDAESWLVTERIEGELLLDVIARAEPLD---KRAVLLDVLRQLAALEQQGFYHDDVRVWNVMLDxSGRKARLIDFGSIGNQpedcVWPHNLYLSFMIFVKEV()}*

_0].
[0_(1)=MTRK(4){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$1=164(164):
>MTRK.fas consensus seq 
{()STCIQEVGDGNLNYVFHVYXGDSXRSLVIKQAVPYYWEPWTDRARFESTTLRKHNSPHVPRVYYSDREETMAVTVMRYLSHKIARKGLIAGXGENYPHLSFLAQHMSDYMAKTLFTTLIHGDLHTGSVMATQHXSTQVIDPEFSFYXGPMGFDVGQFGNNIFAN()}*

$2=165(164):
>2olc_MTRK_Bac_ELK 
{()LTC-QEIGDGNLNYVFHIY-XDXXxRALIIKQAVPXYWXPLtxIDRARIESSALIREHVPHLVRVFYSDTEXXMAVTVMEDLSLKIARKGLIEGXXENYPHLSXXXQHIGEFLGKTLFSTLIHGDLHTGSIFASEHXETKVIDPEFAFYXGPIGFDVGQFIANLFLN()}*

$3=164(164):
>MTRK.fas  {|0(1)|}consensus seq 
{()STCIQEVGDGNLNYVFHVY-XDS-SxrSLVIKQAVPYYWEPWxTDRARFESTTLRKHNSPHVPRVYYSDR-ETxMAVTVMRYLSHKIARKGLIA--GxgENYPHLSFLAQHMSDYMAKTLFTTLIHGDLHTGSVMATQHXSTQVIDPEFSFYXGPMGFDVGQFGNNIFA-()}*

$4=164(164):
>2pyw_MTRK_Plant_ELK  {|0(1)|} 
{()SLVIKEVGDGNLNFVFIVV-GSSXGSLVIKQALPYIGESWpmTERAYFEATTLRKHNSDHVPEVYHFDRXtX-MALIGMRYLEHIILRKGLIAXXGIEYPXXXFLADHMSDYMAKTLFTALIHGDLHTGSVMVTQDXSTQVIDPEFSFYXGPMGFDIGALGNLILA-()}*

_0].
[0_(1)=APH3(23){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$0=164(164):
>APH3.fas consensus seq
{()GGHSSKTYKFTYQWSDGNSKNCXTDTYVLKMKPPGXFCXXKPSMEREAKMMQWLTGVPVPKVYWWCQDQDWPFYIMKYIPGKTCQSWHMSPXEQREKICQQMAEQXLAQXXQLHSIDNQHGDFRPGNIMVXHDGKVAIIDWEMAGWXGDPYCDLAWACMSYWWF()}*

$1=306(164):
>APH3_12  {|0(4)|} 
{()LHNAKVISEINKGFSVdRxxxxxxKyQ-XVXxdEXTYxLVRVFPID-XLXXxWqEXxxxxxxxxxRKQEFKIIQALdsxQTPFVPRAYxxDFGCIERxxxxxEGYMIISYLRGEDAesgmtXXXHxxxxxxxxxxxxxxLSHXSEQFKAGFSAGEIXLRXXXEVHKIplDIPkmnwldfqtakfkrkveelkeleitasfltdteqfvyeniarlknrpiclqxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxHGDFHPANIIL-XNxxxxxxxxxnkxXKFvGLIDFNRLEFXGDPLFDLAKIGFF----()}*

$2=310(164):
>APH3_17 
{()YNCSVKCLPFGLXXXXXXXXXXXXXXXxxxxxxxxxxCLKIGRRVXXXXXXXXxxxxxxxxxxMENEANALRLVekhTSINAPQLIxxDFAQDDDnxxxxSGFILMTRVPGVPMnqvmyXXXRxxxxxxxxxxxxxxMTYXEEREQIGKDLAKW-XISXXxQYRQIxxPNTskhlicntlggpicdhrtdtrvwgpynskaeftdmltedvvdieqrrheppisvlyekkhevcftxxxxxxxxxxxxxxxxxxHSDLHMSNIFV-XHxxxxxxxxxsgxXRLcGIIDWENACFXKPEYWEFTRAMWPYMSD()}*

$3=310(164):
>APH3_19 
{()ECHYDDAFDGKTIYNYfGnrvikhNtSXdxxgH--XPVXAIKFKTTG-X---FXXxfxxxxxxxxxxxxWRSEADMMHYAhsxQNILAPAVXxxWGCYQVItkrplARAMVSDFVPGNPLdnvwhXXXTxxxxxxxxxxxxxxMNKXEQCMSIKEQLAYQXLKXXXLMRSCxxTQPyigrvnrqptrnpydriethfmgpfdseaqfdewclsrvkgnpsekwkwrralaemrrkspsrfvltxxxxxxxxxxxxxxxxHGDLFPRNIMVXkxxxxxxxxXDGxxKItGIVDWEYSGFXYPEYMEYALAMCLYDGH()}*

$4=310(164):
>APH3_11 
{()HCCYKSHVKLFYGASDrGxxxxxxVwS-XIXxGSXdmXILKDRPYD-X---PXXxpxxxxxxxxxxxxNFKEVPNIQFVqenTSIPVPTVVkdWNERDGHXxxxxxYFILMKRIPGQPLsnvwpxxxkxxxxxxxxxxxxxxM---STX-AQKEMIAKQTAEY-XLMXXxQLRKLqsDNMqciggqpcysnflfrnkmephgpfasddelwacmerglhhvpeqarenlrkcmpscapytftxxxxxxxxxxxxxxxxxxxxxHCDLTNVNIMVXkxxxxxxxxXNGxxKVtGIIDWEMCGYXFPVWWEYVCAGIGDGED()}*

$5=304(164):
>APH3_20  {|0(6)|} 
{()QGCTSTVRKXXXXXXXXxxxxxxxIvTXcxxQ--DXAFxLLKSSFQQ-X---KxxxyxRewXLxxxxxxxMREAFVLQKLtetRRLPVPIYYxxGMIQEQHxxxxxASHIIMSFEDGITLtsalrkakG-----XXxxxxxxxxxxxxxxdxTEKKKLIKSFGQFXLQXXXRLHETXXexMvsglqpetdwldlqikrageyvkkgqaegsaellkeldrcrpvpvqqtmixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxHGDCTTDNVLVXkxxxxxxxxXDGxxEVyLFIDAAGISAXGDPRYDESLAI------()}*

$6=310(164):
>APH3_2 
{()SGHDNRMFHLXXXXXXXXXXXXXxxxxxxxxGDxEMxSVRMPSME-X----AxxxxxyxxxApqxxxxVEKENKWLPILskhLPLPIPTPIakGNPSEGYpxxxxWPWSIYKWIEGETVxxxxxxxxtxxxxxxxxkqNXXXIDEXRDLNEFAADLASF-XLKXXxELQSIxxDPSngpiagtrgghisvydeeveeriaetrhcienkdvgegfdllkhvwedalqsptwdgkpvwvxxxxxxxxxxxxxxxxxxxxxHGDVHPGNLLV-XDxxxxxxxxxdgxXKLcAVIDFGCMGVXGDPACDLAXXXXMAWTF()}*

$7=310(164):
>APH3_7 
{()TARFRKRDGYCVPFLXXXXXXXXxxxxxxxxsRXHLxCVKTGPWV-XHXXXXXxxxxxxxxxxxLTEAATMQFVAentTSIPVPKVYxxCAFVHKNxxxxxRTYIVMERIRGQMLxxxxxxxxAsgwvkpprSXXxxxEESXKAAAKILAQLKDM-XVQXXxEMRALxxPPPpggtgvhncdggslydcriprcqprfgpfktiqdfhrwlrdgmqpserpknfqinaddpdhywedikemiamqdgpwpepvftHGDLNSLNILVXrxxxxxxxxXGDxxKVvGIIDWETAGWXYPSYXXXXWEYTSAWYV()}*

$8=310(164):
>APH3_18 
{()GGASKIQMAFTLRYSDpAxxxxxxDgPXvxxtE--XRMXVLRMDPPE-X---SxxxixNatsRXxxxxxxMREFEIINCMaxxGVVPVPTVYXxxWCDEDGchfpxYPALICGFVHGVTKxxxxxxxxPsraqsgqiSGIgtnFGPXELRKKLAPQFVDH-XL---AxxxcIHTFxxDVKqakldhyekpapgttqcaqwqlnwwrrvweedcgediplmdvaagwlrdnmpvcdhvsvvxxxxxxxxxxxxxxxxxxxxxxxHGDYRSGNFLF-XdeDxxxxxxxsgxXCItAILDWELCHIXGDRHQDLAWCTCYPFGH()}*

$9=310(164):
>APH3_15 
{()FGHSAEMMTLSVVWRRgGrXXXxxhxxxexxR--RXDVxVLRLRPSP-X---PxxxaxLlxxEpydxxxLARQFTILRALakxTPVRVPRALxxWLEPTGEvlgxxRPFFVMEHVAGDVYemqppaelDXXXxxxxxxxxxxxaSDX-HTVVRMCQSLVEQXLAXXXAIHSVxxDLTrtglatlddgathldrelghwaaemnrvkrgplpalerlhqallagkpapcptvtlvxxxxxxxxxxxxxxxxxxxxxxxxxxHGDAKPGNFAFXtxxxxxxxxXGGxxEVsAVFDWEMTTVXGDPLTDIGWLEMLXWMQ()}*

$10=310(164):
>APH3_8 
{()GGHSSLTYKXXXXXXXXxxxxxxxVtTXsxxrP--XDYXVVKMVPPG-X---QxxxkxPixxGnhdxxxMLRQARIMKALaaxTDVPVPRIXxxCWEDEGQxxxxxPPWFAMSCVEGECYepvfdvcpPXXXxxxxxxxxxxxVPPXDTAAARMRNACRMXLPXXXRLHNVgtPMLdedpdtiglgdepvlspqaelhrwkrtmdtvppelvpgweelhdqlaacipeampptlvxxxxxxxxxxxxxxxxxxxxxxxxHGDYRLGNIICXsxxxxxxxxxgTXXEInAVIDWEIWSIXGDPRIDLGWFLIFCDHT()}*

$11=57(164):
>APH3.fas  {|253(0)|}consensus seq 
{()-----------------------------------------------------------------------------------------------------------------------HGDFRPGNIMV-XhxxxxxxxxXDGxxkVxAIIDWEMAGWXGDPYCDLAWACMSYWWF()}*

$12=310(164):
>APH3_3 
{()GGACQENWSFQVQVLgGXxxxxxxPwK-X--GXxsyxdTXVLRTDKPyxsxxX-L---XPaslSxxxxxxREQEFAVCQLAynxAGVNTPHPFxxWLCTDPDitgxxSPFYIMQKISGKTIgryivkXXGXxxxxxxxxxxxxxADPXSFNKQLARQCGENXLAXXXKIHSIxxKPPcctlwmlpdpnkipandsirsyrhyydsmgwpypvmewilnwcekhappssdvclvxxxxxxxxxxxxxxxxxxxxxxxxxxxHGDFRTGNYMVXtxxxxxxxxxpDXXGLqAILDWEFAHWXGDPHEDLTWFCMRCWRF()}*

$13=273(164):
>APH3_13  {|37(0)|} 
{()---------------------------IIRTVPPD-XCXXxhxxxxX-DggxxxxMEMEAKCQRAAakxAGVPVPHILdhCFSPDDNvlgxxNPYLIMNFIKGESIprkiqrdX---DxxxxxxxxxxxxxxtyHX-TARARFVQQCGQIXLAXXXRIHRMxxDPHqtpglshmedqldewhtqyprmfdtmptgfewafqwlranrpppspkscvvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxHGDFRMGNLIVXdxxxxxxxxxgSXXGIaAVLDWELVHIXGQPYEDLGWFCIKAWRF()}*

$14=310(164):
>APH3_6 
{()NGMSSETIIFTAQWTQxqxxxxxxGGqX-TXXeQXKCxVARVAPTAXexxxD---XVpvfPtyrxxxLDHQFEVMRKVgehTDVPVPRVRxxWYCNTGDvlgxxTPFFVMDYVDGIVPxxxxxxxxPdvmpytfgNWFtdxAPPXEQQRKLQDNTVEVXLAXXXKIHSIxxPNNtfgflnggqpgdtalrkhfnwqkswyewavpdigrspliertfdwleenwpadsaaaigrpepvlcxxxxxxxxxxxxxxxxxWGDSRIGNVMYXkxxxxxxxxXDFxxEPvAVLDWEMATLXGPRELDVAWMIFAHMFF()}*

$15=246(164):
>APH3_orph  {|63(1)|} 
{()-------------------------------------------LAHDMKRVMKLfkhTGVPVPKVYxxAYCEDDSxxxxxNPYIVMEYIPGRINxxxxxxxxQplkkrxxxCW---XXxxxxpxTQREQICEQLKEY-XLAXXxQLHSHxxDIQsfgpwgphnfwrggplslamldegpfedyckrqvdtwtkqyrwstefidenwpserfqhhfdkvwecwmynrtktqpipavfcHGDFCPGNIMVXHnkgkigkdkDDxxKVtGIIDWEMAGWXGPPYCDLAWCCMCYWN-()}*

$16=310(164):
>APH3_0 
{()GGQSNWTYRLTYQGSEXqxxxxxxNqSXexxN--TXDWxVLRRPPPGXhxxxkxA---KXxSahdxxxMAREYRVMSALhpxVYPPVPKMVXxcLCEDESvigxxCPFYVMEFVEGIIFranrwdeqQXXXxxxxxxxxxxxLSPXEQRRQCCENMIDTXLAXXXELHQVxxDYKeavgledfgkpegycerqverwskqyeksktrnipsfdelmkwlndnipqdsktcivxxxxxxxxxxxxxxxxxxxxxxxxxxHNDYRFDNVIF-XhpdnxxxxxxPTxxRViAVLDWEMCTLXGDPLMDLGWMCMYWIQP()}*

$17=310(164):
>APH3_4 
{()HGQSNPTYYIXXXXXXXXxxxxxxxxQK-X--GxxnqqYXVLRKKPPG-XKXXxlXLpxxSahaxxxIDREFRVMKALfnxTGFPVPKVLxxCYCEDSSvigxxTPFYVMEYCQGRIFkdpslpXXGXxxxxxxxxxxxxxVSPXSERRAIYTAMNETXLCXXXQIHSVxxDIQsigledygkggnyckrqvstwtkqyqasthqdipameqliewlmknlpdnnettlvxxxxxxxxxxxxxxxxxxxxxxxxxxxHGDFRLDNLVFXHxxxxxxxxxPTecRViAVLDWELSTLXGHPMCDVAYSCMFYYWP()}*

$18=310(164):
>APH3_9 
{()VAHFAKGRPMQPVWRNeLgghtfqIgGXmxxG--DXTReFIKWQPHG-X---TXxxpxxxxxEidxxxxFANEAQRLRWAqryTTVXXPQVLxxEQGTDQDxxxxxGSWLHTAGIPGRSAvhprwkXXAXXXXXXxxxxxxxxxxxxxNPQTAVRAIGQGXLRXXXTLHDRlxPVHqcpfswsvqsrlkkikqriiqgenpknwmpeyhhhlqkarkelsehppidrlrvvcxxxxxxxxxxxxxxxxxxxxxxxxxxxHGDACSPNTLL-XhDxxxxxxxxdgxXNFcGHVDFGSLGVXADRWADLAVATMSTQWN()}*

$19=310(164):
>APH3_5 
{()FPEQIRTYIEGANFYDsSshsgatVfY-XIXxdTXGYxYLKIDQKG-XRXXXXXxxxxxxxxxxxLAREATMAKWFekxQGLGVPVIHxxYLSADKDXXxxxxxWLLTKEAIGKNCXXXXxxxxlxxxxxxxxxxxxxxAFLXNQPERLCQTMACAXLRXXXKLHSLxxQPQhcpimnhlqhykeqaeknyqkgcfyqkallpqfhihsreeayqliqeqghlltadvfixxxxxxxxxxxxxxxxxxxxxxxxxHGDACLPNFILXkxxxxxxxxXDAwxHFsCFIDVGCAGFXSDRHIDLFWAIWSLNYN()}*

$20=310(164):
>APH3_16 
{()GQSPSDVYSFN-------XXxxxxxxxxxxxxxrxnxxN--EXTFxFLKRSSTL-X---YXxxtxexxxTtysxxxVSREAKMLSWLsxxEKLKVPELIxxMTFQDEQxxxxxFEFMITKAINAKPIsXXXXxxxaxxxxxxxxxxxxxxLFGXLTDQELLAIYKEAXLNXXXQLNSVxxAIIdcpfisnidhrlkeskffidnqllddidqddfdaelwgdhktylslwneltetrveerlvfsxxxxxxxxxxxxxxxxxxxxxHGDITDSNIFI-XDxxxxxxxxxkfxXNEiYFLDLGRAGLXADEFVDISFVERCLRED()}*

$21=309(164):
>APH3_1  {|0(1)|} 
{()-TSCSRMNSPSINWVxEyGykwaqqTiG-XCXxSGXAtxVYRQGRPHXlxxxF---XVkhgKssvfneLQDEMDRLNWLktxTGMPCPTVKxxHWITTQGxxxxxRAWLLTTAIPGKTAsXXXXxxxsxxxxxxxxxxxxxxHYPXLSPEKKWSIMAQQXMRXXXRLHSIxxPVCncpfnhqlkhmieqaqtrmnnglvnqddfddehkgwpqeqlwkrmkkempfrlsqertdmvvcxxxxxxxxxxxxxxxxxxxxHGDFCMPNIMV-XDxxxxxxxxxnKtlKCsGCIDCGRLGIXADRYQDIALMWRNIEE-()}*

$22=310(164):
>APH3_14  
{()-KACIDTEGMSPAXXXXxxxxxxxxKvGXexxnE--XNLXYLKMTDST-XYXXxdxxxxxxxxxxxX--VEREKDMMLWLexxGKLPVPKVLxxHFERHDGxxxxxWSNLLMSEADGVLCseXXXXxxexxxxxxxxxxxxxxYEDXESPEKIIELYAQC-XirxxX---QFHSIxxDISdcpytnsldsrlaeldyllnngkvdladvdcenweedtpfkdprelyqflktnkpqeelvfsxxxxxxxxxxxxxxxxxxxxxHGDLGDSNIFV-XKxxxxxxxxxdgxXKVsGFIDLGRSGRXADKWYDIAFCVRSIRHD()}*

_0].
[0_(1)=PI3K(26){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$0=164(164):
>PI3K.fas consensus seq
{()VYMSSKXQKPKWXITWKGSDPFKYYPFMFKPXXGDLXEXXDXELRQDMRMMQFFRRDLIRPYKCVPTGEECGXXMIEWVPNTCLKPIMEKPAVFHYWSRENYTYSCAGYCMVTYIXXLGIGDRHNDNIMLDKTGHVFHIDFGCCFEHGKTFXXXXILXXXTPEF()}*

$1=521(164):
>PI3K_15  {|0(16)|} 
{()I-LPTKXTKPKKXLLFLGSD-XXxxxxxxgxxxxksxxxxxxxxxxxxxxxxxYXPYLFKG--XXLXEXXxxxdxX--LHLDERIMQFLsivntmfatinrqeTXxxxxxxxxxxxxxpxxxxxxxxxxRFhxxxxxxxxxxxxxxxxxxxxxxxxxxxxARHYSVTPLGTRSGXXLIQxxxxxxxxxxWVXxxxxxxxxxxxxxxxxdxGATpLfglykrwqqREAAXxxxxxxxxxxxxxxxxxxlxxxxxxxxxxQaqkaqdsyqtpqnpgivprpselyyskigpalkavglsldvsrrdwplhvmkavleelmeatxxxxxxxxxxxxxxxxxpxxxPXNLlakELxxxxxxxxxxxxxWsscttpdewwr-xxxxxxxxxxxxxxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxTQSYARSTAVMSMVGYIXXIGLGDRHLDNVLIDmTxTGEVVHIDYNVCFEKGKSlrvpekvp--------------()}*

$2=53(164):
>PI3K.fas  {|459(25)|}consensus seq 
{()--------------------------------------------------------------------------------------------------RENYTYSCAGYCMVTYIXXLGIGDRHNDNIMLDxExTGHVFHIDFGCCFEHGK---------------()}*

$3=521(164):
>PI3K_orph{|0(16)|} 
{()V-MPSKXQKPKKXIWCVGSD-XXxxxxxxgxxxxkexxxxxxxxxxxxxxxxxYXKFIFKGXXHX-----DXXxxxdxxLRQDMRMMQFFgmmnniwkkindpr-----------------------------------nxxxxxxxxxxxxxxkxxxxxxxxxxryhxxxxxxxxxxxxxxxxxxxxxxxxxxxxirpykviptgpncgxxmiexxxxxxxxxxwvxxxxxxxxxxxxxxxxxpxntttlxxxxxxxxxheiixxxxxxxxxxxxxxxxxxxkxxxxxxxxxxDwrqreaaaqnscyqtpqenpgipppvpiqsemfyngitpalkkhgipglnahkrwmpqdwvevfeevceetpxxxxxxxpxxxpxvlxxxhhxxxxxxxXXXXXXWfwckfpdpeawfe-xxxxxxxxxxxxxxxxxxxxxxxxxxrxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRTNYTRSSAGYCMVGYIXXLGIGDRHNGNIMIDdKxTGHVVHIDFGCCFQAGKRlpvpervp--------------()}*

$4=521(164):
>PI3K_9  {|0(16)|} 
{()I-LASLXQKPKKXISLKGSD-XXxxxxxxgxxxxkfxxxxxxxxxxxxxxxxxYXIMMCKPXXKX-----DXXxxxdxxLRKDCRLMEFNslinkclrkdaesrRXxxxxxxxxxxxxxrxxxxxxxxxxELhxxxxxxxxxxxxxxxxxxxxxxxxxxxxIRTYAVIPLNDECGXXIIExxxxxxxxxxWVXxxxxxxxxxxxxxxxxnxNTAgLxxxxxxxxxRPILXxxxxxxxxxxxxxxxxxxtxxxxxxxxxxKlykekgvymtgkelrqcmlpksaalseklkvfqefllprhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxxPXVFxxxHExxxxxxxxxxxxxWflrtfpdptswys-xxxxxxxxxxxxxxxxxxxxxxxxxxsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRSAYCRSTAVMSMVGYIXXLGLGDRHGENILFDsLxTGECVHVDFNCLFNKGETfevpeivp--------------()}*

$5=537(164):
>PI3K_26 
{()I-FHSLXQMPRQXVTIRGSD-XXXXxxxxgxxxxnaxxxxxxxxxxxxxxxxxyxRLMVKXXXkxdxxxxxDX-----X--TRKDAKVVEFTtmvnrilststearK---xxxxxxxxxxxxxxrxxxxxxxxxxglqxxxxxxxxxxxxxxxxxxxxxxxxxxxxIANYSVVPLAEHMGXXVIExxxxxxxxxxFVXxxxxxxxxxxxxxxxxmxDVQtMxxxxxxxxxKGIIXxxxxxxxxxxxxxxxxxxnxxxxxxxxxxEqrkrmgqmvnerkmfmhldeaqkmvkgkhdsdqkamddlvagfrhichecxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxxPXVLxxxHNxxxxxxxxxxxxxWfieqfsdpsawym-xxxxxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRNSFTRSSAVMSMVGYIXXMGLGDRHCENILFFkKxTGAVLHIDFDCLFEKGKTlptpeivpxxfxxxX----RLXXXTQNM()}*

$6=521(164):
>PI3K_30  {|0(16)|} 
{()V-MRSLXAKPRKXITIRGSN-XXXXxxxxgxxxxqtxxxxxxxxxxxxxxxxxyxMFLGKPXXKX-----DXXxxxdxxLRKDARLMDFNsiinkllkansesrRXxxxxxxxxxxxxxrxxxxxxxxxxQLhxxxxxxxxxxxxxxxxxxxxxxxxxxxxIRTYGVVTLNEECGXXFIQxxxxxxxxxxWVXxxxxxxxxxxxxxxxxpxNTIpIxxxxxxxxxRPVLXxxxxxxxxxxxxxxxxxxcxxxxxxxxxxKyydsrgikswnnemsdvfkkikencdkeaahifvtkvlpqfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxxPXVFxxxHExxxxxxxxxxxxxWfietfpepsawla-xxxxxxxxxxxxxxxxxxxxxxxxxxsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRTAYGRTAAVMSMVGFIXXLGLGDRHCENILLDtNxTGDVVHVDFNCLFEKGKTletpervp--------------()}*

$7=522(164):
>PI3K_7  {|0(15)|} 
{()V-MASIXRKPKRXIIIRGHD-XxxxxxxxexxxxREXxxxxxxxxxxxxxxxxyxPFLVKG--XX-----GXExxxxxdxxLRQDQRIEQLFqvmngilsqdaacsQXxxxxxxxxxxxxxrxxxxxxxxxxNMqxxxxxxxxxxxxxxxxxxxxxxxxxxxxLRTYQVVPMTSRLGXXLIExxxxxxxxxxWIXxxxxxxxxxxxxxxxxexNTCtLxxxxxxxxxKDLLXxxxxxxxxxxxxxxxxxxlxxxxxxxxxxNnmsqeekaaytsdpkappceyrdwltkmsgkhdvgaymlmykganrtetvtsfrkreskvxxxxxxxxxxxxxxxxxxxpxxXAdLLxxxKRxxxxxxxxxxxxxAfvkmstgpeafla-xxxxxxxxxxxxxxxxxxxxxxxxxxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRSHFASSHALICISHWIXXLGIGDRHLNNFMVNmExTGGMIGIDFGHAFGSATQflpvpelmp--------------()}*

$8=521(164):
>PI3K_18  {|0(16)|} 
{()V-FSSLXKKPKQXLNIIGSD-XXxxxxxxgxxxxnixxxxxxxxxxxxxxxxxYXGIMCKXXXkxexxxxxDX-----X--VRQDNQYMQFAttmdfllskdiasrK---xxxxxxxxxxxxxxrxxxxxxxxxxslgxxxxxxxxxxxxxxxxxxxxxxxxxxxxITTYSVLSLREDCGXXILExxxxxxxxxxMVXxxxxxxxxxxxxxxxxpxNVVtLxxxxxxxxxRSIFXxxxxxxxxxxxxxxxxxxsxxxxxxxxxxTkyesmkikyslkslhdrwqhtaadgklgfymeqvdkf------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxxpxvlxxxyqxxxxxxxxxxxxxWflenfpdpinwfn-xxxxxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRNTYARSYAVMAMVGHIXXLGLGDRHCENILLDiQxTGKVLHVDFDCLFEKGKRlpvpeivp--------------()}*

$9=521(164):
>PI3K_24  {|0(16)|} 
{()I-LSSLXQRPKKXIVFLGSD-XXxxxxxxgxxxxiexxxxxxxxxxxxxxxxxRXPFLCKPXXKX-----DXXxxxdxxLRKDARMMEFTaminrllskypesrRXxxxxxxxxxxxxxrxxxxxxxxxxKLyxxxxxxxxxxxxxxxxxxxxxxxxxxxxIRTFAVIPLTEDCGXXMVExxxxxxxxxxWVXxxxxxxxxxxxxxxxxpxHTRgLxxxxxxxxxRHILXxxxxxxxxxxxxxxxxxxqxxxxxxxxxxDiyitcgkfdrqktnpqikriydqcqgkmpeyemlktkilpmfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxxPXVFxxxHKxxxxxxxxxxxxxWflttfsepaawfr-xxxxxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRVAYAHTTAVWSMVGHIXXVGLGDRHGENILFDsTxTGDCVHVDFSCLFDKGLQlekpelvp--------------()}*

$10=521(164):
>PI3K_20  {|0(16)|} 
{()I-ASGVXSAPKIXITAVGTD-XXXXxxxxgxxxxvrxxxxxxxxxxxxxxxxxyxKQLVKGXXGnD-----XXXxxdxxLRQDAIMEQVFxxxxxxxxxxxxxxAavsellkhhrttrqRxxxxxxxxxxNLgxxxxxxxxxxxxxxxxxxxxxxxxxxxxIRTYKVLPLTSTSGXXLIEfvpntiplheYLmpaheryypkdwkxxxxgXSQCxrxxxxxxxxXKEIMnvqgkttetrisvyrkvteR-----xxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhxxxpxvmxxxRYffmeyfpdpdewfV-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRTAYTRTTAAISMLGHVXXLGLGDRHGHNILLDtKxTGEVVHIDLGVAFEMGRIlpvpelvp--------------()}*

$11=521(164):
>PI3K_11  {|0(16)|} 
{()L-AGGVXNLPKIXIDCVGSD-XxxxxxxxgxxxxKEXxxxxxxxxxxxxxxxxrxRQLVKXXXGrD-----XXXxxdxxLRQDAVMQQVFxxxxxxxxxxxxxxQmcntllqrntetrkRxxxxxxxxxxKLtxxxxxxxxxxxxxxxxxxxxxxxxxxxxICTYKVVPLSQRSGXXVLEwctgtvpigeFLvnnedgahkryrpndfsAxFQCxqxxxxxxxxXKKMMevqkksfeekyetfmdvcqN-----xxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqxxxpxvfxxxRYfcmekfldpavwfE-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRLAYTRSVATSSIVGYIXXLGLGDRHVQNILINeQxSAELVHIDLGVAFEQGKIlptpetvp--------------()}*

$12=521(164):
>PI3K_16  {|0(16)|} 
{()V-ASGVXSAPKIXVTAVASN-XXXXxxxxgxxxxhrxxxxxxxxxxxxxxxxxyxKQLFKGXXGnD-----XXXxxdxxLRQDAIMEQVFxxxxxxxxxxxxxxEqvsnllkdhratqqRxxxxxxxxxxNLgxxxxxxxxxxxxxxxxxxxxxxxxxxxxIRTYKVLPLTSNAGXXIIEfvqntmplhdYLmpahqryfpkdmkxxxxpXNVCxrxxxxxxxxXKHINdvqtrsfeqrvktyrqvteH-----xxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhxxxpxvmxxxRYffmekfnnpddwfS-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRLAYTRSTAAISMLGHVXXLGLGDRHGHNILLDeKxTGEVVHIDLGVAFEQGRVlpvpevvp--------------()}*

$13=515(164):
>PI3K_29  {|6(16)|} 
{()-------SLPKIXVTFNISD-XXxxxxxxgxxxxttxxxxxxxxxxxxxxxxxQXKALMKGXXSnD-----XXXxxdxxLRQDAIMEQVFxxxxxxxxxxxxxxQqvnkvlqndkvmrnLxxxxxxxxxxDLgxxxxxxxxxxxxxxxxxxxxxxxxxxxxIRTYKVVPLGPKAGXXIIEfvanstslhqILsklhtndkitxxxxxxxfXDQAxrxxxxxxxxXKGMKavqtksneerlkaylkitnE-----xxxxxxxxxxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxxxpxqlxxxRNfffdsfpdpldwfE-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxKKTYTKGVAASSIVGYIXXLGLGDRHLNNILLDcSxTGEPIHIDLGIAFDQGKLlpipelvp--------------()}*

$14=507(164):
>PI3K_28  {|0(30)|} 
{()V-ITSKXQRPRKXLCIHGSD-XXXXxxxxgxxxxndxxxxxxxxxxxxxxxxxyxAFLLKGXXHX-----EXXxxxdxxLRQDERVMQLFxxxxxxxxxxxxxxGlvntllendpecfkRxxxxxxxxxxHLsxxxxxxxxxxxxxxxxxxxxxxxxxxxxIQRYAVIPLSPNSGXXLIGxxxxxxxxxxWVxxxxxxxxxxxxxxxxxPnCDT--------------xlxxxxxxxxxhalixxxxxxxxxxxxxxxxxxxrxxxxxxxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxyrxxxxxxxxxxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxKkkiflniehrcmlqmapdydhlplmqkvevfehaldnttgddlykvlwfkspssevwfdrRTNYTRSLAVMSMVGYIXXLGLGDRHPSNLMLDxRyTGKIIHIDFGDC--------------------()}*

$15=507(164):
>PI3K_0  {|0(30)|} 
{()V-ITSKXQRPRKXLCIKGSD-XxxxxxxxgxxxxKDXxxxxxxxxxxxxxxxxyxMFLLKGXXHX-----EXXxxxdxxIRQDERVMQLFxxxxxxxxxxxxxxGlvntllandpecfkRxxxxxxxxxxHLsxxxxxxxxxxxxxxxxxxxxxxxxxxxxIQRYPVIPLSPNSGXXLIGxxxxxxxxxxWVxxxxxxxxxxxxxxxxxPnCDTxlxxxxxxxxXHVLI---------xxxxxxxxxxxxxxxxxxxrxxxxxxxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxyrxxxxxxxxxxxxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxSrkillniehrimlqmapdydhltlmqkvevfeyaldnttgqdlykvlwlkspssevwfdrRTNYTRSLAVMSMVGYIXXLGLGDRHPSNLMLDxRiTGKIIHIDFGDC--------------------()}*

$16=537(164):
>PI3K_2 
{()RYMDSK-MRPLWXIMWENPDxxAgsdllFXxxxqnxxxxxxxxxxxxxxxxxVXGIIFKNXXGX-----DXXxxxdxxLRQDMLTLQMIxxxxxxxxxxxxxxRimdniwknqgXxxxlxxxxxxxxxxDLryyfllgsvlicscrsciqnavlllcvsrMTPYGCLSTGDCTGXXLIE---------------------xxxxxxxxxxvvrxxxxxxxxxxxxxxxxhxshtxixxxxxxxxxaniqxxxxxxxxxxxxxxxxxxxcnggnmaaaafnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxxxdxalxxxhnxxxxxxxxxxxxxwlkdknpggdydrxxxxxxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxIEEFTLSCAGYCVATYVXXLGIGDRHSDNIMVKXdxTGQLFHIDFGHFLGHFKTkfgikrervpFXXXXILXXXTYDF()}*

$17=537(164):
>PI3K_33 
{()V-FSSKXKKPLMXLVLQNYDxxPaXxxxgxxxxDDXxxxxxxxxxxxxxxxxixMVIQKVXXGX-----DXXxxxdxxLRQDILTLQMIxxxxxxxxxxxxxxRimntmwknagXxxxlxxxxxxxxxxDLcxxxxxxxxxxxxxxxxxxxxxxxxxxxxMLPYQCIATGNETGXXMLE-------------------xxxxxxxxxxlvkxxxxxxxxxxxxxxxxdxsetxyxxxxxxxxxgkimxxxxxxxxxxxxxxxxxxxaddgrrlavcxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnxxxdxalxxxttxxxxxxxxxxxxxWmqkqcarpeskvtfeq-xxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxVENFTHSCAGYCVATYIXXLGIGDRHSDNVMITXrxDGKFFHIDFGHFLGNFKSkfgvkrertpFXXXXKFXXXTQHF()}*

$18=524(164):
>PI3K_6  {|0(13)|} 
{()Y-FNSNXAVPLKXVTMQNVDxxPmXxxxgxxxxENXxxxxxxxxxxxxxxxxixNVMFKCXXGX-----DXXxxxdxxLRQDMLTLQMIxxxxxxxxxxxxxxRimdkiwvqegXxxxlxxxxxxxxxxDMrxxxxxxxxxxxxxxxxxxxxxxxxxxxxMVIFRCFSTGRGRGXXMVE---------------------xxxxxxxxxxmipxxxxxxxxxxxxxxxxnxaetxlxxxxxxxxxrkiqxxxxxxxxxxxxxxxxxxxvehgvtgsfxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdxxxrxplxxxadxxxxxxxxxxxxxwlqkhnpgeeyekxxxxxxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxVENFIYSCAGCCVATYVXXLGICDRHNDNIMLKXTxtGHMFHIDFGRFLGHAQMfgnikrdrapF-------------()}*

$19=524(164):
>PI3K_22  {|0(13)|} 
{()F-FSSNXAMPLKXVTMVNADPmGXXXxxxxxxxxxxxxxxxxxxxxxxxxxxixNVMFKV--XX-----GXExxxxxdxxLRQDMLALQMIxxxxxxxxxxxxxxKimdkiwlkegXxxxlxxxxxxxxxxDLrxxxxxxxxxxxxxxxxxxxxxxxxxxxxMVIFKCLSTGRDRGXXMVE---------------------xxxxxxxxxxmvpxxxxxxxxxxxxxxxxaxsdtxlxxxxxxxxxrkiqxxxxxxxxxxxxxxxxxxxveygvtgsfxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdxxxkxpixxxaexxxxxxxxxxxxxwlrkynpseeeyekxxxxxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxSENFIYSCAGCCVATYVXXLGICDRHNDNIMLRXsxTGHMFHIDFGKFLGHAQMfgsfkrdrapF-------------()}*

$20=537(164):
>PI3K_32 
{()V-LGSAXKQPLRXLVWKNNNxxPlseqXHXxxxlexxxxxxxxxxxxxxxxxFcSMIFKNXXGX-----DXXxxxdxxLRQDMLVLQVLxxxxxxxxxxxxxxEvmdniwkaanXxxxixxxxxxxxxxDCcxxxxxxxxxxxxxxxxxxxxxxxxxxxxLNPYGVLPMGEMIG------------------------xxiiexxxxxxxxxxvvpxxxxxxxxxxxxxxxxnxcqtxixxxxxxxxxfeiqxxxxxxxxxxxxxxxxxxxvgngfmntavxrxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsieasxfmxxxnkxxxxxxxxxxxxxWirkqcgleddkkkkkkkdasknesvqkidnaqatkkyfeSxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxVDRFLYSCVGYSVATYIXXMGIKDRHSDNLMVTXexDGKYFHIDFGHILGHGKTklgiqrdrqpFXXXXILXXXTEHF()}*

$21=537(164):
>PI3K_27 
{()KVMASK-KKPLWXLEFKCADxxPtalsXNXxxxetxxxxxxxxxxxxxxxxxiXGIIFKHXXGX-----DXXxxxdxxLRQDMLILQILxxxxxxxxxxxxxxRimesiwetesXxxxlxxxxxxxxxxDLcxxxxxxxxxxxxxxxxxxxxxxxxxxxxLLPYGCISTGDKIGXXMIE---------------------xxxxxxxxxxivkxxxxxxxxxxxxxxxxdxattxixxxxxxxxxakiqxxxxxxxxxxxxxxxxxxxqstvgntgafxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdxxxexvlxxxnhxxxxxxxxxxxxxwlkekspteekfqaxxxxxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxVERFVYSCAGYCVATFVXXLGIGDRHNDNIMITXexTGNLFHIDFGHILGNYKSflginkervpFXXXXVLXXXTPDF()}*

$22=537(164):
>PI3K_8 
{()KVMASK-KKPLWXLEFKCADxxPtalsXnxxxxETsvXxxxxxxxxxxxxxxixGIIFKHXXGX-----DXXxxxdxxLRQDMLILQILxxxxxxxxxxxxxxRimesiwetesXxxxlxxxxxxxxxxDLcxxxxxxxxxxxxxxxxxxxxxxxxxxxxLLPYGCISTGDKIGXXMIExxxxxxxxxxIVkXxxxxxxxxxxxxxxxdxATTXixxxxxxxxxAKIQxxxxxxxxxxxxxxxxxxxQstvgntrgafK--------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdxxxexvlxxxnhxxxxxxxxxxxxxwlkekcpieekfqaxxxxxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxVERFVYSCAGYCVATFVXXLGIGDRHNDNIMITXexTGNLFHIDFGHILGNYKSflginkervpFXXXXVLXXXTPDF()}*

$23=524(164):
>PI3K_17  {|0(13)|} 
{()Y-FNSNXTLPLKXINFVGCD-XXxxxxxxaxxxxeixxxxxxxxxxxxxxxxxCXPAIFKCXXGX-----DXXxxxdxxLQQDMLTIQMIxxxxxxxxxxxxxxRimnkmwlkegXxxxlxxxxxxxxxxDLkxxxxxxxxxxxxxxxxxxxxxxxxxxxxMVTFNCVPTGYKRGXXMIE-------------------xxxxxxxxxxmvtxxxxxxxxxxxxxxxxexaetxlxxxxxxxxxrkiqxxxxxxxxxxxxxxxxxxxvecgltgsfxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdxxxrxpixxxaexxxxxxxxxxxxxWlgkhnpseleyqr-xxxxxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxVRNFTASCAGYSVATYIXXLGICDRHNDNIMLKXtxSGHLFHIDFGKFLGDAQMfgnfkrdrtpF-------------()}*

$24=537(164):
>PI3K_14 
{()Y-FTSNXALPLKXITFINANxxPmXxxxgxxxxKNXxxxxxxxxxxxxxxxxixSIIFKAXXGX-----DXXxxxdxxLRQDMLVLQIIxxxxxxxxxxxxxxQvmdniwlqegXxxxlxxxxxxxxxxDMqxxxxxxxxxxxxxxxxxxxxxxxxxxxxMIIYRCLSTGKGQG------------------------xxlvqxxxxxxxxxxmvpxxxxxxxxxxxxxxxxdxavtxlxxxxxxxxxakihxxxxxxxxxxxxxxxxxxxrhsgligplxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxexxxnxtixxxkkxxxxxxxxxxxxxWfsqhnhlkadyek-xxxxxxxxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxLRNFFYSCAGWCVVTFIXXLGVCDRHNDNIMLTXkxSGHMFHIDFGKFLGHAQTfggikrdrapFXXXXIFXXXTSEM()}*

$25=531(164):
>PI3K_1  {|6(0)|} 
{()-------FKSALXMPAKITFXxxxxxxxKttenKKhghtsqidgggknkygkYxPVIFKHXXGX-----DXXxxxdxxLRQDQLVIQIIxxxxxxxxxxxxxxtxxxxxxxxxxxxxXLmdrllrkenlDLkxxxxxxxxxxxxxxxxxxxxxxxxxxxxLTPYKVLATSTKHG-----------------xxmmqxxxxxxxxxxfixxxxxxxxxxxxxxxxxpxsmtxvxxxxxxxxxaeilxxxxxxxxxxxxxxxxxxxaxxxxxxxxxxKyggtsvtysggnngetsdvppsvnmnsiqnyfrkhhpdqqseepisksilwtntsqsntlpnvnnnataqrqpehgsngP-------xxxyxgixxxsaxxxxxxxxxxxxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxMDTYVKSCAGYCVITYIXXLGVGDRHLDNLLLTXpxDGHFFHIDFGYILGRDPKpfpppxxxxxmxxxX----KLXXXCKEM()}*

_0].
[0_(1)=ELK(62){go=19,gx=2,pn=5.0,lf=0,rf=0}:
(164)********************************************************************************************************************************************************************

$0=164(164):
>ELK.fas consensus seq
{()XXXXXXXXXXXXXXXXXXXXXXXXXVVAVKVQHKGIEEQIDLLLDQEARNMERFRRXLMXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXKPXVFHEWSPTRYTRSLAKTYMVGXHIPXPHGDFHPGNIMFQPXGQLCYIDFGMYGRSPKXMRTTLCLYXWCHQH()}*

$1=87(164):
>APH2_3  {|712(518)|} 
{()--------------------------------------------------------------------------------------------------PTRFNDSLAKxxxxxxxxxTLAALXHxsxIdhekfrnagvrvktpqearqsmaqnmdeikrqfgisenlwerwqkwladdsywpkhs------------------------------------------------()}*

$2=87(164):
>APH2_3  {|712(518)|} 
{()--------------------------------------------------------------------------------------------------PTRFNDSLAKxxxxxxxxxTLAALXHxsxIdhekfrnagvrvktpqearqsmaqnmdeikrqfgisenlwerwqkwladdsywpkhs------------------------------------------------()}*

$3=96(164):
>APH3_12  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------PkmnwldfqtakfkrkveelkeleitasfltdteqfvyeniarlknrpiclqxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxHGxxDFHPANII-----------------------------------()}*

$4=96(164):
>APH3_12  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------PkmnwldfqtakfkrkveelkeleitasfltdteqfvyeniarlknrpiclqxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxHGxxDFHPANII-----------------------------------()}*

$5=96(164):
>APH3_17  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------TskhlicntlggpicdhrtdtrvwgpynskaeftdmltedvvdieqrrheppisvlyekkhevcftxxxxxxxxxxxxxxxxxxHSxxDLHMSNIF-----------------------------------()}*

$6=96(164):
>APH3_17  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------TskhlicntlggpicdhrtdtrvwgpynskaeftdmltedvvdieqrrheppisvlyekkhevcftxxxxxxxxxxxxxxxxxxHSxxDLHMSNIF-----------------------------------()}*

$7=96(164):
>APH3_19  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------PyigrvnrqptrnpydriethfmgpfdseaqfdewclsrvkgnpsekwkwrralaemrrkspsrfvltxxxxxxxxxxxxxxxxHGxxDLFPRNIM-----------------------------------()}*

$8=96(164):
>APH3_19  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------PyigrvnrqptrnpydriethfmgpfdseaqfdewclsrvkgnpsekwkwrralaemrrkspsrfvltxxxxxxxxxxxxxxxxHGxxDLFPRNIM-----------------------------------()}*

$9=96(164):
>APH3_11  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------MqciggqpcysnflfrnkmephgpfasddelwacmerglhhvpeqarenlrkcmpscapytftxxxxxxxxxxxxxxxxxxxxxHCxxDLTNVNIM-----------------------------------()}*

$10=96(164):
>APH3_11  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------MqciggqpcysnflfrnkmephgpfasddelwacmerglhhvpeqarenlrkcmpscapytftxxxxxxxxxxxxxxxxxxxxxHCxxDLTNVNIM-----------------------------------()}*

$11=96(164):
>APH3_2  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------SngpiagtrgghisvydeeveeriaetrhcienkdvgegfdllkhvwedalqsptwdgkpvwvxxxxxxxxxxxxxxxxxxxxxHGxxDVHPGNLL-----------------------------------()}*

$12=96(164):
>APH3_2  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------SngpiagtrgghisvydeeveeriaetrhcienkdvgegfdllkhvwedalqsptwdgkpvwvxxxxxxxxxxxxxxxxxxxxxHGxxDVHPGNLL-----------------------------------()}*

$13=96(164):
>APH3_3  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------PcctlwmlpdpnkipandsirsyrhyydsmgwpypvmewilnwcekhappssdvclvxxxxxxxxxxxxxxxxxxxxxxxxxxxHGxxDFRTGNYM-----------------------------------()}*

$14=96(164):
>APH3_3  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------PcctlwmlpdpnkipandsirsyrhyydsmgwpypvmewilnwcekhappssdvclvxxxxxxxxxxxxxxxxxxxxxxxxxxxHGxxDFRTGNYM-----------------------------------()}*

$15=96(164):
>APH3_orph  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------QsfgpwgphnfwrggplslamldegpfedyckrqvdtwtkqyrwstefidenwpserfqhhfdkvwecwmynrtktqpipavfcHGxxDFCPGNIM-----------------------------------()}*

$16=96(164):
>APH3_orph  {|914(307)|} 
{()----------------------------------------------------------------------------------------------------------------------QsfgpwgphnfwrggplslamldegpfedyckrqvdtwtkqyrwstefidenwpserfqhhfdkvwecwmynrtktqpipavfcHGxxDFCPGNIM-----------------------------------()}*

$17=269(164):
>ChoK_5  {|809(239)|} 
{()--------------------------------------------------------------------------------------------------------------------Pllswerkngpacwqkinqwlrtiekvdqwvgdpkiqnvllcenwskfmdivdryhkwlieqgieqvnknlv--xxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxHNxxDAQYGNLLxFtapvmktistaisspfldptdpnhsintpslytatsltsqssslfpsssnvivddiinppkqeqsq----------------------------------()}*

$18=535(164):
>FruK_2  {|201(581)|} 
{()-----------------------------------------RemgseggadrqgfwllvmdyiepgsastllaredlirslklLxxxxxxYRxxKDSNSWGWK----------------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxxxxxnnfixxxxxxxgxxtxxxxxxxxxxxxxxxxxxxxxxxxxxxxxlxsxxxxxxxxxxxxxxxxxxxxqknxxqxxxxxxwxxyxstxxxxxfxxxxxxekxxxfxxxxxxxxxxfwxxxxxxxxxxxxxxxxxexsrlstxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-----LDLAFSRKLittkdaenvKYVFQ---------------------------------------------------()}*

$19=1117(164):
>UbiB_31  {|191(9)|} 
{()----------------------------------GIKTIIleDxLknaksivdwiawaepqynfnpmidewckeapkxxxxxxxxxexxxxxXLDfnHEAENTRTV------------------------------------------------------------------------------------xxxxxxxxxxxxxxsxxxxxxxxxxxxxxkxxxxxxxxxxnxxlgcrxxxxxxxxxxxxxxxxxxxxxxxxxxnkcdsnmsanqvdxxxxxxxxxxvlixxpeviqxxstxexxxxxxxxxkxxxxvlilxxexxxxxxxxxxymxxxxxxxxxxxxxxxxxdxgixxrxlxxxxxxxxxnxxxxxxxxdnxxxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxslxxxeaxxxxxxxxxxxxxyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgvdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkqklveeitrxxxxxxxxxayahqxiyvxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxngxxdphpgnflxvskexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrpxilxxLDFGLTxKKiSSSmkqalakmflacaegdhvallsafaemglklrldmpeqamevttvffrqstpaneahetmktlndqrernmkviqekmkmnqkevkrfnpvdafpgdivixxxxxxxxxxxfxxxxxxxxxXMRVLNLLR------()}*

$20=1071(164):
>UbiB_25  {|191(55)|} 
{()----------------------------------GVEDLMmtDxIrnmqafalymqktdikfdlysvtkemekqicyxxxxxxxxxexxxxxXFDftREANAMERI-------------------------------------------------------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxxxxxcxxxxxxxxxxfxxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxyxennkkkspxxxxxxxxxxxxxvlvxxprvipnmvtxrxxxxxxxxxrxxxxvlvmxxexxxxxxxxxxyixxxxxxxxxxxxxxxxxnxgixxpxixxxxxxxxxmxxxxxxxxnlxxxxgxxxxxxxxxxxxxxxxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxemxxxakxxxxxxxxxxxxxrxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxginphgkiaaaaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkqnilssltlxxxxxxxxxaygqmxilkxsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnilxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxcxxxxxxxxxkxxGseashqftclfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxiVXALxxLDYGQVxKDlPDHlrlgyanlviamadndpsrasesyrelgietwskceneqqelfqlaqtmfdtklppgvtmmqpfsedssikkigvesfpeelfs---------------()}*

$21=1074(164):
>UbiB_43  {|191(52)|} 
{()----------------------------------CIRYQVfwDxLytyrfvmwmigalfnmpvdwskktvcdgirrxxxxxxxxxxexxxxxXMDfsMEANNAKRM-------------------------------------------------------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxxxxxcxxxxxxxxxxhxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxaxdnpnxxxxxxxxxxxxxxxxxvyvxxpkvhdhyvtxpxxxxxxxxxqxxxxllvmxxexxxxxxxxxxwixxxxxxxxxxxxxxxxxdxgtxxkxlxxxxxxxxxnxxxxxxxxdvxxxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtvxxxrqxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqydxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxektvlttlfdxxxxxxxxxafgdmxifkxhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhcxxdphganvlxvrpppkpxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgtdmxxxxxxxxxxxxxxxxaxxxxxxxxxaxxGtstqsqqqhhkccnpxxxxxxxxxxxxxxxxxxxxxxxxxxxxxQVXVLxxiDFGLCxCPeSERfryeyallfksmfthdmetimkivhdwgindaemfasiqmqkpyesiqrgnygevtrmevihmqtkaheraknilrnqeripkelpm---------------()}*

$22=1147(164):
>UbiB_18  {|167(3)|} 
{()---------------------------AIKIQXKxxxxxxxxxxxxxxxpxEIAKQIewDxLwafkvvmwiyskwfdipfyslvpyicerlflxxxxxxxxxxexxxxxXTDfeNEADNSETM------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxexxxxxxxxxxlxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnxseprlrgrxxxxxxxxxxxxxvyixxpkvypelssxkxxxxxxxxxrxxxxvmttxxexxxxxxxxxxwixxxxxxxxxxxxxxxxxexgcxxrxlxxxxxxxxxwxxxxxxxxdkxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxaixxxtrxxxxxxxxxxxxxPwrggwgqgspgchgtpldpdmeaprrelrtnppnyqekikperdewkgrngrgglGls----------------------------------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtkevmttmvdxxxxxxxxxlfsaqxmfkxwxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxwxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhcxxdphpgnifxirrxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhxxxxxxxxxxxxxxxxxxxpxxxxxxxxxnxxgkaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxelxvlxxIDHGLYxVHmSPKfrhqycrfwkalmtfdnktiseiteewgiknpdmfasatlmrpyeggddhstqnhlmkglsknktpaerhyemqqrmkqgirdmladedkwpkelifxxxxxxxxxxxxxxixxxxxxxxxXGRNMRIVQxgXNNQ--()}*

$23=1069(164):
>UbiB_39  {|189(59)|} 
{()--------------------------------HXYIENQVvvDxLwcyrfiskvyekvfdipcsmftkyvceqmhhxxxxxxxxxxexxxxxXTDfvHEMQNSEKL------------------------------------------------------------------------------------xxxxxxxxxxxxxxqxxxxxxxxxxxxxxqxxxxxxxxxxfxxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxaxndpsmknmcxxxxxxxxxxxxvhixxpknypelttxkxxxxxxxxxqxxxxvltaxxexxxxxxxxxxwcxxxxxxxxxxxxxxxxxdxgvxxpxlxxxxxxxxxtxxxxxxxxhkxxxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxelxxxlaxxxxxxxxxxxxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkydxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxlttimkqyikxxxxxxxxxlfgrqxifkxyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhcxxdphpgnllxvcfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdxxxxxxxxxxxxxxxxxxxsxxxxxxxxxnxxgkqxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqlxvlxxLDHGLYxITlPEKfrcqycnlwrylfclntkgieeigrdwgigstdmfatmvqlrpvlmdpentsppedtrdmsdlfrdfigdekkfpmelpf---------------()}*

$24=1073(164):
>UbiB_47  {|191(53)|} 
{()----------------------------------DIAHRFngDxMrtiqlmfniagfffpgydfsqiicklnntvanxxxxxxxxxexxxxxXMDftIEADNCERA------------------------------------------------------------------------------------xxxxxxxxxxxxxxgxxxxxxxxxxxxxxrxxxxxxxxxxdxxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxacgwgnrxxxxxxxxxxxxxxvvcxxpdvfwnyttxrxxxxxxxxxrxxxxvlttxxrxxxxxxxxxxfipxxxxxxxxxxxxxxxxnxaaxxkxixxxxxxxxxsxxxxxxxxdrxxxxrxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgixxxeaxxxxxxxxxxxxxmxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxglkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpktvatwfydxxxxxxxxxaiayqxifixtxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhgxxdphagnilxvhkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxlxxxxxxxxxxxxxxxxxxxpxxxxxxxxxnxxgkpxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqvxvlxxLDFGLCxTElSDEmrrelaciwtasvthdtptikqiakrfgcedyalfascflqhpyeyfsaetnlttkmtmemmreqvknrmheinnivsglpkeyal---------------()}*

$25=1075(164):
>UbiB_36  {|191(51)|} 
{()----------------------------------GLEQQMkiDxTmtmsflskcvswifpdyrfewlvtefektmsmxxxxxxxxxexxxxxXLDfiQEAKNSERT------------------------------------------------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxsxxxxxxxxxxcxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrxnnnmxxxxxxxxxxxxxxxxxvkixxpcvfwelttxrxxxxxxxxxqxxxxvltmxxexxxxxxxxxxfcxxxxxxxxxxxxxxxxxhxghxxkxvxxxxxxxxxdxxxxxxxxdlxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxflxxxrkxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdisxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxptkvakaliexxxxxxxxxvfaemxifcxhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhgxxdphpgnilxvshnparxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxegxxxxxxxxxxxxxxxxxxqxxxxxxxxxnxxgfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxslxvlxxLDHGIYxKElDEKfrldycqlwkamilldsnkiqelgeqfgvgkyakyfpviftgrtidsksalgtqmsieektrlkqdlnslgmddissfmeslppdflt---------------()}*

$26=1123(164):
>UbiB_19  {|191(3)|} 
{()----------------------------------DLRDRFdgDxIhtieflldivefmhpsfgfswvlqdlkgtlaqxxxxxxxxxexxxxxXLDfeNEGRNAERC------------------------------------------------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxrxxxxxxxxxxexxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxhfhyxxxxxxxxxxxxxxxxxvvvxxpkvhwdktsxkxxxxxxxxxrxxxxvltaxxexxxxxxxxxxfcxxxxxxxxxxxxxxxxxdxgcxxkxvxxxxxxxxxnxxxxxxxxdvxxxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxaixxxksxxxxxxxxxxxxxqxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxglsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxvqdiaekliqxxxxxxxxxafaeqxifyxtxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhsxxdphpgnvfxvrkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxpxxxxxxxxxdxxgkaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxelxvlxxLDHGLYxQFlPEKdrsalcqlwraiilrddaamkahaaalgvqdymlfcemlmqrpvrmgqlwgshllsreeaaymqdmarehfehimgvlkamprpmllxxxxxxxxxxxxxxxxxxxxxxxxvxxxxxxxxxXLRNINTVRXAXINH()}*

$27=1076(164):
>UbiB_42  {|191(50)|} 
{()----------------------------------KVQAQSskDxIllmevlvlavkqlfpefefmwlvdeakknlplxxxxxxxxxexxxxxXLDflNEGRNAEKV------------------------------------------------------------------------------------xxxxxxxxxxxxxxsxxxxxxxxxxxxxxqxxxxxxxxxxmxxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxhfdfxxxxxxxxxxxxxxxxxlkvxxprihwdlstxexxxxxxxxxrxxxxvllmxxexxxxxxxxxxfvxxxxxxxxxxxxxxxxxdxggxxqxvxxxxxxxxxnxxxxxxxxdrxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxymxxxerxxxxxxxxxxxxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkidxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxvneisrhlgkxxxxxxxxxmysemxifvxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhcxxdphpgnvlxvrkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxgtgkaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxeixvlxxLDHGLYxQVlTEEfrlnychlwqsliwtdmkrvkeysqrlgagdlyplfacmltarswdsvnrgisqapvtatedseirnnaanylpqisqllnhvprqmll---------------()}*

$28=1147(164):
>UbiB_12  {|167(3)|} 
{()---------------------------AVKVQxHXxxxxxxxxxxxxxxpxHVQAQSskDxIktmevlvnavhwifpdfefmwlvdeakknlpkxxxxxxxxxexxxxxXLDflNEGRNAEKC------------------------------------------------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxqxxxxxxxxxxmxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxhfdwhianyxxxxxxxxxxxxlkvxxpkiywelstxsxxxxxxxxxrxxxxvltmxxexxxxxxxxxxfcxxxxxxxxxxxxxxxxxdxggxxqxvxxxxxxxxxnxxxxxxxxdrxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxymxxxerxxxxxxxxxxxxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkidxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpneisrhlgkxxxxxxxxxmysemxifvxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhcxxdphpgnvlxvrkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxcxxxxxxxxxxxxxxxxxxxpxxxxxxxxxgxxgkrnifgwrkpxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxeixvlxxLDHGLYxQNlTDEfrwdychlwqsiiwtdmkamkeysqrlgagdlyplfacmltmrpwdsvnrpgidqlvtpytatedseiqnnaanyfpqisqllnhvprqmllxxxxxxxxxxxxxxxxxxxixxxxxxxxxXLKTNDCLRxgXIEH--()}*

$29=1123(164):
>UbiB_21  {|191(3)|} 
{()----------------------------------GVADSIssDxLamvkpiairmfnmqgkdsdhyfkevedkllexxxxxxxxxxexxxxxXTDyiLEVRQSQEI------------------------------------------------------------------------------------xxxxxxxxxxxxxxsxxxxxxxxxxxxxxexxxxxxxxxxaxxcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsxhipnxxxxxxxxxxxxxxxxxlvfxxpkyypelssxexxxxxxxxxrxxxxiitmxxdxxxxxxxxxxwmxxxxxxxxxxxxxxxxxdxgvxxhxlxxxxxxxxxsxxxxxxxxewxxxxtxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxatxxxnpxxxxxxxxxxxxxsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqeaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrnkigqalwdxxxxxxxxxfymfqxmhnxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnflxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdxxxxxxxxxxxxxxxxxxxkxxxxxxxxxdxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnlxiaxxIDFGCIxKViPEDfytpyfelanpeninnpeifnqklyeleilrpddtpeeivffsamfhemlslftkpfhvetfdfsdetfwnqiaalgerysndtelrkmngnrgsrhflyxxxxxxxxxxxixxxxxxxxxXNRTFFGLYxnXLMH--()}*

$30=1079(164):
>UbiB_20  {|191(47)|} 
{()----------------------------------GVDQAIraDxMknlelftkfwkfkkmplnnkafmdeismnieexxxxxxxxxexxxxxXLDyrHEARNQHRF----------------------------------------------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxexxxxxxxxxxlxxyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrxghpfxxxxxxxxxxxxxxxxxirixxpdcipelctxsxxxxxxxxxhxxxxvlvmxxexxxxxxxxxxylxxxxxxxxxxxxxxxxxdxgqxxpxwxxxxxxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxymxxxqqxxxxxxxxxxxxxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdqdexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxknrwgeiiyrxxxxxxxxxfycgsxmyhxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxexxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxcgxxdphpgnylxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnxxxxxxxxxxxxxxxxxxxpxxxxxxxxxdxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtvxGFxxVDFGCCxKRmNPHhvwfwrhcmraaategrkqdmyelmvergfidpdssmtpqdcyewchavsgwvdepqpmtytpeiassamilavdprssgmrnqnlppehif---------------()}*

$31=1126(164):
>UbiB_6  {|191(0)|} 
{()----------------------------------GADEALrsDxLkqmsrmsrlfkqlspgmdikpmidelidrteexxxxxxxxxexxxxxXLDyrLEADNQRAF------------------------------------------------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxkxxxxxxxxxxaxxyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhxddpdxxxxxxxxxxxxxxxxxfyvxxphvvhxxscxpxxxxxxxxxkxxxxvivtxxexxxxxxxxxxwmxxxxxxxxxxxxxxxxxdxgtxxpxlxxxxxxxxxsxxxxxxxxkixxxxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxanxxxgxxxxxxxxxxxxxxtxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqeqxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrnrcgqlmaexxxxxxxxxfhfssxpyrxcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxlxxxxxxxxxxxxxxxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhcxxdphpgnfmxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxlxxxxxxxxxxxxxxxxxxxpxxxxxxxxxdxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrmxcvxxIDFGTCxDRyPNGlppeigmclrmaldgnydevyetmckngfikpsiqvspdeildylrpyiepiyvesfhftrkwmqkqaaritdprspqyktgrqmnlppsylmxxxxxxxxxxxxxxxxxxixxxxxxxxxXHRVTLSCIxgXICCxQL()}*

$32=1073(164):
>UbiB_34  {|191(53)|} 
{()----------------------------------GLLAQVqmDxLllmkmgsrvlaclpgikwlslpeiveefeklmvqxxxxxxxqxxxxxXIDlrYEARNLEHF-------------------------------------------------------------------------------xxxxxxxxxxxxxxqxxxxxxxxxxxxxxcxxxxxxxxxxnxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqxnvnsxxxxxxxxxxxxxxxxxvkfxxptplrpfvtxrxxxxxxxxxdxxxxvlvexxtxxxxxxxxxxyexxxxxxxxxxxxxxxxxexsvxxpxvxxxxxxxxxsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsyxxxqqxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgipmdlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkrkiarmginxxxxxxxxxmllkmxifvxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdlhpgnilxvqgadwpxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqdssdxxxxxxxxxxxxxxxsxxxxxxxxxqxxGlsqeaqtqqmdvcdtlvvavtparcplXxxxxxxxxxxxxxxxxrlxvLXXLDAGIVxAElQAAdlrnfravfmavvmgqgqrvaelilhharasecrdvegfkaematlvtqarkntitleklqvssllsnvfkllmthkvklesnfas---------------()}*

$33=1149(164):
>UbiB_23  {|165(3)|} 
{()-------------------------VXAVKVRxHXxxxxxxxxxxxxxxpxGVGESIrrDxFviinwvakistfiptlnwlrldesvqqfavfmmsxxxxxxxqxxxxxXVDlaREAAHLSRF------------------------------------------------------------------------------------xxxxxxxxxxxxxxixxxxxxxxxxxxxxyxxxxxxxxxxnxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrxrwkdxxxxxxxxxxxxxxxxxvsfxxpkplyplvhxpxxxxxxxxxaxxxxvlvexxtxxxxxxxxxxyexxxxxxxxxxxxxxxxxqxgexxsxvxxxxxxxxxshxxxxxxxyvxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdlxxxegxxxxxxxxxxxxxhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxerixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxksalahigthxxxxxxxxxallkmxllvxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdmhpgnilxvrvnqnkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnxxxxxxxxxxxxxxxxxxxsxxxxxxxxxnxxktlfkskpxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhvxvfxxLDVGMTxAElSKNdrvnlleffkavarrdgrtaaectlklskqqncpnpkafieeveesftfwgtpegdlvhpadcmhqlleqvrrhkvnidgnvctxxxxxxxxxxxxxxxxxxxxxxxxxxxvxxxxxxxxxXMVTTLVLExgXWQR--()}*

$34=1170(164):
>UbiB_7  {|144(3)|} 
{()--------------------------pkkvrknvdtvvppqrvpssyVxAIKVLxHXxxxxxxxxxxxxxxpxNVERTIrrDxLkimgffanminaiptmewlslpdeveqfgemmklxxxxxxxqxxxxxXLDlrIEAANLARF-----------------------------------------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxxxxxkxxxxxxxxxxnxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkdrttpxxxxxxxxxxxxxxxxxawfxxpypyteyttxrxxxxxxxxxnxxxxvlvexxexxxxxxxxxxyaxxxxxxxxxxxxxxxxxqxgixxpxmxxxxxxxxxexxxxxxxxdfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxmexxxngxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgvyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqhdiadegldxxxxxxxxxaflhmxllixdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxHAxxDLHPGNIMxVrfykpsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxQpdllknlwnrwkhkfdpdyPXxxxxxxxxexxGhpspgkhsnedvteqvlhrlrphrhdpeawcaelaqidaegyrpQIxCFxxIDTGLVxTElNATnrrnfldlfravaefdgykaghlmcercrqpetvidkeifalkmqhlvlgvksrtfalgnikigdilqqvlsmvrnhhvrmegdfvnxxxxxxxxxxxxxxxxxxxxxxxxvxxxxxxxxxXVISILLLExgXIGR--()}*

$35=1072(164):
>UbiB_44  {|191(54)|} 
{()----------------------------------GITQTMrhDxLalictgaaligrlpkmrgmplaemvdqmcasiygxxxxxxxqxxxxxXLDfqREATNLTRL------------------------------------------------------------------------------------xxxxxxxxxxxxxxhxxxxxxxxxxxxxxexxxxxxxxxxnxxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsxtlpnxxxxxxxxxxxxxxxxxiwvxxpkvhpelcrxpxxxxxxxxxnxxxxcivmxxexxxxxxxxxxwixxxxxxxxxxxxxxxxxpxglxxxxxxxxxxxxxxxxxxxxxxxdhxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtqxxxhhxxxxxxxxxxxxxcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxspemxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrkkfadntmrxxxxxxxxxamyqmxlfcxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhcxxdlhpgnlyxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtxxxxxxxxxxxxxxxxxxxkxxxxxxxxxsxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqvxvixxLDAGFSxWQlTDKmrenfaeffynmginngwqcaeivlssaihvgedydidgftedmkdlvkrnhgtpakefdlisfatemfdiqrrngiyassefvf---------------()}*

$36=1071(164):
>UbiB_35  {|191(55)|} 
{()----------------------------------GIEKQVraDxIciinwfnhlfrplfshytknsieavinefskmivqxxxxxxexxxxxXCNmtVELHNLKKF---------------------------------------------------------------------------------xxxxxxxxxxxxxxwxxxxxxxxxxxxxxqxxxxxxxxxxvxxyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxencgpemxxxxxxxxxxxxxxxxvrfxxpkpypcyccxcxxxxxxxxxdxxxxalvmxxsxxxxxxxxxxfexxxxxxxxxxxxxxxxxexgwxxrxfxxxxxxxxxdxxxxxxxxdkxxxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxglxxxqkxxxxxxxxxxxxxhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgidxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfktimdklvdxxxxxxxxxfyteqxmlvxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxyxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnllxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnxxxxxxxxxxxxxxxxxxxcxxxxxxxxxqxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxelxtLXXLDFGMVxKRiPNDtrvamievvkaaherdyetyisackrlgtityeapqsemmeftermfdifgndnlsaesmqklafevmesmrdmpfklpqdaiy---------------()}*

$37=1117(164):
>UbiB_22  {|191(9)|} 
{()----------------------------------GIETLIanDxLknirsflkrinrylfrfeykkihdeithlvtrxxxxxxxxxexxxxxXTDlqLEADSYDRM----------------------------------------------------------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxxxxxqxxxxxxxxxxlxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxaxeepdxxxxxxxxxxxxxxxxxyvfxxpkvirqfsgxkxxxxxxxxxsxxxxvlvtxxexxxxxxxxxxfixxxxxxxxxxxxxxxxxexgvxxkxixxxxxxxxxtrxxxxxxxatxxxxpxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxvlxxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgqaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxksrpvellvrxxxxxxxxxayvlmxifqxyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnlixyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtxxxxxxxxxxxxxxxxxxxpxxxxxxxxxdxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxklxCFxxIDFGAVxGEmNSMgvyalkkiflcaiakdyygvvdglddmgalsasadrdkleevvrysleklgrfvadtdyfrnlsleqihtredrlflkeinsslkeifrmiqipenfifxxxxxxxxxxxxlxxxxxxxxxXERVLGLLV------()}*

$38=1071(164):
>UbiB_16  {|191(55)|} 
{()----------------------------------NIAEEIrlDxLqlferaiplikyipetsvvdlkgvlqevkrslinxxxxxxxexxxxxXMDflKESQNGEQF------------------------------------------------------------------------------------xxxxxxxxxxxxxxyxxxxxxxxxxxxxxqxxxxxxxxxxkxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnxgwkexxxxxxxxxxxxxxxxxirsxxpkiydawcsxkxxxxxxxxxkxxxxvivmxxexxxxxxxxxxymxxxxxxxxxxxxxxxxxsxgkxxnxixxxxxxxxxnxxxxxxxxhlxxxxmxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnaxxxngxxxxxxxxxxxxxtetfitxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgiqnkqlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkqevakllvexxxxxxxxxnfmkqxvfdxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnllxfhvltkexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxeqtrasrktetvheikedpvpxxxxxxxxxpxxyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtvxnyxxIDFGMMxGHlSAGlrqkltqavlalytkdayriekavlrlcnqegsfdesrfhqeltsfleqyydspideinfqevfyhvvtichqnnlqfdrditm---------------()}*

$39=1071(164):
>UbiB_14  {|191(55)|} 
{()----------------------------------GIDKIVaqDxIalikgiadlvartefgqdydivalaeefckavkaxxxxxxxexxxxxXLDftQEAHYTDQL------------------------------------------------------------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxxxxxyxxxxxxxxxxnxxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsxksrwfdpkqxxxxxxxxxxxxlvvxxpkiywhlttxexxxxxxxxxkxxxxlmvmxxexxxxxxxxxxwlxxxxxxxxxxxxxxxxxdxgkxxpxixxxxxxxxxlxxxxxxxxeaxxxxdxxxxxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxshxxxppxxxxxxxxxxxxxhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgkdisqrxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrkeittllfrxxxxxxxxxaffqqxiyixdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnifxyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxlxxxxxxxxxxxxxxxxxxxdxxxxxxxxxdxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrvxalxxIDCGMVxGRlDPRtqqiltemllaivdldaqrcaqltlelsesgqpvnlsrlendydrmlrkyynlslsqinfsevfyeilqvarnnkiklpgnmgl---------------()}*

$40=1071(164):
>UbiB_27  {|191(55)|} 
{()----------------------------------HIDEIVteDxIqllirlarhipkhfipmvdvqevlqqlcetltcxxxxxxxxexxxxxXIDfrNEAEAMKCF------------------------------------------------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxaxxxxxxxxxxyxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqxrvpcxxxxxxxxxxxxxxxxxvgvxxpevydefttxpxxxxxxxxxhxxxxmivexxexxxxxxxxxxyixxxxxxxxxxxxxxxxxpxgixxpxixxxxxxxxxnxxxxxxxxhyxxxxsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqlxxxqexxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgydxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxledvgkklmlxxxxxxxxxsfikqxvfkxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxyxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhgxxdphpgnllxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrxxxxxxxxxdxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkixcfxxIDFGIMxGElENGmraslndilysftaqdvdgmtqailsitqfdngmnkavlsqdvermlgrysgvdlgslsmtdmfedlvaicqknhlkassqiti---------------()}*

$41=1069(164):
>UbiB_32  {|191(57)|} 
{()----------------------------------NISETMkrDxLdilihfalwisnkstwaknigfrdlahgfsiamkexxxxxxexxxxxXIDfkIEARNFEQV------------------------------------------------------------------------------------xxxxxxxxxxxxxxsxxxxxxxxxxxxxxdxxxxxxxxxxsxxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxknsetkxxxxxxxxxxxxxxxxxvkixxpkvyqeycnxsxxxxxxxxxkxxxxilvmxxexxxxxxxxxxylxxxxxxxxxxxxxxxxxdxgvxxsxvxxxxxxxxxksxxxxxxxgsxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxlfxxxnexxxxxxxxxxxxxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqidxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtkkvqrqvfdxxxxxxxxxcileqxmffxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxixxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnvyxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdexxxxxxxxxxxxxxxxxxrxxxxxxxxxdxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtpxalxxLDFGSVxGRlGTLqqdafkrfligfernnsyvlcdgllqlvekkpnvdkegmeqsisqlliqstyvstnpteefihglfqiiaefelafypmvag---------------()}*

$42=1071(164):
>UbiB_40  {|191(55)|} 
{()----------------------------------GAKEQIetDxMrilrrivlvmqwfvpslrrwgpaaivdevwanfryxxxxxxexxxxxXMDlsREASNVRRF------------------------------------------------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxdxxxxxxxxxxaxxwxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrxgspdxxxxxxxxxxxxxxxxxivixxpdvvdglctxpxxxxxxxxxtxxxxvmvqxxqxxxxxxxxxxfsxxxxxxxxxxxxxxxxxhxgaxxxxxxxxxxxxxxxxxxxxxxxhlxxxxhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxglxxxpsxxxxxxxxxxxxxsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxmxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgpayagklvdxxxxxxxxxsyvhqxifrxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhgxxdphpgnvfxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxmxxxxxxxxxxxxxxxxxxxdxxxxxxxxxdxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrixclxxHDFGIVxGRvDGNmrralvafalafveqdaewvvdawldlgllgegtdravfvpvvrtivadcaqhplkdwslsdamaqlvaagrtqnvrlprdllv---------------()}*

$43=1071(164):
>UbiB_0  {|191(55)|} 
{()----------------------------------NIEPVIkqDxIklmywlarwverywpdgrrlhpvevvreyektihdxxxxxxexxxxxXLDlmREAANCSQL-----------------------------------------------------------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxxxxxrxxxxxxxxxxnxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxexdspmxxxxxxxxxxxxxxxxxlyvxxpevywdycsxexxxxxxxxxnxxxxvmtmxxexxxxxxxxxxwixxxxxxxxxxxxxxxxxyxgixxpxvxxxxxxxxxsxxxxxxxxdixxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtlxxxkaxxxxxxxxxxxxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgtnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxmkklaergvqxxxxxxxxxifftqxvfrxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdmhpgnifxvsyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxexxxxxxxxxxxxxxxxxxxhxxxxxxxxxpxxwxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxnpxcYiaIDCGIMxGSlNKEdkrylaenfyaffnrdyrrvaelhvesgwvppdtnveefeqairtvcepifekplceisfghvlmnlfnttrrfnmevqpqlvl---------------()}*

$44=1077(164):
>UbiB_38  {|191(49)|} 
{()----------------------------------GIDKIVdvDxLaalrkvcgwlshvrfvshrvdmpalveefaqtshexxxxxxexxxxxXIDylHEASNSERF------------------------------------------------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxaxxxxxxxxxxnxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxaxddprxxxxxxxxxxxxxxxxxctyxxpevvwerctxrxxxxxxxxxcxxxxvltlxxexxxxxxxxxxdvxxxxxxxxxxxxxxxxxtxaixxkxixxxxxxxxxtxxxxxxxxdhxxxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxglxxxraxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgidxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxphevapvfasxxxxxxxxxvmfdqxlftxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnifxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtpvdxxxxxxxxxxxxxxxxpxxxxxxxxxdxxgsdqsehpwxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxklxtfxxIDFGMMxGEvPPNtrrglrkmliaaasrdgkglvncmsdvgvllpsadttqleramtqlfdrfggmgfaelqhvdpkefrdfghefgdvmrsmpfqlpenfll---------------()}*

$45=1072(164):
>UbiB_13  {|191(54)|} 
{()----------------------------------GIYETMaqDxIslmrsaaklatpvmrtkqvvdfnmvvdelwntfqqxxxxxxexxxxxXMDflNEARNLEEF----------------------------------------------------------------------------------xxxxxxxxxxxxxxkxxxxxxxxxxxxxxrxxxxxxxxxxfxxcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxdykyxxxxxxxxxxxxxxxxxmdcxxpkpyreyctxqxxxxxxxxxhxxxxvlvmxxexxxxxxxxxxyixxxxxxxxxxxxxxxxxdxgixxpxixxxxxxxxxdxxxxxxxxhpxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqlxxxhexxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgydxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxlkeigtklvdxxxxxxxxxnyckqxildxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgniwxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrxxxxxxxxxgxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxqixVWxxIDLGMMxGRlSNRdrsvlknmifavakndspalkdavlrfggtkgdppdhpqlyadidvimkqygtcdfgdldigeflqqltqmakrhhiecphgvtm---------------()}*

$46=1071(164):
>UbiB_26  {|191(55)|} 
{()----------------------------------EIKEKMhmDxLailrriftltkwhttdtlidpveafdelwnatnqxxxxxxxexxxxxXLDfnNEAENTKRF----------------------------------------------------------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxxxxxyxxxxxxxxxxfxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxnvacxxxxxxxxxxxxxxxxxcyaxxpytidkycsxsxxxxxxxxxkxxxxiltmxxexxxxxxxxxxyixxxxxxxxxxxxxxxxxhxgcxxkxixxxxxxxxxtxxxxxxxxdtxxxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxslxxxdexxxxxxxxxxxxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgydxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfedvahklanxxxxxxxxxsyckqxvfexdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhgxxdphpgnlfxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrxxxxxxxxxcxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkixCYxxIDFGIMxGTlSSWlrnwlnkamvamafgdvdtlvsvvmsigikkgyvnrnhlyedinylfdtylstplknikmstmlqevfecanrnniqlpreltm---------------()}*

$47=1070(164):
>UbiB_29  {|191(56)|} 
{()----------------------------------GIREQVerDxLdivnrickwchkntdwarsmgvkdladgfaeslhexxxxxxexxxxxXLDyrIEARNMKTV-------------------------------------------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxhxxxxxxxxxxnxxcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxaxyypxxxxxxxxxxxxxxxxxxvtixxpkvykeyctxexxxxxxxxxrxxxxvlvmxxexxxxxxxxxxwlxxxxxxxxxxxxxxxxxdxgkxxpxlxxxxxxxxxdcxxxxxxxakxxxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxelxxxdnxxxxxxxxxxxxxcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgcdrxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrhelartllnxxxxxxxxxcmmhqximixhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnimxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxlxxxxxxxxxxxxxxxxxxxhxxxxxxxxxdxxGtwrsqwshsvgsvhisggseeeqytxxxxxxxxxxxxxxxxxxxrixGMXXLDFGSVxGRiDPStrqclqnmfyaidrndpaglcdclieimdrpeeideqrlerelgqfmakhcfnhgkqpdtemftdlfrmindygfsvppeiaa---------------()}*

$48=1072(164):
>UbiB_48  {|191(54)|} 
{()----------------------------------NVLETIacDxLhicrsvappwkkmpnfnttwvglvdewgcrfvnxxxxxxxxexxxxxXLDyrLEANNTINF----------------------------------------------------------------------------------xxxxxxxxxxxxxxmxxxxxxxxxxxxxxkxxxxxxxxxxdxxmxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxsrpaxxxxxxxxxxxxxxxxxvcixxphvymecstxrxxxxxxxxxkxxxxvhttxxexxxxxxxxxxwvxxxxxxxxxxxxxxxxxdxgexxrxlxxxxxxxxxdxxxxxxxxesxxxxtxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxvprlcntamnxxxxxxxxxcymtmxmlextxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxlxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhcxxdphpgnllxrxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtxxxxxxxxxxxxxxxxxxxtxxxxxxxxxdxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxklxCIxxLDWGMMxTEiPPDqqyamiehiahlnhkdydkipqdfvnlgfvppendtkmmvpiqrsiftealngqgakgvnmkgvsnkmeemqkkypfqippwfay---------------()}*

$49=1071(164):
>UbiB_30  {|191(55)|} 
{()----------------------------------GIEKMMrnDxLdimqklaqwaehveelaqwqpcdtvrqfqrmimrxxxxxxxexxxxxXLDfnRERKNMKQF------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxqxxxxxxxxxxnxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkddktexxxxxxxxxxxxxxxxxvhfxxpkpypqyctxrxxxxxxxxxrxxxxvltmxxexxxxxxxxxxemxxxxxxxxxxxxxxxxxdxgixxpxlxxxxxxxxxskxxxxxxxkfxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhmxxxesxxxxxxxxxxxxxArknesesltaasaektagedrtpnskshhpxxxxxxxxxxxxxxxxxxxxxxxxxGadtnevaigmtkadl----------------------------------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxppevahngakxxxxxxxxxtyldmxifrxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnlmxcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxlxxxxxxxxxxxxxxxxxxxnxxxxxxxxxkxxqxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrixgixxLDCGMVxGRiDERmresieemliaintgdcesiteqimrwgnppcdfdqpaleidvhdfvdtygsqkldefdlsgclndvteiihrhhiylpnqvam---------------()}*

$50=1096(164):
>UbiB_3  {|167(54)|} 
{()---------------------------AVKVQXrxxxxxxxxxxxxxxxpXNLRPCItlDxLyimrwlacwlnkwvplnidlvaivdewgtrlfexxxxxxxxexxxxxXMDycNEGRNAEKF------------------------------------------------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxexxxxxxxxxxnxxyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrghnpdxxxxxxxxxxxxxxxxxvyvxxpkiyweytsxrxxxxxxxxxrxxxxvltmxxexxxxxxxxxxwixxxxxxxxxxxxxxxxxdxgtxxkxcxxxxxxxxxtxxxxxxxxntxxxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgidxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpnhmvnvgvqxxxxxxxxxcslrqxllexhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnlfxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxmxxxxxxxxxxxxxxxxxxxpxxxxxxxxxdxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkmxcyxxLDFGMMxSQiSPYqkygmieccvhlvnrdyeamakdfvklgfltpdtdltpiipalekvfgnalgasgvsnfnfktitddfsemmyeypfrvpayyal---------------()}*

$51=1074(164):
>UbiB_orph  {|191(52)|} 
{()----------------------------------GIEEQIssDxLkilrwlawwlpkytpwgrrydwpgivdewkktmywxxxxxxexxxxxXCDyrNEARNCERF------------------------------------------------------------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxxxxxkxxxxxxxxxxnxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxnwppxxxxxxxxxxxxxxxxxvyvxxpkvyweyctxkxxxxxxxxxrxxxxvltmxxexxxxxxxxxxwixxxxxxxxxxxxxxxxxdxgcxxkxixxxxxxxxxdxxxxxxxxdgxxxxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdpxxxkkxxxxxxxxxxxxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxggdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpkeicrtgvexxxxxxxxxcylhqxifexhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhcxxdphpgnilxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtxxxxxxxxxxxxxxxxxxxpxxxxxxxxxdxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtixcfxxIDFGMCxGElSPEtrrnfcklwyhlinndyegmcdcmiemgfwpescdmtivrpdlqrvmnhyitwhtqelkkinfgemtndlfdlmydypfqippyftf---------------()}*

$52=1070(164):
>UbiB_2  {|191(56)|} 
{()----------------------------------GIRKTIetDxLrilkhlaklaerhwpwakryqpcemvdefarslrnxxxxxxexxxxxXLDytNECRNCERF------------------------------------------------------------------------------------xxxxxxxxxxxxxxaxxxxxxxxxxxxxxkxxxxxxxxxxnxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxndpyxxxxxxxxxxxxxxxxxvyixxpkvywewccxkxxxxxxxxxrxxxxvctmxxexxxxxxxxxxyixxxxxxxxxxxxxxxxxdxgixxkxgxxxxxxxxxnxxxxxxxxdlxxxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxldxxxamxxxxxxxxxxxxxaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgydxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrkkiarrgadxxxxxxxxxavckqxifexhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxcxxxxxxxxxxxxxxxfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxhaxxdphpgnifxcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxlxxxxxxxxxxxxxxxxxxxpxxxxxxxxxdxxnxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrixcfxxIDFGMVxGRlSPEmreqlaslchammnqdtdgvidvlcdwggpddvdmnrlkrdidefvdkyygvplkqisigemlndlfaimrkhhitlppdftl---------------()}*

$53=355(164):
>PI3K_orph  {|254(708)|} 
{()---------------------------------------------QDMRMMQFFgmmnniwkkindpr-----------------------------------nxxxxxxxxxxxxxxkxxxxxxxxxxrxxyhxxxxxxxxxxxxxxxxxxxxxxxxxxxxixrxxxxxxxxxxxxxxxxxxxxpykxxvxxxxxxipxtgpnxxxxxcgxxxxxxmixxexxxxxxxxxxwvxxxxxxxxxxxxxxxxxpxntxxttlxxxxxxxxxhxxxxxxxxeixxxxixxxxxxxxxxxxxxxxxxxkxxxxxxxxxxDwrqreaaaqnscyqtpqenpgipppvpiqsemfyngitpalkkhgipglnahkrwmpqdwvevfeevceetpxxxxxxxpxxxpxvlxxxhhxxxxxxxXXXXXXWfwckfpdpeawfe-------------------------------------------------------------------()}*

$54=355(164):
>PI3K_9  {|254(708)|} 
{()---------------------------------------------KDCRLMEFNslinkclrkdaesrR----------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxexxlhxxxxxxxxxxxxxxxxxxxxxxxxxxxxixrxxxxxxxxxxxxxxxxxxxxtyaxxvxxxxxxipxlndexxxxxcgxxxxxxiixxexxxxxxxxxxwvxxxxxxxxxxxxxxxxxnxntxxaglxxxxxxxxxrxxxxxxxxpixxxxlxxxxxxxxxxxxxxxxxxxtxxxxxxxxxxKlykekgvymtgkelrqcmlpksaalseklkvfqefllprhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxxPXVFxxxHExxxxxxxxxxxxxWflrtfpdptswys-------------------------------------------------------------------()}*

$55=355(164):
>PI3K_26  {|254(708)|} 
{()---------------------------------------------KDAKVVEFTtmvnrilststearK----------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxgxxlqxxxxxxxxxxxxxxxxxxxxxxxxxxxxixaxxxxxxxxxxxxxxxxxxxxnysxxvxxxxxxvpxlaehxxxxxmgxxxxxxvixxexxxxxxxxxxfvxxxxxxxxxxxxxxxxxmxdvxxqtmxxxxxxxxxkxxxxxxxxgixxxxixxxxxxxxxxxxxxxxxxxnxxxxxxxxxxEqrkrmgqmvnerkmfmhldeaqkmvkgkhdsdqkamddlvagfrhichecxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxxPXVLxxxHNxxxxxxxxxxxxxWfieqfsdpsawym-------------------------------------------------------------------()}*

$56=355(164):
>PI3K_30  {|254(708)|} 
{()---------------------------------------------KDARLMDFNsiinkllkansesrR----------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxqxxlhxxxxxxxxxxxxxxxxxxxxxxxxxxxxixrxxxxxxxxxxxxxxxxxxxxtygxxvxxxxxxvtxlneexxxxxcgxxxxxxfixxqxxxxxxxxxxwvxxxxxxxxxxxxxxxxxpxntxxipixxxxxxxxxrxxxxxxxxpvxxxxlxxxxxxxxxxxxxxxxxxxcxxxxxxxxxxKyydsrgikswnnemsdvfkkikencdkeaahifvtkvlpqfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxxPXVFxxxHExxxxxxxxxxxxxWfietfpepsawla-------------------------------------------------------------------()}*

$57=355(164):
>PI3K_18  {|254(708)|} 
{()---------------------------------------------QDNQYMQFAttmdfllskdiasrK----------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxsxxlgxxxxxxxxxxxxxxxxxxxxxxxxxxxxixtxxxxxxxxxxxxxxxxxxxxtysxxvxxxxxxlsxlredxxxxxcgxxxxxxilxxexxxxxxxxxxmvxxxxxxxxxxxxxxxxxpxnvxxvtlxxxxxxxxxrxxxxxxxxsixxxxfxxxxxxxxxxxxxxxxxxxsxxxxxxxxxxTkyesmkikyslkslhdrwqhtaadgklgfymeqvdkf------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxxpxvlxxxyqxxxxxxxxxxxxxWflenfpdpinwfn-------------------------------------------------------------------()}*

$58=355(164):
>PI3K_24  {|254(708)|} 
{()---------------------------------------------KDARMMEFTaminrllskypesrR----------------------------------xxxxxxxxxxxxxxrxxxxxxxxxxkxxlyxxxxxxxxxxxxxxxxxxxxxxxxxxxxixrxxxxxxxxxxxxxxxxxxxxtfaxxvxxxxxxipxltedxxxxxcgxxxxxxmvxxexxxxxxxxxxwvxxxxxxxxxxxxxxxxxpxhtxxrglxxxxxxxxxrxxxxxxxxhixxxxlxxxxxxxxxxxxxxxxxxxqxxxxxxxxxxDiyitcgkfdrqktnpqikriydqcqgkmpeyemlktkilpmfxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxpxxxPXVFxxxHKxxxxxxxxxxxxxWflttfsepaawfr-------------------------------------------------------------------()}*

$59=482(164):
>PI3K_28  {|254(581)|} 
{()---------------------------------------------QDERVMQLFxxxxxxxxxxxxxxGlvntllendpecfkR-----------------------------------------xxxxxxxxxxhxxlsxxxxxxxxxxxxxxxxxxxxxxxxxxxxixqxxxxxxxxxxxxxxxxxxxxryaxxvxxxxxxipxlspnxxxxxsgxxxxxxlixxgxxxxxxxxxxwvxxxxxxxxxxxxxxxxxpncdxxtxlxxxxxxxxxhxxxxxxxxalxxxxixxxxxxxxxxxxxxxxxxxrxxxxxxxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxyrxxxxxxxxxxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxKkkiflniehrcmlqmapdydhlplmqkvevfehaldnttgddlykvlwfkspssevwfdrRTNYTRSLAVxxxxxxxxxMSMVG---------------------------------------------------()}*

$60=482(164):
>PI3K_0  {|254(581)|} 
{()---------------------------------------------QDERVMQLFxxxxxxxxxxxxxxGlvntllandpecfkR-----------------------------------------xxxxxxxxxxhxxlsxxxxxxxxxxxxxxxxxxxxxxxxxxxxixqxxxxxxxxxxxxxxxxxxxxrypxxvxxxxxxipxlspnxxxxxsgxxxxxxlixxgxxxxxxxxxxwvxxxxxxxxxxxxxxxxxpncdxxtxlxxxxxxxxxhxxxxxxxxvlxxxxixxxxxxxxxxxxxxxxxxxrxxxxxxxxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxyrxxxxxxxxxxxxxexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxSrkillniehrimlqmapdydhltlmqkvevfeyaldnttgqdlykvlwlkspssevwfdrRTNYTRSLAVxxxxxxxxxMSMVG---------------------------------------------------()}*

$61=83(164):
>PI3K_2  {|254(980)|} 
{()---------------------------------------------QDMLTLQMIxxxxxxxxxxxxxxRimdniwknqgXXxxlxxxxxxxxxxdxxLryyfllgsvlicscrsciqnavlllcvsrM---------------------------------------------------------------------------------------------------------()}*

_0].
